# Zaaksysteem Apps

A collection of apps which (combined) expose the Zaaksysteem.nl UI.

## Versions

This Docker image is based on Debian Stretch.

Dockerhub Node version: `node:8-stretch`

## Layout

```
apps            Source of our apps.
  Admin         The Admin SPA. App directories are upper camel cased.
  [, ...]       TODO apps :-)
  data          JSON serializable apps data.
  library       Transient location for apps code that should be published.
  manual        Custom apps documentation in markdown for `ESDoc`.
  test          Apps test configuration and doubles.
dev-bin         Utilities for use in development mode.
development     Mock API server & local nginx proxy
  mocks         Mock API server
    src         Mock request files
  nginx         Local nginx proxy
Docker          Assets for our docker instance.
node            The developer working directory for the container.
  bash          Bash setup for the development container users.
  bin           Node.js build executables.
    library     Utility functions for the executables.
    manual      Custom build documentation in markdown for `ESDoc`.
    test        Build test configuration and doubles.
npm             npm package and lock files under version control.
  build         Node.js build and QA dependencies.
  vendor        Client-side (i.e. for the browser) dependencies.
```

## Documentation

```
$ cd /path/to/zaaksysteem
$ docker-compose exec -e COLUMNS="`tput cols`" -e LINES="`tput lines`" apps bash
# zs docs
# zs rtfm
```

The documentaton is available at `http://localhost:8080` on your host.

## Docker usage

This image is designed to be used with an nginx proxy from the main zaaksysteem.nl repository. It
is also possible to run the image without a proxy.

### Build

When building an image, you can optionally define a target. The default is `development`, example:

```
dev-bin/build.sh development    # Builds for target development, just as:
dev-bin/build.sh                # identical

dev-bin/build.sh production     # Builds for production

```

### Run with mock API

#### Starting the application

It is possible to run this application with a mocked API. To start the application with a mocked API, run the 
following command in the root of this project `docker-compose up -d`

The application is now running but you still need to start the Webpack development server. First we need to access
the `zs` environment running inside the container. Execute `docker-compose exec -u node apps bash --init-file bash/node.bashrc` in the 
root of this project.

To start the development server run `zs start development`

#### Mocking a request

The local nginx proxy proxies all requests to /api to the mock server. The mocks are being served from `development/mocks/src`, each segment 
of the request is represented by a folder in the src directory. In the final segment of the request path you can create a mock file. If you
want to mock a GET request, create a GET.mock file. 

If for example we want to mock the following API request `/api/v2/example/1`, the directory structure should look like this:
```
development/
  mocks/
    src/
      api/
        v2/
          example/
            1/
              GET.mock
```

### Run simple proxy

Running a nginx on port 8765 is as easy as running the run command _after_ you have build the repo:

```
dev-bin/run.sh
```

And point your browser to `http://localhost:8765/admin/` for the admin package

### docker-compose development override

Cf. `../zaaksysteem/docker-compose.override.yml`

```
services:
  apps:
    build:
      context: "../zaaksysteem-apps"
      target: "development"
      args:
        - NODE_ENV=development
    depends_on:
      - frontend
    ports:
      - "80:80"
      - "8080:8080"
      - "8081:8081"
      - "8082:8082"
    volumes:
      - "../zaaksysteem-apps/dev-bin:/opt/zaaksysteem-apps/dev-bin"
      - "../zaaksysteem-apps/node/bin:/opt/zaaksysteem-apps/node/bin"
      - "../zaaksysteem-apps/apps:/opt/zaaksysteem-apps/node/src"
      - "../zaaksysteem-apps/npm:/opt/zaaksysteem-apps/npm"
      - "certificates:/etc/nginx/ssl"
```

### Enjoy!

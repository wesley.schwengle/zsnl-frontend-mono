// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import Typography from '@mui/material/Typography';
import SubAppHeader from '../../components/Header/SubAppHeader';
import { useDataStoreStyles } from './DataStore.style';
import { DataTypeType, FiltersType, DataType } from './DataStore.types';
import { defaultDataType, defaultFilters, getData } from './DataStore.library';
import ActionBar from './ActionBar/ActionBar';
import DataTable from './DataTable/DataTable';

const DataStore: React.ComponentType = () => {
  const classes = useDataStoreStyles();
  const [t] = useTranslation('dataStore');
  const [dataType, setDataType] = useState<DataTypeType>(defaultDataType);
  const [filters, setFilters] = useState<FiltersType>(defaultFilters);

  const [data, setData] = useState<DataType>();

  const refreshData = () => {
    getData(dataType, filters).then((data: DataType) => {
      setData(data);
    });
  };

  useEffect(() => {
    refreshData();
  }, [dataType, filters]);

  return (
    <div className={classes.wrapper}>
      <SubAppHeader>
        <Typography variant="h3">{t('dataStore')}</Typography>
      </SubAppHeader>
      <ActionBar
        data={data}
        dataType={dataType}
        filters={filters}
        setDataType={setDataType}
        setFilters={setFilters}
      />
      <DataTable
        data={data}
        dataType={dataType}
        filters={filters}
        setFilters={setFilters}
        refreshData={refreshData}
      />
    </div>
  );
};

export default DataStore;

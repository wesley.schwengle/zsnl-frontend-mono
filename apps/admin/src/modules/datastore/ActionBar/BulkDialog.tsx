// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
//@ts-ignore
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import { capitalize } from '@mintlab/kitchen-sink/source';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { getActiveFilters, performBulkAction } from '../DataStore.library';
import { DataType, DataTypeType, FiltersType } from '../DataStore.types';
import { useBulkDialogStyles } from './BulkDialog.style';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['primary', 'text'],
  tertiaryPresets: ['default', 'text'],
});

type FiltersDialogPropsType = {
  data?: DataType;
  dataType: DataTypeType;
  filters: FiltersType;
  setSnack: (text: string) => void;
  onClose: () => void;
  open: boolean;
};

const BulkDialog: React.ComponentType<FiltersDialogPropsType> = ({
  data,
  dataType,
  filters,
  setSnack,
  onClose,
  open,
}) => {
  const [t] = useTranslation('dataStore');
  const [onlyCurrentPage, setOnlyCurrentPage] = useState<boolean>(false);
  const classes = useBulkDialogStyles();
  const dialogEl = useRef();

  const title = t('bulk.dialog.title', { dataType });

  const activeFilters =
    getActiveFilters(t, filters, dataType) || t('common:none');

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'dataStore-bulk-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="delete"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent padded={true}>
          <div className={classes.contentWrapper}>
            <span>{t('bulk.dialog.explanation')}</span>
            {activeFilters.length > 0 && (
              <ul>
                {activeFilters.map(filter => (
                  <li key={filter}>{capitalize(filter)}</li>
                ))}
              </ul>
            )}
            <Checkbox
              formControlClasses={{ label: classes.checkboxLabel }}
              label={t('bulk.dialog.onlyCurrentPage')}
              onChange={() => setOnlyCurrentPage(!onlyCurrentPage)}
              checked={onlyCurrentPage}
              size="small"
            />
            <span>{t('cleanUp.dialog.warning')}</span>
          </div>
        </DialogContent>
        <>
          <DialogDivider />
          <DialogActions>
            {getDialogActions(
              {
                text: t('common:verbs.delete'),
                action() {
                  performBulkAction({
                    type: 'delete',
                    onlyCurrentPage,
                    data,
                    dataType,
                    filters,
                  }).then(() => {
                    setSnack(t('bulk.snack.delete'));
                    onClose();
                  });
                },
              },
              {
                text: t('common:verbs.create'),
                action() {
                  performBulkAction({
                    type: 'create',
                    onlyCurrentPage,
                    data,
                    dataType,
                    filters,
                  }).then(() => {
                    setSnack(t('bulk.snack.create'));
                    onClose();
                  });
                },
              },
              {
                text: t('common:dialog.cancel'),
                action: onClose,
              },
              'datastore-bulk-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
    </>
  );
};

export default BulkDialog;

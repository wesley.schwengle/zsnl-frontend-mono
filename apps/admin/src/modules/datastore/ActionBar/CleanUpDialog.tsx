// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { performBulkAction } from '../DataStore.library';
import { DataTypeType } from '../DataStore.types';
import { useCleanUpDialogStyles } from './CleanUpDialog.style';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
});

type FiltersDialogPropsType = {
  dataType: DataTypeType;
  setSnack: (text: string) => void;
  onClose: () => void;
  open: boolean;
};

const FiltersDialog: React.ComponentType<FiltersDialogPropsType> = ({
  dataType,
  setSnack,
  onClose,
  open,
}) => {
  const [t] = useTranslation('dataStore');
  const classes = useCleanUpDialogStyles();
  const dialogEl = useRef();

  const title = t('cleanUp.dialog.title');

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'dataStore-cleanup-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="delete"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent padded={true}>
          <div className={classes.contentWrapper}>
            <span>{t('cleanUp.dialog.explanation')}</span>
            <span>{t('cleanUp.dialog.warning')}</span>
          </div>
        </DialogContent>
        <>
          <DialogDivider />
          <DialogActions>
            {getDialogActions(
              {
                text: title,
                action() {
                  performBulkAction({
                    type: 'delete',
                    dataType,
                    filters: {
                      active: '2',
                      cases: '2',
                    },
                  }).then(() => {
                    setSnack(t('cleanUp.snack'));
                    onClose();
                  });
                },
              },
              {
                text: t('common:dialog.cancel'),
                action: onClose,
              },
              'datastore-cleanup-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
    </>
  );
};

export default FiltersDialog;

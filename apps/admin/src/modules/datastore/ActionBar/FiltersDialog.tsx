// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { defaultFilters } from '../DataStore.library';
import { useFiltersDialogStyles } from './FiltersDialog.style';
import {
  DataTypeType,
  FiltersType,
  SetFiltersType,
} from './../DataStore.types';
import {
  getFiltersDialogFormDefinition,
  getFiltersDialogRules,
} from './FiltersDialog.formdefinition';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
  tertiaryPresets: ['default', 'text'],
});

type FiltersDialogPropsType = {
  dataType: DataTypeType;
  filters: FiltersType;
  setFilters: SetFiltersType;
  onClose: () => void;
  open: boolean;
};

const FiltersDialog: React.ComponentType<FiltersDialogPropsType> = ({
  dataType,
  filters,
  setFilters,
  onClose,
  open,
}) => {
  const [t] = useTranslation('dataStore');
  const classes = useFiltersDialogStyles();
  const dialogEl = useRef();

  const title = t('filters.dialog.title');
  const formDefinition = getFiltersDialogFormDefinition(t, dataType, filters);
  const rules = getFiltersDialogRules(dataType);

  let {
    fields,
    formik: { values },
  } = useForm({
    formDefinition,
    rules,
  });

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'dataStore-filters-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="settings"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent padded={true}>
          <div className={classes.formWrapper}>
            {fields.map(
              ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
                const props = cloneWithout(rest, 'mode');

                return (
                  <FormControlWrapper
                    {...props}
                    label={suppressLabel ? false : props.label}
                    compact={true}
                    key={`${props.name}-formcontrol-wrapper`}
                  >
                    <FieldComponent
                      {...props}
                      t={t}
                      containerRef={dialogEl.current}
                    />
                  </FormControlWrapper>
                );
              }
            )}
          </div>
        </DialogContent>
        <>
          <DialogDivider />
          <DialogActions>
            {getDialogActions(
              {
                text: t('filters.dialog.set'),
                action() {
                  setFilters({
                    ...values,
                    freeform_filter: filters.freeform_filter,
                    zapi_page: 0,
                    zapi_num_rows: filters.zapi_num_rows,
                  });
                  onClose();
                },
              },
              {
                text: t('filters.dialog.reset'),
                action() {
                  setFilters({
                    ...defaultFilters,
                    freeform_filter: filters.freeform_filter,
                  });
                  onClose();
                },
              },
              {
                text: t('common:dialog.cancel'),
                action: onClose,
              },
              'datastore-filters-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
    </>
  );
};

export default FiltersDialog;

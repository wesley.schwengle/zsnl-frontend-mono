// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    dataStore: 'Gegevensmagazijn',
    activeFilters: 'Actieve filters',
    cleanUp: {
      button: 'Opschonen',
      snack:
        'Alle inactieve objecten zonder zaken in de verwijderwachtrij geplaatst.',
      dialog: {
        title: 'Opschonen',
        explanation:
          'Opschonen zal alle inactieve contacten zonder zaken verwijderen.',
        warning: 'Weet u zeker dat u wilt doorgaan?',
      },
    },
    export: {
      button: 'Exporteren',
      snack:
        'Exportbestand wordt gegenereerd. Er wordt na afronding een bericht verstuurd.',
    },
    bulk: {
      button: 'Bulkactie',
      snack: {
        create: 'Objecten succesvol in de aanmaakwachtrij geplaatst.',
        delete: 'Objecten succesvol in de verwijderwachtrij geplaatst.',
      },
      dialog: {
        title: 'Bulkactie uitvoeren voor {{dataType}}',
        explanation: 'De bulkactie zal voor alle objecten uitgevoerd worden.',
        onlyCurrentPage: 'Enkel voor de huidige pagina',
      },
    },
    filters: {
      button: 'Filters',
      dialog: {
        title: 'Filters instellen',
        set: 'Instellen',
        reset: 'Resetten',
      },
      fields: {
        freeform_filter: {
          label: 'Tekst: {{value}}',
          placeholder: 'Typ om te zoeken…',
        },
        object_subscription: {
          0: 'Met of zonder afnemersindicatie',
          1: 'Met afnemersindicatie',
          2: 'Zonder afnemersindicatie',
        },
        active: {
          0: 'Actieve of inactieve personen',
          1: 'Actieve personen',
          2: 'Inactieve personen',
        },
        cases: {
          0: 'Wel of niet betrokken bij zaken',
          1: 'Wel betrokken bij zaken',
          2: 'Niet betrokken bij zaken',
        },
        running: {
          0: 'Met of zonder lopende zaken',
          1: 'Met lopende zaken',
          2: 'Zonder lopende zaken',
        },
        concepts: {
          0: 'Met of zonder conceptzaken',
          1: 'Met conceptzaken',
          2: 'Zonder conceptzaken',
        },
        address: {
          0: 'Met of zonder adres',
          1: 'Met adres',
          2: 'Zonder adres',
        },
        deceased: {
          NatuurlijkPersoon: {
            0: 'Levende of overleden personen',
            1: 'Overleden personen',
            2: 'Levende personen',
          },
          Organisatie: {
            0: 'Opgeheven of functionerende bedrijven',
            1: 'Opgeheven bedrijven',
            2: 'Functionerende bedrijven',
          },
        },
        muncipality: {
          0: 'Binnen- of buitengemeentelijke personen',
          1: 'Binnengemeentelijke personen',
          2: 'Buitengemeentelijke personen',
        },
      },
    },
    table: {
      labelRowsPerPage: 'Per pagina',
      noResults: 'Geen resultaten',
      column: {
        pending: 'wachtrij',
      },
      actions: {
        create: {
          button: 'Aanmaken',
          success: 'Actie voor object met ID {{id}} succesvol uitgevoerd.',
        },
        delete: {
          button: 'Verwijderen',
          success:
            'Actie voor object met ID {{id}} in de verwijderwachtrij geplaatst.',
        },
      },
    },
  },
};

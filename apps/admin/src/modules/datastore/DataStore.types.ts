// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormikValues } from 'formik';

export type DataTypeType =
  | 'NatuurlijkPersoon'
  | 'Organisatie'
  | 'BagLigplaats'
  | 'BagNummeraanduiding'
  | 'BagOpenbareruimte'
  | 'BagPand'
  | 'BagStandplaats'
  | 'BagVerblijfsobject'
  | 'BagWoonplaats';

export type SetDataTypeType = (dataType: DataTypeType) => void;

export type FilterValueType = '0' | '1' | '2' | string;

export type FilterKeyType =
  | 'object_subscription'
  | 'active'
  | 'cases'
  | 'running'
  | 'concepts'
  | 'address'
  | 'deceased'
  | 'muncipality' // [SIC]
  | 'freeform_filter';

export type FiltersType = {
  freeform_filter: string;
  active: FilterValueType;
  address: FilterValueType;
  cases: FilterValueType;
  concepts: FilterValueType;
  deceased: FilterValueType;
  muncipality: FilterValueType;
  object_subscription: FilterValueType;
  running: FilterValueType;
  zapi_num_rows: number;
  zapi_page: number;
  zapi_order_by?: string;
  zapi_order_by_direction?: 'ASC' | 'DESC';
};

export type SetFiltersType = (filters: FiltersType) => void;

export type RowType = any;

export type DataType = {
  count: number;
  rows: RowType[];
};

export type GetDateType = (
  dataType: DataTypeType,
  filters: FiltersType
) => Promise<DataType>;

export type InitiateExportType = (
  dataType: DataTypeType,
  filters: FiltersType
) => any;

export type ActionTypeType = 'create' | 'delete';

export type PerformBulkActionType = (params: {
  type: ActionTypeType;
  onlyCurrentPage?: boolean;
  data?: DataType;
  dataType: DataTypeType;
  filters: any;
}) => any;

export type GetFilterLabelType = (
  t: i18next.TFunction,
  filter: FilterKeyType,
  dataType: DataTypeType,
  value: FilterValueType
) => string;

export type GetActiveFiltersType = (
  t: i18next.TFunction,
  filters: FiltersType,
  dataType: DataTypeType
) => string[];

export type ConvertFilterFormValuesType = (values: FormikValues) => FiltersType;

export type UpdateContactType = (
  actionType: ActionTypeType,
  dataType: DataTypeType,
  id: number
) => any;

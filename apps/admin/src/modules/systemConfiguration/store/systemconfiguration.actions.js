// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  SYSTEMCONFIGURATION_FETCH,
  SYSTEMCONFIGURATION_UPDATE_CHOICES,
  SYSTEMCONFIGURATION_SAVE,
  SYSTEMCONFIGURATION_DISCARD,
  SYSTEMCONFIGURATION_DISCARD_DONE,
} from './systemconfiguration.constants';
import { appToDb } from './library/library';

const fetchAjaxAction = createAjaxAction(SYSTEMCONFIGURATION_FETCH);
const updateAjaxAction = createAjaxAction(SYSTEMCONFIGURATION_UPDATE_CHOICES);
const saveAjaxAction = createAjaxAction(SYSTEMCONFIGURATION_SAVE);

/**
 * @param {string} name
 * @return {Object|undefined}
 */
const parameters = {
  emailTemplate: {
    url: '/api/v1/email_template',
    match: 'label',
  },
  group: {
    url: '/api/v1/group',
    match: 'description',
  },
  municipalities: {
    url: '/api/v1/general/municipality_code',
    match: 'name',
  },
  rolev2: {
    url: '/api/v2/cm/authorization/get_roles',
  },
};

/**
 * @type {Object}
 */
export const choicesMap = {
  allocation_notification_template_id: parameters.emailTemplate,
  bag_priority_gemeentes: parameters.municipalities,
  case_distributor_group: parameters.group,
  case_distributor_role: parameters.rolev2,
  case_suspension_term_exceeded_notification_template_id:
    parameters.emailTemplate,
  case_term_exceeded_notification_template_id: parameters.emailTemplate,
  export_queue_email_template_id: parameters.emailTemplate,
  feedback_email_template_id: parameters.emailTemplate,
  new_assigned_case_notification_template_id: parameters.emailTemplate,
  new_attribute_proposal_notification_template_id: parameters.emailTemplate,
  new_document_notification_template_id: parameters.emailTemplate,
  new_ext_pip_message_notification_template_id: parameters.emailTemplate,
  new_int_pip_message_notification_template_id: parameters.emailTemplate,
  new_user_template: parameters.emailTemplate,
  subject_pip_authorization_confirmation_template_id: parameters.emailTemplate,
};

/**
 * @return {Function}
 */
export const fetchSystemConfiguration = () =>
  fetchAjaxAction({
    url: '/api/v1/config/panel',
    method: 'GET',
  });

/**
 * @param {Object} parameters
 * @param {string} parameters.input
 * @param {string} parameters.name
 * @return {Function}
 */
export const updateChoices = ({ input, name }) => {
  const { url, match } = choicesMap[name];
  let parsedUrl;

  if (url.match('/v1/')) {
    parsedUrl = input
      ? `${url}?query:match:${match}=${input}&rows_per_page=200`
      : url;
  } else if (name === 'case_distributor_role') {
    parsedUrl = `${url}?filter[relationships.parent.id]=${input}`;
  }

  return updateAjaxAction({
    url: parsedUrl,
    method: 'GET',
    payload: {
      name,
    },
  });
};

/**
 * @param {*} payload
 * @return {Object}
 */
export const discard = payload => ({
  type: SYSTEMCONFIGURATION_DISCARD,
  payload,
});

/**
 * @param {*} payload
 * @return {Object}
 */
export const discardDone = payload => ({
  type: SYSTEMCONFIGURATION_DISCARD_DONE,
  payload,
});

/**
 * @param {Array} items
 * @return {Function}
 */
export const saveSystemConfiguration = items => {
  const saveData = items.reduce((accumulator, item) => {
    accumulator[item.reference] = {
      value: appToDb(item.value),
    };
    return accumulator;
  }, {});

  return saveAjaxAction({
    url: '/api/v1/config/update',
    method: 'POST',
    data: {
      items: saveData,
    },
  });
};

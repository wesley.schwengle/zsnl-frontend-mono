// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { asArray, get } from '@mintlab/kitchen-sink/source';
import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import { CATALOG_CHANGE_ONLINE_STATUS } from '../changeOnlineStatus/changeOnlineStatus.constants';
import {
  CATALOG_FETCH,
  CATALOG_TOGGLE_ITEM,
  CATALOG_CLEAR_SELECTED,
  CATALOG_ROUTE_CHANGE,
  CATALOG_RELOAD,
} from './items.constants';

export const initialState = {
  state: AJAX_STATE_INIT,
  selectedItems: [],
  items: [],
  pageLoaded: 0,
  query: null,
};

const getBreadcrumbItem = (response, path) => {
  const link = get(response, path);
  return link
    ? {
        name: link.name,
        id: link.id,
      }
    : null;
};

export const fetchCatalogSuccess = (state, action) => {
  const { response } = action.payload;
  const data = get(response, 'data');
  const itemsByUuid = data.map(item => item.id);
  const selectedItems = state.selectedItems.filter(uuid =>
    asArray(itemsByUuid).includes(uuid)
  );

  const newItems = response.data.map(element => ({
    active: element.attributes.active,
    type: element.attributes.type,
    name: element.attributes.name,
    id: element.id,
  }));

  return {
    ...state,
    currentFolderName: get(response, 'links.self.name'),
    items: [...state.items, ...newItems],
    selectedItems,
    breadcrumbs: {
      current: getBreadcrumbItem(response, 'links.self'),
      parent: getBreadcrumbItem(response, 'links.parent'),
      grandparent: getBreadcrumbItem(response, 'links.grandparent'),
    },
    pageLoaded: action.payload.page,
  };
};

export const selectItem = (state, id, multiSelect = false) => ({
  ...state,
  selectedItems: [...(multiSelect ? state.selectedItems : []), id],
});

export const deselectItem = (state, id) => ({
  ...state,
  selectedItems: state.selectedItems.filter(item => item !== id),
});

export const toggleSelectedItem = (state, action) => {
  const { id, multiSelect } = action.payload;
  const shouldDeselectItem = state.selectedItems.includes(id) && multiSelect;

  return shouldDeselectItem
    ? deselectItem(state, id)
    : selectItem(state, id, multiSelect);
};

/** Update the item's active state after a succesfull request */
/**
 *
 * @param {Object} state
 * @param {Object} action
 * @returns {Object}
 */
export const handleChangeOnlineStatusSuccess = (state, action) => {
  const { active, id } = action.payload;

  const newItems = state.items.map(item => {
    if (item.id === id) {
      return {
        ...item,
        active,
      };
    }

    return item;
  });

  return {
    ...state,
    items: newItems,
  };
};

const handleLoadItems = (state, action) => {
  return {
    ...state,
    load: Date.now(),
    pageLoaded: 0,
    items: [],
    currentFolderUUID: action?.payload?.id || null,
  };
};

/* eslint complexity: [2, 12] */
export function items(state = initialState, action) {
  const handleAjaxState = handleAjaxStateChange(CATALOG_FETCH);

  switch (action.type) {
    case CATALOG_FETCH.ERROR:
    case CATALOG_FETCH.PENDING:
      return handleAjaxState(state, action);

    case CATALOG_FETCH.SUCCESS:
      return fetchCatalogSuccess(handleAjaxState(state, action), action);

    case CATALOG_TOGGLE_ITEM:
      return toggleSelectedItem(state, action);

    case CATALOG_CLEAR_SELECTED:
      return {
        ...state,
        selectedItems: [],
      };

    case CATALOG_CHANGE_ONLINE_STATUS.SUCCESS:
      return handleChangeOnlineStatusSuccess(state, action);

    case CATALOG_ROUTE_CHANGE:
    case CATALOG_RELOAD:
      return handleLoadItems(state, action);

    default:
      return state;
  }
}

export default items;

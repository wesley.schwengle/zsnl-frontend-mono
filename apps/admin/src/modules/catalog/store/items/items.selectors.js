// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const getSelectedItems = state => {
  const {
    catalog: {
      items: { items, selectedItems },
    },
  } = state;

  return selectedItems.map(id => items.find(item => item.id === id));
};

export const getSelectedItem = state => {
  const [first] = getSelectedItems(state);

  return first || null;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  CATALOG_MOVEITEMS_START,
  CATALOG_MOVEITEMS_CLEAR,
  CATALOG_MOVEITEMS_CONFIRM,
} from './moveItems.constants';

const moveAjaxAction = createAjaxAction(CATALOG_MOVEITEMS_CONFIRM);

export const startMoveItems = selectedItems => ({
  type: CATALOG_MOVEITEMS_START,
  payload: selectedItems,
});

export const clearMoveItems = () => ({
  type: CATALOG_MOVEITEMS_CLEAR,
  payload: undefined,
});

export const confirmMoveItems = ({ currentFolderUUID, moveItems }) => {
  const data = {
    destination_folder_id: currentFolderUUID,
    folder_entries: moveItems,
  };

  return moveAjaxAction({
    url: '/api/v2/admin/catalog/move_folder_entries',
    method: 'POST',
    data,
  });
};

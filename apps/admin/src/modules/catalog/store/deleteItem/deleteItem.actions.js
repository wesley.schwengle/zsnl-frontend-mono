// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  CATALOG_DELETE_ITEM,
  CATALOG_REQUEST_DELETE_ITEM,
} from './deleteItem.constants';

/* eslint complexity: [2, 8] */
const getDeleteEndPointOptions = item => {
  const { id, type } = item;
  switch (type) {
    case 'folder':
      return {
        url: '/api/v2/admin/catalog/delete_folder',
        data: {
          folder_uuid: id,
        },
      };

    case 'attribute':
      return {
        url: '/api/v2/admin/catalog/delete_attribute',
        data: {
          attribute_uuid: id,
        },
      };

    case 'email_template':
      return {
        url: '/api/v2/admin/catalog/delete_email_template',
        data: {
          email_template_uuid: id,
        },
      };

    case 'document_template':
      return {
        url: '/api/v2/admin/catalog/delete_document_template',
        data: {
          document_template_uuid: id,
        },
      };

    case 'case_type':
      return {
        url: '/api/v2/admin/catalog/delete_case_type',
        data: {
          case_type_uuid: id,
        },
      };

    case 'object_type':
      return {
        url: '/api/v2/admin/catalog/delete_object_type',
        data: {
          object_type_uuid: id,
        },
      };

    case 'custom_object_type':
      return {
        url: '/api/v2/cm/custom_object_type/delete_custom_object_type',
        data: {
          version_independent_uuid: id,
        },
      };

    default:
      throw Error(`${type} is not supported`);
  }
};

const deleteItemAjaxAction = createAjaxAction(CATALOG_DELETE_ITEM);

export const requestDeleteItem = () => ({
  type: CATALOG_REQUEST_DELETE_ITEM,
});

export const deleteItem = (item, reason) => {
  const { url, data } = getDeleteEndPointOptions(item);

  return deleteItemAjaxAction({
    url,
    method: 'POST',
    data: {
      ...data,
      reason,
    },
    payload: item,
  });
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import Button from '@mintlab/ui/App/Material/Button';
import Typography from '@mui/material/Typography';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Render from '@mintlab/ui/App/Abstract/Render';
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { detailViewStyleSheet } from './DetailView.style';
import Details from './Details/Details';

/**
 * @reactProps {Object} classes
 * @reactProps {Function} closeDetailView
 * @reactProps {Object} detailInfo
 * @reactProps {string} detailInfo.icon
 * @reactProps {string} detailInfo.name
 * @reactProps {string} detailInfo.type
 * @reactProps {Object} detailInfo.details
 * @reactProps {string} detailInfo.version
 * @reactProps {boolean} loading
 * @reactProps {Function} t
 * @reactProps {Function} doNavigate
 * @return {ReactElement}
 */
const DetailView = ({
  classes,
  toggleDetailView,
  detailInfo: { icon, name, type, details, version },
  loading,
  t,
  doNavigate,
}) => (
  <div className={classes.detailView}>
    <div className={classes.header}>
      <div className={classes.headerIcon}>
        <ZsIcon size="large">{icon}</ZsIcon>
      </div>
      <Typography
        variant="h3"
        classes={{
          root: classes.title,
        }}
      >
        {name}
      </Typography>
      <div>
        <Button
          action={toggleDetailView}
          presets={['icon', 'large']}
          scope="catalog-detail:close"
        >
          close
        </Button>
      </div>
    </div>
    <Render
      condition={
        (type === 'case_type' || type === 'custom_object_type') && version
      }
    >
      <div className={classes.subHeader}>
        <div className={classes.version}>
          {`${t('catalog:detailView:versionTitle')} ${version}`}
        </div>
      </div>
    </Render>
    <div className={classes.content}>
      {loading ? (
        <Loader />
      ) : (
        <Render condition={details}>
          <Details
            details={details}
            t={t}
            doNavigate={doNavigate}
            type={type}
          />
        </Render>
      )}
    </div>
  </div>
);

export default withStyles(detailViewStyleSheet)(DetailView);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import Icon from '@mintlab/ui/App/Material/Icon';
import { valuesStyleSheet } from './Values.style';

/**
 * @reactProps {Object} classes
 * @reactProps {Object} value
 * @reactProps {Function} doNavigate
 * @reactProps {string} title
 * @reactProps {boolean} [internal=true]
 * @return {ReactElement}
 */
const NavigateTo = ({ classes, value, title, doNavigate, internal = true }) => (
  <a
    className={classes.link}
    href={value}
    title={title}
    onClick={preventDefaultAndCall(() => {
      if (internal) {
        doNavigate(value);
      } else {
        window.open(value, '_blank').focus();
      }
    })}
  >
    {title}
    {!internal && <Icon size="extraSmall">open_in_new</Icon>}
  </a>
);

export default withStyles(valuesStyleSheet)(NavigateTo);

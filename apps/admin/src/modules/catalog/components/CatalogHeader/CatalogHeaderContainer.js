// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { get } from '@mintlab/kitchen-sink/source';
import { getPathToItem } from '../../library/pathGetters';
import CatalogHeader from './CatalogHeader';

/**
 * Transform parent and self into breadcrumbs
 * @param {Object} breadcrumbs
 * @return {Array<Object>}
 */
export const getBreadcrumbs = breadcrumbs => {
  const grandparent = get(breadcrumbs, 'grandparent', null);
  const parent = get(breadcrumbs, 'parent', null);
  const current = get(breadcrumbs, 'current', null);
  const breadcrumbsInOrder = [grandparent, parent, current];

  return breadcrumbsInOrder
    .filter(item => item && item.id)
    .map(({ id, name }) => ({
      path: getPathToItem(id),
      label: name,
    }));
};

const mapStateToProps = ({
  catalog: {
    items: { breadcrumbs, items },
    search: { query },
  },
  route,
}) => ({
  query,
  route,
  numItems: items.length,
  breadcrumbs: getBreadcrumbs(breadcrumbs),
});

function mergeProps(stateProps, dispatchProps, ownProps) {
  const { breadcrumbs, query, route, numItems } = stateProps;
  const { t } = ownProps;
  const breadcrumbsRoot = {
    path: getPathToItem(),
    label: t('catalog:title'),
  };
  const searchResultBreadcrumb = {
    path: route,
    label: `${t('common:searchResultsTitle')} ${
      numItems ? `(${numItems})` : ''
    }`,
  };

  return {
    ...stateProps,
    ...ownProps,
    ...dispatchProps,
    breadcrumbs: [
      breadcrumbsRoot,
      ...(query ? [searchResultBreadcrumb] : breadcrumbs),
    ],
  };
}

export default connect(mapStateToProps, null, mergeProps)(CatalogHeader);

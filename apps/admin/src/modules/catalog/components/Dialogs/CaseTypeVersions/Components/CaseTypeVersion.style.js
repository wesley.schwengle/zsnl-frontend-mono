// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const caseTypeVersionStyleSheet = ({
  mintlab: { greyscale },
  palette: { common, secondary },
  typography,
}) => ({
  panel: {
    padding: '0',
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    fontFamily: typography.fontFamily,
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expandedPanel: {
    margin: '0',
  },
  panelSummary: {
    paddingLeft: '52px',
    paddingRight: '24px',
  },
  panelSummaryActive: {
    backgroundColor: secondary.lightest,
  },
  summaryContent: {
    margin: '20px 0',
    '&$expanded': {
      margin: '12px 0',
    },
    '&>*:last-child': {
      padding: '0',
    },
  },
  summaryExpanded: {},
  summaryExpandIcon: {
    left: '8px',
    right: 'auto',
  },
  summaryWrapper: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    padding: '0',
  },
  avatar: {
    backgroundColor: greyscale.dark,
    color: common.black,
  },
  avatarActive: {
    backgroundColor: secondary.main,
  },
  dateTime: {
    flexGrow: '1',
    display: 'flex',
  },
  date: {
    fontWeight: typography.fontWeightMedium,
    marginLeft: '20px',
  },
  time: {
    color: greyscale.darkest,
    marginLeft: '10px',
  },
  activateButton: {
    marginLeft: '15px',
  },
  detailsActive: {
    paddingTop: '24px',
  },
  detailsCard: {
    background: greyscale.dark,
    width: '100%',
    '&>div:last-child': {
      paddingBottom: '0px',
    },
  },
  detailsContent: {
    padding: '0 16px',
    '&>div:not(:last-child)': {
      borderBottom: `1px solid ${greyscale.darker}`,
    },
  },
  detailWrapper: {
    display: 'flex',
    padding: '10px 0px',
  },
  detailTitle: {
    minWidth: '100px',
  },
  detailValue: {
    fontWeight: typography.fontWeightMedium,
  },
});

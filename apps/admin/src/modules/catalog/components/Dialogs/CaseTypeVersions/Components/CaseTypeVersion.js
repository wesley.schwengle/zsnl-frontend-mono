// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import { withStyles } from '@mui/styles';
import Avatar from '@mui/material/Avatar';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Button from '@mintlab/ui/App/Material/Button';
import Card from '@mintlab/ui/App/Material/Card';
import Icon from '@mintlab/ui/App/Material/Icon';
import Render from '@mintlab/ui/App/Abstract/Render';
import { caseTypeVersionStyleSheet } from './CaseTypeVersion.style';
import { withFormatDate } from './../../../../../../components/App/withFormatDate';

const CaseTypeVersion = ({
  classes,
  formatDate,
  initCaseTypeVersionsActivate,
  case_type_id,
  version: { active, details, created, version_id, version },
  t,
}) => {
  const dateTimeCreated = new Date(created);
  const date = formatDate(dateTimeCreated, t('common:dates:dateFormatText'));
  const time = formatDate(dateTimeCreated, t('common:dates:timeFormat'));

  return (
    <Accordion
      classes={{ root: classes.panel, expanded: classes.expandedPanel }}
    >
      <AccordionSummary
        classes={{
          root: classNames(classes.panelSummary, {
            [classes.panelSummaryActive]: active,
          }),
          content: classes.summaryContent,
          expanded: classes.summaryExpanded,
          expandIcon: classes.summaryExpandIcon,
        }}
        expandIcon={<Icon>expand_more</Icon>}
      >
        <div className={classes.summaryWrapper}>
          <Avatar className={active ? classes.avatarActive : classes.avatar}>
            {version}
          </Avatar>
          <div className={classes.dateTime}>
            <Render condition={created}>
              <div className={classes.date}>{date}</div>
              <div className={classes.time}>{`${time} ${t(
                'common:hour'
              )}`}</div>
            </Render>
          </div>
          <Render condition={!active}>
            <Button
              action={event => {
                event.stopPropagation();
                initCaseTypeVersionsActivate({
                  case_type_id,
                  version_id,
                  version,
                });
              }}
              classes={{ root: classes.activateButton }}
              scope={`catalog-header:button-bar:activate`}
              presets={['primary', 'semiContained']}
            >
              {t('caseTypeVersions:dialog.restore')}
            </Button>
          </Render>
        </div>
      </AccordionSummary>
      <AccordionDetails
        className={classNames({ [classes.detailsActive]: active })}
      >
        <Card
          classes={{
            card: classes.detailsCard,
            content: classes.detailsContent,
          }}
        >
          {details.map((detail, index) => {
            return (
              <div key={index} className={classes.detailWrapper}>
                <div className={classes.detailTitle}>{`${detail.title}:`}</div>
                <div className={classes.detailValue}>{detail.value}</div>
              </div>
            );
          })}
        </Card>
      </AccordionDetails>
    </Accordion>
  );
};

export default withStyles(caseTypeVersionStyleSheet)(
  withFormatDate(CaseTypeVersion)
);

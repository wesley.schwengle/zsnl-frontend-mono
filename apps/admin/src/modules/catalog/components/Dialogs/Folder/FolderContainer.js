// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { v4 } from 'uuid';
import { AJAX_STATE_PENDING } from '../../../../../library/redux/ajax/createAjaxConstants';
import { saveFolder } from '../../../store/folder/folder.actions';
import formDefinition from '../../../fixtures/folder';
import Folder from './Folder';

const mangleValuesForSave = ({ name, id, currentFolderUUID }) => {
  const isNew = !id;
  const folder_uuid = isNew ? v4() : id;

  return {
    name,
    folder_uuid,
    parent_uuid: currentFolderUUID,
    isNew,
  };
};

const mapStateToProps = (
  {
    catalog: {
      folder,
      items: { currentFolderUUID },
    },
  },
  { t }
) => {
  // Insert translations, requests and other values into FormDefinition
  const mapFormDefinition = field => {
    return {
      ...field,
      label: t(field.label),
      placeholder: t(field.placeholder),
      hint: t(field.hint),
      help: t(field.help),
      loadingMessage: t(field.loadingMessage),
      value: folder.name,
    };
  };
  const newProps = {
    ...folder,
    formDefinition: formDefinition.map(mapFormDefinition),
    currentFolderUUID,
  };

  return {
    busy: folder.savingState === AJAX_STATE_PENDING,
    ...newProps,
  };
};

const mapDispatchToProps = dispatch => ({
  dispatchSave: params => dispatch(saveFolder(params)),
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { currentFolderUUID, id } = stateProps;
  const { dispatchSave } = dispatchProps;

  return {
    ...stateProps,
    ...ownProps,
    saveAction: ({ values }) =>
      dispatchSave(
        mangleValuesForSave({
          name: values.name,
          id,
          currentFolderUUID,
        })
      ),
  };
};

const connectedDialog = withTranslation()(
  connect(mapStateToProps, mapDispatchToProps, mergeProps)(Folder)
);

export default connectedDialog;

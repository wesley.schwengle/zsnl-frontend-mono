// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Component } from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import { withStyles } from '@mui/styles';
import Button from '@mintlab/ui/App/Material/Button';
import Icon from '@mintlab/ui/App/Material/Icon';
import classNames from 'classnames';
import { bind } from '@mintlab/kitchen-sink/source';
import { withFormatDate } from '../../../../../../../components/App/withFormatDate';
import { versionsStylesheet } from './Versions.style';

class Versions extends Component {
  static defaultProps = {
    value: [],
  };
  constructor(props) {
    super(props);
    this.state = {
      mainExpanded: false,
    };
    bind(this, 'setExpanded', 'getDetailPanel', 'getOption');
  }

  render() {
    const {
      props: { classes, t, value },
      state: { mainExpanded },
    } = this;

    if (!value || !value.length) {
      return null;
    }

    const mainExpandButton = (
      <Button presets={['primary', 'semiContained', 'small']}>
        {mainExpanded ? t('common:hide') : t('common:show')}
      </Button>
    );

    return (
      <div className={classes.wrapper}>
        <Accordion
          classes={{
            root: classes.main,
          }}
          onChange={this.setExpanded}
        >
          <AccordionSummary
            classes={{
              expandIcon: classes.expandButton,
            }}
            expandIcon={mainExpandButton}
          >
            {t('attribute:dialog.history')}
          </AccordionSummary>
          <AccordionDetails
            classes={{
              root: classes.mainDetails,
            }}
          >
            {value.map(this.getDetailPanel)}
          </AccordionDetails>
        </Accordion>
      </div>
    );
  }

  setExpanded() {
    this.setState({
      mainExpanded: !this.state.mainExpanded,
    });
  }

  getOption(option) {
    const { classes } = this.props;
    return (
      <div
        className={classNames(classes.option, {
          [classes.disabled]: !option.active,
        })}
        key={`attribute-version-detail-option-${option.value}`}
      >
        {option.value}
      </div>
    );
  }

  getDetailPanel(detail) {
    const { classes, formatDate, t } = this.props;
    const createdDate = new Date(detail.created_date);

    return (
      <Accordion
        classes={{
          root: classes.version,
          expanded: classes.versionExpanded,
        }}
        key={`attribute-version-detail-${detail.version}`}
      >
        <AccordionSummary
          classes={{
            content: classes.versionSummary,
          }}
          expandIcon={<Icon>expand_more</Icon>}
        >
          <div className={classes.versionNr}>{detail.version}</div>
          <div className={classes.versionTitle}>
            <span>
              {formatDate(createdDate, t('attribute:dialog:versionTitleDate'))}
            </span>
            , {formatDate(createdDate, t('attribute:dialog:versionTitleTime'))}
          </div>
        </AccordionSummary>
        <AccordionDetails
          classes={{
            root: classes.versionDetails,
          }}
        >
          <div className={classes.versionDetailRow}>
            <div>{t('attribute:dialog.user')}</div>
            <div>{detail.created_by}</div>
          </div>
          <div className={classes.separator} />
          <div className={classes.versionDetailRow}>
            <div>{t('attribute:dialog.fields.options.label')}</div>
            <div>{detail.options.map(this.getOption)}</div>
          </div>
        </AccordionDetails>
      </Accordion>
    );
  }
}

export default withStyles(versionsStylesheet)(withFormatDate(Versions));

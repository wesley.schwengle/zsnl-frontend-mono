// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';

export const fetchProductChoices =
  (onError: OpenServerErrorDialogType) =>
  async (uuid: string, location_id: string) => {
    const body = await request<{
      result: { instance: { data: { id: string; label: string }[] } };
    }>(
      'GET',
      buildUrl(`/api/v1/sysin/interface/${uuid}/trigger/get_product_list`, {
        location_id,
      })
    ).catch(onError);

    return body
      ? body.result.instance.data.map(product => ({
          value: product.id,
          label: product.label,
        }))
      : [];
  };

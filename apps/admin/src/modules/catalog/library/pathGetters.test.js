// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getPathToItem } from './pathGetters';

describe('The `pathGetters` library module', () => {
  describe('The `getPathToItem` function', () => {
    const basePath = '/admin/catalogus';
    test('returns a full path when given a `id`', () => {
      expect(getPathToItem('test')).toEqual(`${basePath}/test`);
    });

    test('returns a base path when not given a `id`', () => {
      expect(getPathToItem()).toEqual(basePath);
    });
  });
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { SET_OBJECT_TYPE_UNSAVED_CHANGES } from './unsavedChanges.constants';

export type SetObjectTypeUnsavedChangesActionPayloadType = boolean;

export const setObjectTypeUnsavedChangesAction = (
  value: boolean
): ActionWithPayload<SetObjectTypeUnsavedChangesActionPayloadType, string> => ({
  type: SET_OBJECT_TYPE_UNSAVED_CHANGES,
  payload: value,
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormikProps } from 'formik';
import { useTranslation } from 'react-i18next';
import { UseSteppedFormReturnType } from '@zaaksysteem/common/src/components/form/hooks/useSteppedForm';
import Button from '@mintlab/ui/App/Material/Button';
import { useObjectTypeStepControlsStyles } from './ObjectTypeStepControls.style';

export type ObjectTypeStepControlsType = Pick<
  UseSteppedFormReturnType<any>,
  'handleNextStep' | 'handlePreviousStep' | 'hasNextStep' | 'hasPreviousStep'
> &
  Pick<FormikProps<any>, 'submitForm'> & {
    busy: boolean;
    activeStepValid: boolean;
  };

export const ObjectTypeStepControls: React.ComponentType<
  ObjectTypeStepControlsType
> = ({
  handleNextStep,
  handlePreviousStep,
  hasNextStep,
  hasPreviousStep,
  submitForm,
  activeStepValid,
  busy,
}) => {
  const [t] = useTranslation('objectTypeManagement');
  const classes = useObjectTypeStepControlsStyles();

  return (
    <div className={classes.wrapper}>
      <div className={classes.previousStepWrapper}>
        {hasPreviousStep && (
          <Button action={handlePreviousStep} presets={['text']}>
            {t('objectTypeManagement:form.controls.previous')}
          </Button>
        )}
      </div>

      <div className={classes.nextStepWrapper}>
        {hasNextStep ? (
          <Button
            disabled={!activeStepValid}
            action={handleNextStep}
            presets={['contained', 'primary']}
          >
            {t('objectTypeManagement:form.controls.next')}
          </Button>
        ) : (
          <Button
            disabled={busy || !activeStepValid}
            action={submitForm}
            presets={['contained', 'primary']}
          >
            {t('objectTypeManagement:form.controls.submit')}
          </Button>
        )}
      </div>
    </div>
  );
};

export default ObjectTypeStepControls;

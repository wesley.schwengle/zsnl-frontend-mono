// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CustomFieldTypeType } from '@zaaksysteem/common/src/types/CustomFields';

export type AttributeItemType = {
  label: string;
  name: string;
  id: string;
  draft: boolean;
  attribute_uuid: string;
  is_required: boolean;
  description: string;
  external_description: string;
  is_hidden_field: boolean;
  custom_field_type: CustomFieldTypeType;
  use_on_map?: boolean;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { APICatalog } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { AttributeItemType } from './types';

export const fetchAttributeChoices = (
  search_string: string
): Promise<ValueType<AttributeItemType>[]> =>
  request(
    'GET',
    buildUrl<APICatalog.SearchAttributeByNameRequestParams>(
      '/api/v2/admin/catalog/attribute_search',
      {
        search_string,
      }
    )
  ).then((response: APICatalog.SearchAttributeByNameResponseBody) => {
    return response.data.map(({ attributes: { name, value_type }, id }) => {
      return {
        value: {
          draft: true,
          id: id || '',
          attribute_uuid: id || '',
          name,
          label: '',
          is_required: false,
          description: '',
          external_description: '',
          is_hidden_field: false,
          custom_field_type: value_type,
        },
        label: name,
      };
    });
  });

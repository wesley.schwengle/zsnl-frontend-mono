// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { combineReducers } from 'redux';
import { users } from './users/users.reducer';
import { events } from './events/events.reducer';
import { filters } from './filters/filters.reducer';
import { eventsFetch } from './events/events.actions';
import { logMiddleware } from './log.middleware';

export const log = combineReducers({
  events,
  filters,
  users,
});

export const getLogModule = () => ({
  id: 'log',
  middlewares: [logMiddleware as any],
  initialActions: [eventsFetch({ user: {} } as any) as any],
  reducerMap: {
    log,
  },
});

export default getLogModule;

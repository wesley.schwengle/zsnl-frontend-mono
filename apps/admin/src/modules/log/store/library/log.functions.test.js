// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  getEventsQueryParams,
  shouldRedirect,
  shouldFetchEvents,
  shouldFetchUserLabel,
} from './log.functions';

describe('Log functions', () => {
  const path = '/admin/logboek';
  const completePath = `${path}/1/50`;

  describe('The `getEventsQueryParams` function that', () => {
    test('`returns an object with externalKeys instead of internalKeys`', () => {
      const page = 1;
      const rowsPerPage = 50;
      const keyword = 'foo';
      const caseNumber = '42';
      const user = {
        uuid: 42,
        label: 'Fred Foobar',
      };
      const data = { page, rowsPerPage, keyword, caseNumber, user };
      const actual = getEventsQueryParams(data);
      const expected = {
        page,
        rows_per_page: rowsPerPage,
        'query:match:keyword': keyword,
        'query:match:case_id': caseNumber,
        'query:match:subject': user.uuid,
      };

      expect(actual).toEqual(expected);
    });
  });

  describe('The `shouldRedirect` function', () => {
    test('returns `true` when a Log path does not contain page information', () => {
      expect(shouldRedirect(path)).toEqual(true);
    });

    test('returns `false` when a Log path contains page information', () => {
      expect(shouldRedirect(completePath)).toEqual(false);
    });
  });

  describe('The `shouldFetchEvents` function', () => {
    test('returns `true` when to path is in Log', () => {
      expect(shouldFetchEvents(completePath)).toEqual(true);
    });

    test('returns `false` when path is not in Log', () => {
      expect(shouldFetchEvents(path)).toEqual(false);
    });
  });

  describe('The `shouldFetchUserLabel` function', () => {
    test('returns `true` when filter user has an uuid and no label', () => {
      expect(
        shouldFetchUserLabel({
          log: {
            filters: {
              user: {
                uuid: '1',
                label: '',
              },
            },
          },
        })
      ).toEqual(true);
    });

    test('returns `false` when filter user has both an uuid and label', () => {
      expect(
        shouldFetchUserLabel({
          log: {
            filters: {
              user: {
                uuid: '1',
                label: 'Fred',
              },
            },
          },
        })
      ).toEqual(false);
    });
  });
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import { useTheme } from '@mui/material';

/**
 * @reactProps {Object} classes
 * @reactProps {string} baseClassName
 * @reactProps {Function} action
 * @reactProps {Object} exportParams
 * @reactProps {string} value
 * @return {ReactElement}
 */
export const ExportButton = ({ action, value }) => {
  const theme = useTheme();

  return (
    <div style={{ whiteSpace: 'nowrap' }}>
      <Button
        sx={{ backgroundColor: theme.palette.secondary.main }}
        action={() => action()}
        presets={['secondary', 'contained']}
      >
        {value}
      </Button>
    </div>
  );
};

export default ExportButton;

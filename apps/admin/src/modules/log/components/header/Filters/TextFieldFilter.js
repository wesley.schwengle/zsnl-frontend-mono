// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { createElement } from 'react';
import { withStyles } from '@mui/styles';
import Icon from '@mintlab/ui/App/Material/Icon';
import TextField from '@mintlab/ui/App/Material/TextField';
import { filterStyleSheet } from './Filter.style';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} props.name
 * @param {string} props.value
 * @param {string} props.startAdornmentName
 * @param {Function} props.endAdornmentAction
 * @param {string} props.placeholder
 * @param {Function} props.onChange
 * @param {Function} props.onKeyPress
 * @return {ReactComponent}
 */
export const TextFieldFilter = ({
  classes,
  name,
  value,
  startAdornmentName,
  endAdornmentAction,
  placeholder,
  onChange,
  onKeyPress,
}) => (
  <div className={classes.filterWrapper}>
    <TextField
      name={name}
      value={value}
      startAdornment={createElement(
        Icon,
        {
          size: 'small',
        },
        startAdornmentName
      )}
      closeAction={value ? () => endAdornmentAction(name) : null}
      placeholder={placeholder}
      onChange={onChange}
      onKeyPress={onKeyPress}
      scope={`log-filter:${name}`}
      variant="generic1"
    />
  </div>
);

export default withStyles(filterStyleSheet)(TextFieldFilter);

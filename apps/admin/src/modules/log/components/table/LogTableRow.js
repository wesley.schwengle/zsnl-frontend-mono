// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import deepmerge from 'deepmerge';
import { withStyles } from '@mui/styles';
import { TableRow, TableCell } from '@mintlab/ui/App/Material/Table';
import { LinkCell, DateCell } from '../../../../components/Table/cells';
import { logStyleSheet } from '../Log.style';
import { sharedStylesheet } from '../../../../components/Shared.style';

/**
 * @reactProps {integer} caseId
 * @reactProps {string} component
 * @reactProps {string} date
 * @reactProps {string} description
 * @reactProps {Object} user
 * @reactProps {Object} classes
 *
 * @returns {ReactElement}
 */
const LogTableRow = ({
  caseId,
  component,
  date,
  description,
  user,
  classes,
  t,
  ...rest
}) => (
  <TableRow {...rest}>
    <TableCell className={classes.otherCells}>
      <LinkCell
        path={`/intern/zaak/${caseId}`}
        value={caseId}
        classes={{
          link: classes.caseIdCell,
        }}
      />
    </TableCell>
    <TableCell className={classes.dateCell}>
      <DateCell value={date} t={t} />
    </TableCell>
    <TableCell className={classes.descriptionCell}>{description}</TableCell>
    <TableCell className={classes.componentCell}>
      {t([`log:components:${component}`, `log:components:unknown`])}
    </TableCell>
    <TableCell className={classes.otherCells}>{user.displayName}</TableCell>
  </TableRow>
);

const mergedStyles = theme =>
  deepmerge(sharedStylesheet(theme), logStyleSheet(theme));

export default withStyles(mergedStyles)(LogTableRow);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { numericUiToNumbers } from './LogContainer';

describe('The `LogContainer` module', () => {
  describe('The `numericUiToNumbers` function that', () => {
    test('`returns an array of numbers from a string of spaced numbers`', () => {
      const actual = numericUiToNumbers('-10 0 1 10 100');
      const expected = [-10, 0, 1, 10, 100]; // eslint-disable-line no-magic-numbers

      expect(actual).toEqual(expected);
    });
  });
});

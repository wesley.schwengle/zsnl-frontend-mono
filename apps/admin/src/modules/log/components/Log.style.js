// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import log from './log.svg';

const tableMaxWidth = '1600px';
const paginatorHeight = '45px';
const titleBarHeight = '72px';
const tablePadding = '24px';
const tableMargin = '20px';
const cardPadding = '8px';
const tableSideSpace = `${tableMargin} + ${cardPadding}`;
const paginatorSideSpace = `${tableSideSpace} + ${tablePadding}`;

/**
 * @return {JSS}
 */
export const logStyleSheet = ({
  mintlab: { greyscale },
  palette: { primary },
}) => ({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
  sheet: {
    height: `calc(100% - ${titleBarHeight} - ${paginatorHeight})`,
    margin: 'auto',
    'background-image': `url(${log})`,
    'background-size': '18px',
  },
  tableWrapper: {
    display: 'block',
    maxWidth: tableMaxWidth,
    overflowX: 'auto',
    width: `calc(100% - (${tableSideSpace}) * 2)`,
    minHeight: `calc(100% - ${paginatorHeight} + 9px)`,
    margin: `${tableMargin} auto 0 auto`,
  },
  descriptionCell: {
    maxWidth: '1px',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
  },
  caseIdCell: {
    color: primary.main,
    fontWeight: 'bold',
    textDecoration: 'none',
  },
  dateCell: {
    width: '150px',
  },
  componentCell: {
    color: greyscale.evenDarker,
    whiteSpace: 'nowrap',
    width: '1px',
  },
  otherCells: {
    width: '1px',
  },
  pagination: {
    width: `calc(100% - (${paginatorSideSpace}))`,
    position: 'fixed',
    bottom: '0px',
  },
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getLogModule } from './store/log.module';
import LogContainer from './components/LogContainer';

const LogModule: React.ComponentType<{}> = () => {
  return (
    //@ts-ignore
    <DynamicModuleLoader modules={[getLogModule()]}>
      <LogContainer />
    </DynamicModuleLoader>
  );
};

export default LogModule;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { caseTypeVersions } from './caseTypeVersions';
import { common } from './common';
import { server } from './server';
import { attribute } from './attribute';
import { validations } from './validations';
import { emailTemplate } from './emailTemplate';
import { folder } from './folder';
import { documentTemplate } from './documentTemplate';
import { changeOnlineStatus } from './changeOnlineStatus';

export default {
  nl: {
    ...caseTypeVersions,
    ...changeOnlineStatus,
    ...common,
    ...attribute,
    ...emailTemplate,
    ...folder,
    ...documentTemplate,
    ...server,
    ...validations,
  },
};

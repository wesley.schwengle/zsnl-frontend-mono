// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { connect } from 'react-redux';
import CatalogSearchContainer from '../../modules/catalog/components/Search/CatalogSearchContainer';
import { SECTION_CATALOG } from '../../constants/section';

const activeSearchFields = {
  [SECTION_CATALOG]: CatalogSearchContainer,
};

const SearchContainer = ({ section, t }) => {
  const Component = activeSearchFields[section];

  return Component ? <Component t={t} /> : null;
};

const mapStateToProps = ({ app: { section } }) => ({ section });

export default connect(mapStateToProps)(SearchContainer);

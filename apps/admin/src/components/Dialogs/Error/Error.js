// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { asArray } from '@mintlab/kitchen-sink/source';
import Alert from '@zaaksysteem/common/src/components/dialogs/Alert/Alert';

/**
 * Generic Error dialog {@link DialogWrapper}.
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Object} props.ui
 * @param {Function} props.t
 * @param {Function} props.hide
 * @param {Object} props.dialog
 * @return {ReactElement}
 */
const Error = ({ dialog, t, hide, classes }) => {
  const { message, fallback } = dialog.options;
  const hideDialog = () => hide();

  return (
    <Alert
      classes={{
        paper: classes.paper,
      }}
      open={true}
      title={t('server:errorTitle')}
      primaryButton={{
        text: t('dialog:ok'),
        onClick: hideDialog,
      }}
      onClose={hideDialog}
    >
      {asArray(message)
        .map(translationKey => t([translationKey, fallback]))
        .join(' ')}
    </Alert>
  );
};

export default Error;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const searchStyleSheet = ({ mintlab: { greyscale } }) => ({
  wrapper: {
    maxWidth: 700,
    margin: '0 auto',
  },
  icon: {
    color: greyscale.evenDarker,
  },
});

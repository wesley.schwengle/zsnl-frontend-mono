// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const dialogRendererStylesheet = () => ({
  paper: {
    width: '600px',
  },
});

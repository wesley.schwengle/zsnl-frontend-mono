// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import ReactDOM from 'react-dom';
import AdminApp from './App';
import { setupI18n } from './i18n';

window.onerror = null;

try {
  if (window.self !== window.top) {
    const slug = window.location.href.split('/catalogus')[1];
    window.top?.postMessage({ type: 'IFRAME:RELOAD', slug }, '*');
  } else {
    setupI18n().then(
      () =>
        void ReactDOM.render(<AdminApp />, document.getElementById('zs-app'))
    );
  }
} catch (err) {
  !!window;
  setupI18n().then(
    () => void ReactDOM.render(<AdminApp />, document.getElementById('zs-app'))
  );
}

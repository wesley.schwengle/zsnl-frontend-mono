// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import createUIReducer from '@zaaksysteem/common/src/store/ui/ui.reducer';
import { iframe } from './iframe/iframe.reducer';
import { banners } from './banners/banners.reducer';
import { drawer } from './drawer/drawer.reducer';

export const ui = createUIReducer({
  iframe,
  banners,
  drawer,
});

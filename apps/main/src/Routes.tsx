// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import { SessionRootStateType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { Layout } from './components/Layout/Layout';
import { useAppRootStyle } from './App.style';
import WidgetDummy from './modules/advancedSearch/WidgetDummy';

const CaseModule = React.lazy(
  () => import(/* webpackChunkName: "case" */ './modules/case')
);

const CaseComponentModule = React.lazy(
  () =>
    import(/* webpackChunkName: "case-component" */ './modules/case-components')
);

const TaskModule = React.lazy(
  () =>
    import(
      /* webpackChunkName: "task" */ './modules/dashboard/components/widgets/Tasks/Tasks'
    )
);

const DashboardModule = React.lazy(
  () => import(/* webpackChunkName: "dashboard" */ './modules/dashboard')
);

const CustomerContactModule = React.lazy(
  () =>
    import(
      /* webpackChunkName: "customer-contact" */ './modules/customerContact'
    )
);

const DocumentIntake = React.lazy(
  () => import(/* webpackChunkName: "intake" */ './modules/documentIntake')
);

const ObjectViewModule = React.lazy(
  () => import(/* webpackChunkName: "object" */ './modules/objectView')
);

const EmailTemplatePreviewer = React.lazy(
  () =>
    import(
      /* webpackChunkName: "demo" */ './components/EmailTemplatePreviewer/EmailTemplatePreviewer'
    )
);

const WebOdfEditor = React.lazy(
  () =>
    import(
      /* webpackChunkName: "demo" */ './components/WebOdfEditor/WebOdfEditor'
    )
);

const ContactViewModule = React.lazy(
  () => import(/* webpackChunkName: "contact-view" */ './modules/contactView')
);

const ContactSearchModule = React.lazy(
  () =>
    import(/* webpackChunkName: "contact-search" */ './modules/contactSearch')
);

const AdvancedSearchModule = React.lazy(
  () =>
    import(/* webpackChunkName: "advancedSearch" */ './modules/advancedSearch')
);

export interface RoutesPropsType {
  ready: boolean;
  prefix: string;
}

const MainRoutes: React.ComponentType<RoutesPropsType> = ({
  ready,
  prefix,
}) => {
  const classes = useAppRootStyle();
  if (!ready) {
    return <Loader delay={200} />;
  }

  return (
    <div className={classes.app}>
      <ErrorBoundary>
        <Router>
          <Routes>
            {/* VIEWS */}

            <Route
              path={`${prefix}/case/:caseUuid/*`}
              element={
                <Layout>
                  <CaseModule />
                </Layout>
              }
            />

            <Route
              path={`${prefix}/object/:uuid/*`}
              element={
                <Layout>
                  <ObjectViewModule />
                </Layout>
              }
            />

            <Route
              path={`${prefix}/dashboard`}
              element={
                <Layout>
                  <DashboardModule />
                </Layout>
              }
            />

            <Route
              path={`${prefix}/contact-view/:type/:uuid/*`}
              element={
                <Layout>
                  <ContactViewModule />
                </Layout>
              }
            />

            <Route
              path={`${prefix}/advanced-search/*`}
              element={
                <Layout>
                  <AdvancedSearchModule prefix={`${prefix}/advanced-search`} />
                </Layout>
              }
            />

            <Route
              path={`${prefix}/contact-search/*`}
              element={
                <Layout>
                  <ContactSearchModule />
                </Layout>
              }
            />

            <Route
              path={`${prefix}/communication/*`}
              element={
                <Layout>
                  <CustomerContactModule />
                </Layout>
              }
            />

            <Route
              path={`${prefix}/document-intake`}
              element={
                <Layout>
                  <DocumentIntake />
                </Layout>
              }
            />

            {/* IFRAMED COMPONENTS */}

            <Route
              path={`${prefix}/case-component/:caseUuid/*`}
              element={<CaseComponentModule />}
            />

            <Route
              path={`${prefix}/email-template-preview`}
              element={<EmailTemplatePreviewer />}
            />

            <Route path={`${prefix}/webodf`} element={<WebOdfEditor />} />

            <Route
              path={`${prefix}/dashboard/widgets/tasks/:widgetUuid`}
              element={<TaskModule />}
            />

            <Route
              path={`${prefix}/advanced-search/widgetdummy`}
              element={
                <Layout>
                  <WidgetDummy
                    identifier="9f2d2ecb-e37e-493f-b071-74cace295c36"
                    kind="custom_object"
                    page={1}
                    keyword="first second third"
                  />
                </Layout>
              }
            />
          </Routes>
        </Router>
      </ErrorBoundary>
    </div>
  );
};

type PropsFromState = Pick<RoutesPropsType, 'ready'>;

const mapStateToProps = ({
  session: { state, data },
}: SessionRootStateType): PropsFromState => {
  if (state !== AJAX_STATE_VALID || !data || !data.logged_in_user) {
    return {
      ready: false,
    };
  }

  return {
    ready: true,
  };
};

export default connect<PropsFromState, {}, {}, SessionRootStateType>(
  mapStateToProps
)(MainRoutes);

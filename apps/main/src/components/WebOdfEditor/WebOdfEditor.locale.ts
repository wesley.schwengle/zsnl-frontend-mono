// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const webOdfLocale = {
  nl: {
    save: 'Opslaan',
    undo: 'Ongedaan maken',
    redo: 'Opnieuw',
    format_bold: 'Vet',
    format_italic: 'Cursief',
    format_underlined: 'Onderstrepen',
    format_align_left: 'Links uitlijnen',
    format_align_right: 'Rechts uitlijnen',
    format_align_center: 'Centreren',
    format_align_justify: 'Uitgevuld',
    format_list_bulleted: 'Lijst met opsommingstekens',
    format_list_numbered: 'Genummerde lijst',
  },
};

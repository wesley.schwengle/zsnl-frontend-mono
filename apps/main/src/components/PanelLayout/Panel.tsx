// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classnames from 'classnames';
import { usePanelLayoutStyles } from './PanelLayout.styles';

type PanelType = {
  className?: string;
  type?: 'side' | 'fluid';
  children?: any;
};

export const Panel: React.ComponentType<PanelType> = ({
  type = 'fluid',
  children,
  className,
}) => {
  const classes = usePanelLayoutStyles();

  return (
    <div
      className={classnames(
        classes.panel,
        classes[type],
        className && className
      )}
    >
      {children}
    </div>
  );
};

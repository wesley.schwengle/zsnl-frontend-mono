// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useState, useEffect } from 'react';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import { useDebouncedCallback } from 'use-debounce';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useTheme } from '@mui/material';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { Theme } from '@mintlab/ui/types/Theme';
import { useTranslation } from 'react-i18next';
import {
  getChoices,
  getStyles,
  getFilters,
  MultiLineOptionWithExtension,
} from './library';
import { OptionType, FilterType } from './Search.types';
import { useSearchStyles } from './Search.styles';

const DELAY = 400;

const Search: React.ComponentType<{}> = () => {
  //search select
  const [input, setInput] = useState('');
  const [value, setValue] = useState(null as OptionType | null);
  const [choices, setChoices] = useState([] as OptionType[] | []);

  //filters select
  const [filter, setFilter] = useState(null as FilterType | null);
  const [filterOptions, setFilterOptions] = useState([] as FilterType[] | []);

  const [loading, setLoading] = useState(false);
  const [initializing, setInitializing] = useState(true);

  const [t] = useTranslation();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const theme = useTheme<Theme>();
  const classes = useSearchStyles();

  const [getChoicesDebounced] = useDebouncedCallback(async () => {
    async function getResults() {
      if (!filter) return [];
      let response = await getChoices(input, filter, openServerErrorDialog, t);
      return response;
    }

    setLoading(true);
    setChoices(await getResults());
    setLoading(false);
  }, DELAY);

  useEffect(() => {
    getChoicesDebounced();
  }, [input, filter]);

  useEffect(() => {
    async function start() {
      let response = await getFilters(openServerErrorDialog, t);
      setFilter(response[response.length - 1]);
      setFilterOptions(response);
      setInitializing(false);
    }
    start();
  }, []);

  const menuIsOpen =
    filter?.value === 'saved_searches' && input === '' ? true : undefined;

  return initializing ? (
    <Loader />
  ) : (
    <React.Fragment>
      <div className={classes.wrapper}>
        <Select
          generic={true}
          value={value}
          onChange={({ target: { value } }) => {
            setValue(value);
            if (value && value.url && window.top)
              window.top.location = value.url;
          }}
          choices={choices}
          isClearable={true}
          loading={loading}
          isMulti={false}
          components={{
            Option: MultiLineOptionWithExtension,
          }}
          styles={getStyles(theme)}
          startAdornment={<Icon size="small">{iconNames.search}</Icon>}
          translations={{
            'form:choose': t('common:dialog.search'),
            'form:loading': t('common:general.loading'),
            'form:beginTyping': '',
            'form:creatable': '',
            'form:create': '',
          }}
          filterOption={option => Boolean(option)}
          menuIsOpen={menuIsOpen}
          inputValue={input}
          onInputChange={(value, action) => {
            if (
              action.action !== 'input-blur' &&
              action.action !== 'menu-close'
            ) {
              setInput(value);
            }
          }}
        />
      </div>
      {ServerErrorDialog}

      <div className={classes.filters}>
        <Select
          generic={true}
          value={filter}
          onChange={({ target: { value } }) => setFilter(value)}
          choices={filterOptions}
          isMulti={false}
          filterOption={option => Boolean(option)}
        />
      </div>
    </React.Fragment>
  );
};

export default Search;

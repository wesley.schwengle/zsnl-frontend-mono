// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';

import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import AddElement from '@zaaksysteem/common/src/components/dialogs/AddElement/AddElement';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { CaseCreateDialog } from '@zaaksysteem/common/src/components/dialogs/CaseCreate/CaseCreateDialog';
import { ContactMomentCreateDialog } from '@zaaksysteem/common/src/components/dialogs/ContactMomentCreate/ContactMomentCreateDialog';
import { useTopBarStyles } from './TopBar.styles';
import Search from './Search/Search';
import { getSections } from './TopBar.library';
import locale from './TopBar.locale';
import { contactSelector, capabilitiesSelector } from './TopBar.selectors';
import Breadcrumbs, { BreadcrumbsPropsType } from './Breadcrumbs/Breadcrumbs';

export type TopBarPropsType = BreadcrumbsPropsType;

const TopBar: React.ComponentType<TopBarPropsType> = ({ title, subTitle }) => {
  const [t] = useTranslation('topBar');
  const classes = useTopBarStyles();
  const [addElementOpen, setAddElementOpen] = useState(false);
  const [createCaseOpen, setCreateCaseOpen] = useState(false);
  const [createContactMomentOpen, setCreateContactMomentOpen] = useState(false);
  const capabilities = useSelector(capabilitiesSelector);
  const contact = useSelector(contactSelector);

  useEffect(() => {
    const pageTitle = title;
    const systemName = t('common:systemName');

    document.title = pageTitle ? `${pageTitle} - ${systemName}` : systemName;
  }, []);

  return (
    <header className={classes.wrapper}>
      <div className={classes.breadCrumbs}>
        <Breadcrumbs title={title} subTitle={subTitle} />
      </div>
      <div className={classes.search}>
        <Search />
      </div>
      <div>
        <Button
          presets={['primary', 'contained']}
          icon="add"
          action={() => setAddElementOpen(true)}
        >
          {t('new')}
        </Button>
      </div>
      {addElementOpen ? (
        <AddElement
          t={t}
          hide={() => setAddElementOpen(false)}
          sections={getSections({
            t,
            contactAllowed: capabilities.includes('contact_nieuw'),
            setCreateCaseOpen,
            setCreateContactMomentOpen,
            setAddElementOpen,
          })}
          title={t('new')}
        />
      ) : null}

      <CaseCreateDialog
        open={createCaseOpen}
        onClose={() => setCreateCaseOpen(false)}
        {...(contact && { contact })}
      />

      <ContactMomentCreateDialog
        open={createContactMomentOpen}
        onClose={() => setCreateContactMomentOpen(false)}
        {...(contact && { contact })}
      />
    </header>
  );
};

/* eslint-disable-next-line */
export default (props: TopBarPropsType) => (
  <I18nResourceBundle resource={locale} namespace="topBar">
    <TopBar {...props} />
  </I18nResourceBundle>
);

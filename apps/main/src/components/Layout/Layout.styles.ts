// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useLayoutStyles = makeStyles({
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    width: '100vw',
    height: '100vh',
  },
  content: {
    display: 'flex',
    flex: 1,
    overflow: 'auto',
  },
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import { useNotificationBarStyles } from './NotificationBar.styles';

export type NotificationType = {
  message: string;
  button?: {
    label: string;
    action: () => void;
  };
};

type NotificationBarPropsType = {
  notifications: NotificationType[];
};

const NotificationBar: React.ComponentType<NotificationBarPropsType> = ({
  notifications,
}) => {
  const classes = useNotificationBarStyles();
  const [hiddenList, setHiddenList] = useState<string[]>([]);
  const hideNotification = (notification: string) => {
    setHiddenList(hiddenList.concat(notification));
  };

  const notificationList = notifications.filter(
    notification => !hiddenList.includes(notification.message)
  );

  if (!notificationList.length) return null;

  return (
    <div className={classes.wrapper}>
      {notificationList.map((notification, index) => (
        <div key={index} className={classes.notificationWrapper}>
          <div className={classes.notification}>
            <span>{notification.message}</span>
            {notification.button && (
              <Button
                className={classes.actionButton}
                presets={['outlined']}
                action={() => notification.button?.action()}
              >
                {notification.button.label}
              </Button>
            )}
          </div>
          <Button
            className={classes.closeButton}
            presets={['icon', 'extraSmall']}
            action={() => hideNotification(notification.message)}
          >
            close
          </Button>
        </div>
      ))}
    </div>
  );
};

export default NotificationBar;

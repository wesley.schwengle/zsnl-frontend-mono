// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
// @ts-ignore
import documentExplorerLocale from '@zaaksysteem/common/src/components/DocumentExplorer/locale/locale';
import { useUnauthorizedBanner } from './../../library/useUnauthorizedBanner';
import locale from './DocumentIntake.locale';
import DocumentIntake from './DocumentIntake';

export default function DocumentIntakeModule() {
  const Banner = useUnauthorizedBanner((systemRoles, capabilities) =>
    capabilities?.includes('documenten_intake_subject')
  );

  return Banner ? (
    Banner
  ) : (
    <I18nResourceBundle
      resource={documentExplorerLocale}
      namespace="DocumentExplorer"
    >
      <I18nResourceBundle resource={locale} namespace="documentIntake">
        <DocumentIntake />
      </I18nResourceBundle>
    </I18nResourceBundle>
  );
}

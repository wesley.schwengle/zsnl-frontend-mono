// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { rejectDocument } from './RejectDialog.library';
import { RejectDialogPropsType } from './RejectDialog.types';
import { rejectDialogFormDefinition } from './RejectDialog.formDefinition';

const RejectDialog: React.ComponentType<RejectDialogPropsType> = ({
  selectedDocuments,
  open,
  onClose,
  onConfirm,
}) => {
  const [saving, setSaving] = React.useState(false);
  const [t] = useTranslation('documentIntake');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  return (
    <React.Fragment>
      <FormDialog
        formDefinition={rejectDialogFormDefinition}
        formDefinitionT={t}
        title={t('rejectDocument.title')}
        icon="insert_drive_file"
        onClose={onClose}
        saving={saving}
        scope="reject-document"
        open={open}
        onSubmit={formValues => {
          const { reason } = formValues;

          setSaving(true);
          rejectDocument({
            reason,
            uuid: selectedDocuments[0].uuid as any,
          })
            .then(onConfirm)
            .catch(openServerErrorDialog)
            .then(() => setSaving(false));
        }}
      />
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default RejectDialog;

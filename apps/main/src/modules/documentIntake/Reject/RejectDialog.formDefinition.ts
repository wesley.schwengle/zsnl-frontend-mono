// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { FormValuesType } from './RejectDialog.types';

export const rejectDialogFormDefinition: FormDefinition<FormValuesType> = [
  {
    name: 'reason',
    type: fieldTypes.TEXT,
    value: null,
    required: true,
    placeholder: 'rejectDocument.reason',
    label: 'rejectDocument.reason',
    translations: {
      'form:choose': 'assignDocument.role.placeholder',
    },
  },
];

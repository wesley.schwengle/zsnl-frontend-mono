// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import * as i18next from 'i18next';
import {
  Rule,
  showFields,
  hideFields,
  setFieldValue,
  updateField,
  transferDataAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import { normalizeValue } from '@zaaksysteem/common/src/library/normalizeValue';

export const getRules = ({ t }: { t: i18next.TFunction }) => [
  new Rule()
    .when('origin', field =>
      Boolean(
        normalizeValue(field.value) &&
          String(normalizeValue(field.value)).toLowerCase() !== 'intern'
      )
    )
    .then(showFields(['date']))
    .else(hideFields(['date']))
    .and(setFieldValue('date', null)),
  new Rule()
    .when(
      'origin',
      field => String(normalizeValue(field.value)).toLowerCase() === 'inkomend'
    )
    .then(
      updateField('date', {
        label: t(
          'documentIntake:properties.additional.date.incomingDate'
        ) as string,
      })
    ),
  new Rule()
    .when(
      'origin',
      field => String(normalizeValue(field.value)).toLowerCase() === 'uitgaand'
    )
    .then(
      updateField('date', {
        label: t(
          'documentIntake:properties.additional.date.outgoingDate'
        ) as string,
      })
    ),
  new Rule()
    .when('case_uuid', () => true)
    .then(
      transferDataAsConfig('case_uuid', 'document_label_uuids', 'caseUuid')
    ),
];

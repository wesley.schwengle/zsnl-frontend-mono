// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { FormValuesType } from './AssignDialog.types';

export const assignDialogFormDefinition: FormDefinition<FormValuesType> = [
  {
    name: 'type',
    type: fieldTypes.RADIO_GROUP,
    value: 'user',
    required: true,
    label: 'assignDocument.type',
    choices: [
      {
        label: 'assignDocument.contact.user',
        value: 'user',
      },
      {
        label: 'assignDocument.contact.role',
        value: 'role',
      },
    ],
  },
  {
    name: 'contact',
    type: fieldTypes.CONTACT_FINDER,
    value: null,
    required: false,
    label: 'assignDocument.contact.label',
    translations: {
      'form:choose': 'assignDocument.contact.placeholder',
    },
    config: {
      subjectTypes: ['employee'],
    },
  },
  {
    name: 'group',
    type: fieldTypes.DEPARTMENT_FINDER,
    value: null,
    required: false,
    translations: {
      'form:choose': 'assignDocument.group.placeholder',
    },
    label: 'assignDocument.group.label',
  },
  {
    name: 'role',
    type: fieldTypes.ROLE_FINDER,
    value: null,
    required: false,
    placeholder: 'selectRole',
    label: 'assignDocument.role.label',
    translations: {
      'form:choose': 'assignDocument.role.placeholder',
    },
  },
];

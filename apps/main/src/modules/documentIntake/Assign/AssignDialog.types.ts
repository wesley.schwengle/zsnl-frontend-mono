// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ContactType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';
import { DepartmentFinderOptionType } from '@zaaksysteem/common/src/components/form/fields/DepartmentFinder/DepartmentFinder.types';

export type AssignType = 'user' | 'role';

export type FormValuesType = {
  type: AssignType;
  contact: ContactType;
  group: DepartmentFinderOptionType;
  role: string;
};

export type AssignDialogPropsType = {
  selectedDocuments: { uuid: string; name: string }[];
  open: boolean;
  onClose: () => void;
  onConfirm: () => void;
  container?: React.ReactInstance | null;
};

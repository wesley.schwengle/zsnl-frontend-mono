// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APIDocument } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';

export const assignDocumentContact = ({
  uuid,
  contact,
}: {
  uuid: string;
  contact: string;
}): Promise<APIDocument.AssignDocumentToUserResponseBody> =>
  request<APIDocument.AssignDocumentToUserResponseBody>(
    'POST',
    '/api/v2/document/assign_document_to_user',
    {
      document_uuid: uuid,
      user_uuid: contact,
    }
  ).then(response => {
    return response.data || [];
  });

export const assignDocumentRole = ({
  uuid,
  group,
  role,
}: {
  uuid: string;
  role: string;
  group: string;
}): Promise<APIDocument.AssignDocumentToRoleResponseBody> =>
  request<APIDocument.AssignDocumentToRoleResponseBody>(
    'POST',
    '/api/v2/document/assign_document_to_role',
    {
      document_uuid: uuid,
      group_uuid: group,
      role_uuid: role,
    }
  ).then(response => {
    return response.data || [];
  });

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles(({ typography }: Theme) => ({
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexFlow: 'column',
  },
  content: {
    flexGrow: 1,
    height: 1,
  },
  view: {
    margin: '20px 20px 0 20px',
    flexGrow: 1,
  },
  searchWrapper: {
    maxWidth: 800,
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
  },
  resultsWrapper: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
  },
  button: {
    width: 200,
  },
  separator: {
    marginBottom: 20,
    fontWeight: typography.fontWeightBold,
  },
}));

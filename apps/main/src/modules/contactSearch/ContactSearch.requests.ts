// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';

type SearchContactV1Type = (data: any) => Promise<any>;

export const searchContactV1: SearchContactV1Type = async data => {
  const url = buildUrl('/api/v1/subject', { rows_per_page: 11 });
  const response = await request('POST', url, data);

  return response?.result?.instance?.rows;
};

type SearchContactV0Type = (data: any) => Promise<any>;

export const searchContactV0: SearchContactV0Type = async data => {
  const url = buildUrl('/objectsearch/contact/medewerker', {
    zapi_num_rows: 11,
  });
  const response = await request('POST', url, data);

  return response?.json?.entries;
};

export const fetchDepartments = async () => {
  const url = buildUrl('/api/v1/group', { rows_per_page: 250 });
  const response = await request('GET', url);

  return response?.result?.instance?.rows;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    sideMenu: {
      data: 'Gegevens',
      communication: 'Communicatie',
      cases: 'Zaken',
      map: 'Kaart',
      relationships: 'Relaties',
      timeline: 'Tijdlijn',
      environments: 'Omgevingen',
    },
    location: {
      no_address: 'Er zijn geen adresgegevens bekend.',
    },
    save: 'Opslaan',
    totalResults: '{{count}} resultaat in totaal',
    notifications: {
      secret: 'Betrokkene heeft een indicatie "Geheim"',
      internalNote: 'Interne notitie',
    },
  },
};

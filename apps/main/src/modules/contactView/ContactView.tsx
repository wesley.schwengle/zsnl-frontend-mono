// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Routes, Route, useParams, Navigate } from 'react-router-dom';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { PanelLayout } from '../../components/PanelLayout/PanelLayout';
import { Panel } from '../../components/PanelLayout/Panel';
import TopBar from '../../components/TopBar/TopBar';
import NotificationBar from '../../components/NotificationBar/NotificationBar';
import { sessionSelector } from '../../store/session/session.selectors';
import Information from './information/Information';
import Communication from './communication/Communication';
import LocationModule from './location/Location';
import Relationship from './relationship/Relationship';
import Environments from './environments';
import Cases from './cases/Cases';
import { ContactViewSideMenu } from './ContactViewSideMenu';
import { useStyles } from './ContactView.styles';
import ContactTimeline from './timeline';
import {
  formatTitle,
  formatSubTitle,
  getNotifications,
} from './ContactView.library';
import { SubjectType } from './ContactView.types';
import { fetchSubject } from './ContactView.requests';

export interface ContactViewPropsType {}

export type ContactViewParamsType = {
  uuid: string;
  type: SubjectTypeType;
  ['*']:
    | 'data'
    | 'communication'
    | 'cases'
    | 'map'
    | 'relationships'
    | 'timeline';
};

const ContactView: React.FunctionComponent<ContactViewPropsType> = () => {
  const { uuid, type } = useParams<
    keyof ContactViewParamsType
  >() as ContactViewParamsType;
  const classes = useStyles();
  const [t] = useTranslation('contactView');
  const session = useSelector(sessionSelector);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [subject, setSubject] = useState<SubjectType>();

  const getSubject = async () => {
    const subj = await fetchSubject(uuid, type).catch(openServerErrorDialog);

    setSubject(subj);
  };

  useEffect(() => {
    getSubject();
  }, []);

  if (!subject || !session) {
    return (
      <div className={classes.wrapper}>
        {ServerErrorDialog}
        <Loader />
      </div>
    );
  }

  return (
    <div className={classes.wrapper}>
      <TopBar title={formatTitle(subject)} subTitle={formatSubTitle(subject)} />
      <NotificationBar notifications={getNotifications(t, subject)} />
      <div className={classes.content}>
        <PanelLayout>
          <Panel type="side">
            <ContactViewSideMenu session={session} />
          </Panel>
          <Panel className={classes.view}>
            <Routes>
              <Route path="" element={<Navigate to="data" replace={true} />} />
              <Route
                path={'data/*'}
                element={
                  <Information
                    subject={subject}
                    refreshSubject={getSubject}
                    session={session}
                  />
                }
              />
              <Route
                path={'communication/*'}
                element={<Communication uuid={uuid} />}
              />
              <Route path={`cases`} element={<Cases subject={subject} />} />
              <Route
                path={'map/*'}
                element={<LocationModule subject={subject} />}
              />
              <Route path={'relationships/*'} element={<Relationship />} />
              <Route
                path={'timeline/*'}
                element={<ContactTimeline uuid={uuid} type={type} />}
              />
              <Route
                path={`environments`}
                element={<Environments uuid={uuid} />}
              />
            </Routes>
          </Panel>
        </PanelLayout>
      </div>
    </div>
  );
};

export default ContactView;

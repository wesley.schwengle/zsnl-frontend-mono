// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from './Environments.locale';
import Environments, { EnvironmentsPropsType } from './Environments';

const ContactEnvironments: React.FunctionComponent<
  EnvironmentsPropsType
> = props => {
  return (
    <I18nResourceBundle resource={locale} namespace="environments">
      <Environments {...props} />
    </I18nResourceBundle>
  );
};

export default ContactEnvironments;

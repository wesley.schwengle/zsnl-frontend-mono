// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { OtapType } from './Environments.types';

export const fetchSubjectV1 = async (uuid: string) => {
  const url = `/api/v1/subject/${uuid}`;

  const response = await request('GET', url);

  return response.result.instance;
};

export const fetchControlPanel = async (id: string) => {
  const url = `/api/v1/controlpanel?zql=SELECT+%7B%7D+FROM+controlpanel+WHERE+owner+%3D+%22${id}%22`;

  const response = await request('GET', url);

  return response;
};

export const setCustomerType = async (id: string, type: string) => {
  const url = '/api/v1/controlpanel/create';
  const data = {
    owner: id,
    customer_type: type,
  };

  const response = await request('POST', url, data);

  return response;
};

export const fetchEnvironments = async (controlPanelUuid: string) => {
  const url = `/api/v1/controlpanel/${controlPanelUuid}/instance`;

  const response = await request('GET', url);

  return response.result.instance.rows;
};

export const fetchHosts = async (controlPanelUuid: string) => {
  const url = buildUrl(`/api/v1/controlpanel/${controlPanelUuid}/host`, {
    rows_per_page: 100,
  });

  const response = await request('GET', url);

  return response.result.instance.rows;
};

export const updateEnvironment = async (
  controlPanelUuid: string,
  data:
    | {
        fqdn: string;
        label: string;
        otap: OtapType;
        password: string;
        template: string;
      }
    | {
        disabled: boolean;
      }
    | {
        protected: boolean;
      },
  environmentUuid?: string
) => {
  const url = environmentUuid
    ? `/api/v1/controlpanel/${controlPanelUuid}/instance/${environmentUuid}/update`
    : `/api/v1/controlpanel/${controlPanelUuid}/instance/create`;

  const response = await request('POST', url, data);

  return response;
};

export const saveHost = async (
  controlPanelUuid: string,
  actionType: 'create' | 'update',
  data: any,
  hostUuid?: string
) => {
  const hostUuidPath = hostUuid ? `${hostUuid}/` : '';
  const url = `/api/v1/controlpanel/${controlPanelUuid}/host/${hostUuidPath}${actionType}`;

  const response = await request('POST', url, data);

  return response;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { ControlPanelType } from '../Environments.types';
import { getOverviewDialogFormDefinition } from './OverviewDialog.formDefinition';
import { useStyles } from './Overview.styles';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
});

type OverviewDialogPropsType = {
  controlPanel: ControlPanelType;
  onClose: () => void;
  open: boolean;
};

const HostsDialog: React.FunctionComponent<OverviewDialogPropsType> = ({
  controlPanel,
  onClose,
  open,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('environments');
  const dialogEl = useRef();

  const title = t('overview.dialog.title');
  const formDefinition = getOverviewDialogFormDefinition(t, controlPanel);

  let {
    fields,
    formik: { /* values, */ isValid },
  } = useForm({
    formDefinition,
  });

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'environments-overview-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="settings"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent padded={true}>
          <div className={classes.formWrapper}>
            {fields.map(
              ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
                const props = cloneWithout(rest, 'mode');

                return (
                  <FormControlWrapper
                    {...props}
                    label={suppressLabel ? false : props.label}
                    compact={true}
                    key={`${props.name}-formcontrol-wrapper`}
                  >
                    <FieldComponent
                      {...props}
                      t={t}
                      containerRef={dialogEl.current}
                    />
                  </FormControlWrapper>
                );
              }
            )}
          </div>
        </DialogContent>
        <>
          <DialogDivider />
          <DialogActions>
            {getDialogActions(
              {
                text: title,
                disabled: !isValid,
                action() {
                  // not developed yet
                },
              },
              {
                text: t('common:dialog.cancel'),
                action: onClose,
              },
              'environments-overview-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
    </>
  );
};

export default HostsDialog;

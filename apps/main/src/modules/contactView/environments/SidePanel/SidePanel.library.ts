// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { ControlPanelType, EnvironmentType } from '../Environments.types';

export const getOverviewItems = (
  t: i18next.TFunction,
  controlPanel: ControlPanelType
) => [
  {
    label: t('overview.template'),
    value: controlPanel.template,
  },
  {
    label: t('overview.shortname'),
    value: controlPanel.shortname,
  },
  {
    label: t('overview.customerType'),
    value: t(`customerTypes.${controlPanel.customerType}`),
  },
  {
    label: t('overview.readOnly'),
    value: controlPanel.readOnly ? t('common:yes') : t('common:no'),
  },
];

export const getDiskSpace = (
  controlPanel: ControlPanelType,
  environments: EnvironmentType[]
) => {
  const allowedDiskspace = controlPanel.allowedDiskspace;
  const allowed =
    allowedDiskspace || allowedDiskspace === 0 ? allowedDiskspace : '∞';
  const usedDiskSpace = environments.reduce(
    (diskspace, environment) => diskspace + environment.diskspace,
    0
  );

  return `${usedDiskSpace} / ${allowed} GB`;
};

export const getEnvironmentCounter = (
  controlPanel: ControlPanelType,
  environments: EnvironmentType[]
) => {
  const allowedEnvironments = controlPanel.allowedEnvironments;
  const usedEnvironments = environments.length;

  return usedEnvironments
    ? `${usedEnvironments} / ${allowedEnvironments}`
    : '-';
};

export const getUptime = (environments: EnvironmentType[]) => {
  return environments.length ? '99,98%' : '-';
};

// the host is assigned to an environment when creating or editing the host
// but the host itself does not know to which environment it is assigned
// so we have to skim through all the environments to find the one that has the host
export const getEnvironmentUuid = (
  currentHostFqdn: string,
  environments: EnvironmentType[]
) => {
  const match = environments.find(environment => {
    const hostFqdns = environment.hosts.map(host => host.fqdn);
    const environmentHasCurrentHost = hostFqdns.includes(currentHostFqdn);

    return environmentHasCurrentHost;
  });

  return match ? match.uuid : null;
};

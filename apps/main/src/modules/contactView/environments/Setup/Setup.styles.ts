// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
  },
  intro: {
    margin: 20,
  },
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'column',
    gap: 10,
    margin: 20,
  },
  button: {
    width: 300,
  },
}));

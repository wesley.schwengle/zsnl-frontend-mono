// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { setCustomerType } from '../Environments.requests';
import { useStyles } from './Setup.styles';

type SetupPropsType = {
  id: string;
  setLoading: (boolean: true) => void;
  refreshControlPanel: () => void;
};

const customerTypes = [
  'government',
  'commercial',
  'lab',
  'development',
  'staging',
  'acceptance',
  'testing',
  'preprod',
  'production',
];

const Setup: React.FunctionComponent<SetupPropsType> = ({
  id,
  setLoading,
  refreshControlPanel,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('environments');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  return (
    <div className={classes.wrapper}>
      <div className={classes.intro}>{t('activate.intro')}</div>
      <div className={classes.buttonWrapper}>
        {customerTypes.map((customerType, index) => {
          const noun = t(`customerTypes.${customerType}`);
          const verb = t('activate.activate');
          const label = `${noun} ${verb}`;

          return (
            <Button
              key={index}
              action={() => {
                setLoading(true);
                setCustomerType(id, customerType)
                  .then(refreshControlPanel)
                  .catch(openServerErrorDialog);
              }}
              scope={`contactView-environment-create-${customerType}`}
              presets={['primary', 'contained']}
              classes={{ root: classes.button }}
            >
              {label}
            </Button>
          );
        })}
      </div>
      {ServerErrorDialog}
    </div>
  );
};

export default Setup;

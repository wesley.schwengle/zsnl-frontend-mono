// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { GeoMap } from '@mintlab/ui/App/External/GeoMap';
import { IntegrationData } from '@mintlab/ui/types/MapIntegration';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { SubjectType } from '../ContactView.types';
import { fetchContactAddress } from './Location.actions';
import { useLocationStyles } from './Location.style';

type SubjectPropsType = {
  subject: SubjectType;
};

export const Location: React.FunctionComponent<SubjectPropsType> = ({
  subject,
}) => {
  const [t] = useTranslation('contactView');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useLocationStyles();

  const [loading, setLoading] = useState<boolean>(true);
  const [contact, setContact] = useState<IntegrationData['contact']>();

  const contactType = subject.type;

  useEffect(() => {
    fetchContactAddress(subject, openServerErrorDialog)
      .then(response => {
        setContact(response);
        setLoading(false);
      })
      .catch(openServerErrorDialog);
  }, []);

  if (loading) {
    return (
      <>
        <Loader />
        {ServerErrorDialog}
      </>
    );
  } else if (!contact) {
    return <div className={classes.wrapper}>{t('location.no_address')}</div>;
  } else {
    return (
      <GeoMap
        geoFeature={contact.geojson || null}
        name="ContactMap"
        canDrawFeatures={false}
        minHeight="100%"
        context={{
          type: 'ContactMap',
          data: { contact, contactType },
        }}
      />
    );
  }
};

export default Location;

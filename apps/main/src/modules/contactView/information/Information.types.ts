// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { APICaseManagement } from '@zaaksysteem/generated';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';

export type InformationPropTypes = {
  uuid: string;
  type: SubjectTypeType;
  Table: JSX.Element;
};

export type GetFormDefinitionType = ({
  t,
  subject,
  canEdit,
  hasEditRights,
  session,
  contactChannelEnabled,
}: {
  t: i18next.TFunction;
  subject: any;
  canEdit?: boolean;
  hasEditRights?: boolean;
  session: SessionType;
  contactChannelEnabled?: boolean;
}) => AnyFormDefinitionField[];

export type ObjectType = APICaseManagement.GetCustomObjectResponseBody;
export type ObjectTypeType = APICaseManagement.GetCustomObjectTypeResponseBody;

export type AltAuthIntegrationType = {
  interface_config: {
    enable_for: 'persons' | 'companies' | 'both' | null;
  };
};

export type AltAuthDataType = {
  id: number;
  subject_type: 'natuurlijk_persoon' | 'bedrijf';
  active: '0' | '1';
  username?: string;
  phone?: string;
  [key: string]: any;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import {
  AltAuthDataType,
  ObjectType,
  ObjectTypeType,
} from '../Information.types';
import Common from '../tables/common/Common';
import Additional from '../tables/additional/Additional';
import RelatedObject from '../tables/relatedObject/RelatedObject';
import AltAuth from '../tables/altAuth/AltAuth';
import Special from '../tables/special/Special';
import { SubjectType } from './../../ContactView.types';

export interface OrganizationViewPropsType {
  data: {
    subject: SubjectType;
    object: ObjectType;
    objectType: ObjectTypeType;
    altAuthData: AltAuthDataType;
  };
  refreshSubject: () => {};
  setSnackOpen: any;
  session: SessionType;
}

const OrganizationView: React.FunctionComponent<OrganizationViewPropsType> = ({
  data: { subject, object, objectType, altAuthData },
  refreshSubject,
  setSnackOpen,
  session,
}) => (
  <>
    <Common
      subject={subject}
      refreshSubject={refreshSubject}
      setSnackOpen={setSnackOpen}
      session={session}
    />
    <Additional
      subject={subject}
      refreshSubject={refreshSubject}
      setSnackOpen={setSnackOpen}
      session={session}
    />
    {altAuthData && <AltAuth altAuthData={altAuthData} />}
    {object && <RelatedObject object={object} objectType={objectType} />}
    <Special subject={subject} />
  </>
);

export default OrganizationView;

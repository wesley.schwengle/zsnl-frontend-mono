// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { useTranslation } from 'react-i18next';
import { FormikValues } from 'formik';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FileUploadDialog } from '@zaaksysteem/common/src/components/dialogs/FileUploadDialog/FileUploadDialog';
import Button from '@mintlab/ui/App/Material/Button';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { useInformationStyles } from '../../Information.style';
import {
  deleteSignatureAction,
  uploadSignatureAction,
} from '../../Information.requests';
import { SubjectType } from './../../../ContactView.types';
import { canUpload, formatSubtitle } from './library';

type SignaturePropsType = {
  subject: SubjectType;
  session: SessionType;
  lookingAtSelf: boolean;
};

const Signature: React.FunctionComponent<SignaturePropsType> = ({
  subject,
  session,
  lookingAtSelf,
}) => {
  const [t] = useTranslation('information');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useInformationStyles();
  const [signatureId, setSignatureId] = useState(subject.settings.signatureId);
  const [dialog, setDialog] = useState(false);
  const signatureUploadRole = session.configurable.signature_upload_role;

  const canEdit = canUpload(
    session.logged_in_user.system_roles,
    signatureUploadRole,
    lookingAtSelf
  );
  const imageSrc = buildUrl(`/api/betrokkene/signature/for_user`, {
    uuid: subject.uuid,
    refreshTrigger: signatureId,
  });

  const deleteSignature = async () => {
    await deleteSignatureAction(subject.uuid).catch(openServerErrorDialog);

    setSignatureId(null);
  };

  const uploadSignature = async (formValues: FormikValues) => {
    const fileUuid = formValues.files.value;

    await uploadSignatureAction(subject.uuid, fileUuid).catch(
      openServerErrorDialog
    );

    setSignatureId(fileUuid);
    setDialog(false);
  };

  const subTitle = formatSubtitle(t, canEdit, signatureUploadRole);

  return (
    <>
      {ServerErrorDialog}
      <SubHeader title={t('signature.title')} description={subTitle} />
      {(signatureId || canEdit) && (
        <div>
          {signatureId && (
            <>
              <img alt={t('signature.signature')} src={imageSrc} />
              {canEdit && (
                <div className={classes.buttonWrapper}>
                  <Button
                    action={() => {
                      setDialog(true);
                    }}
                    presets={['primary', 'contained']}
                    className={classes.button}
                  >
                    {t('signature.replace')}
                  </Button>
                  {
                    <Button
                      action={deleteSignature}
                      presets={['primary', 'contained']}
                      className={classes.button}
                    >
                      {t('signature.delete')}
                    </Button>
                  }
                </div>
              )}
            </>
          )}{' '}
          {!signatureId && (
            <Button
              action={() => {
                setDialog(true);
              }}
              presets={['primary', 'contained']}
            >
              {t('signature.upload')}
            </Button>
          )}
        </div>
      )}
      <FileUploadDialog
        onConfirm={uploadSignature}
        onClose={() => setDialog(false)}
        open={dialog}
        multiValue={false}
      />
    </>
  );
};

export default Signature;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useAttributesStyles = makeStyles(
  ({ mintlab: { greyscale, radius } }: Theme) => ({
    wrapper: {
      width: '100%',
      padding: '20px 32px',
      display: 'flex',
      flexDirection: 'column',
      maxWidth: '930px',
      gap: 20,
    },
    header: {
      display: 'flex',
      justifyContent: 'space-between',
    },
    buttons: {
      display: 'flex',
      justifyContent: 'flex-end',
      gap: '20px',
      marginLeft: '30px',
    },
    button: {
      backgroundColor: greyscale.dark,
      boxShadow: 'none',
      borderRadius: radius.buttonSmall,
      width: 120,
      marginLeft: 10,
    },
    colContent: {
      flex: 1,
    },
  })
);

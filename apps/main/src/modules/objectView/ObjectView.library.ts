// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import {
  fetchObject,
  updateObject,
  fetchObjectType,
  fetchCaseRelations,
  fetchObjectRelations,
  fetchSubjectRelations,
} from './ObjectView.requests';
import {
  ObjectType,
  ObjectTypeType,
  RelatedCaseRowType,
  RelatedObjectRowType,
  RelatedSubjectRowType,
} from './ObjectView.types';

export const getObject = async (
  uuid: string,
  setObject: (object: ObjectType) => void
) => {
  const object = await fetchObject(uuid);

  const {
    id,
    attributes: {
      custom_fields,
      date_created,
      last_modified,
      status,
      title,
      version_independent_uuid,
    },
    relationships,
    meta,
  } = object;

  const relatedCasesUuids = relationships?.cases
    ?.map((caseRelation: any) => caseRelation.data.id || '')
    .filter(Boolean);

  setObject({
    uuid: id,
    customFieldsValues: custom_fields || {},
    lastModified: last_modified,
    registrationDate: date_created,
    title: title || '',
    objectTypeVersionUuid:
      object.relationships?.custom_object_type?.data.id || '',
    versionIndependentUuid: version_independent_uuid || '',
    status,
    relatedCasesUuids,
    authorizations: meta?.authorizations || [],
  });
};

export const updateObjectAction = async (
  relatedCasesUuids: string[],
  objectUuid: string,
  values: { [key: string]: any },
  status?: 'active' | 'inactive'
) => {
  const data: any /* APICaseManagement.UpdateCustomObjectRequestBody */ = {
    uuid: v4(),
    existing_uuid: objectUuid,
    custom_fields: values,
    // this is mock data, but it's required by the API
    archive_metadata: {
      status: 'to preserve',
      ground: '',
      retention: 0,
    },
    related_to: {
      cases: relatedCasesUuids,
    },
    status: status || 'active',
  };

  return await updateObject(data);
};

export const activateObject = async (
  relatedCasesUuids: string[],
  objectUuid: string,
  values: { [key: string]: any }
) => {
  return await updateObjectAction(
    relatedCasesUuids,
    objectUuid,
    values,
    'active'
  );
};

export const getObjectType = async (
  object: ObjectType,
  setObjectType: (objectType: ObjectTypeType) => void
) => {
  const objectType = await fetchObjectType(object.objectTypeVersionUuid);

  if (!objectType.data.attributes.custom_field_definition?.custom_fields) {
    return;
  }

  setObjectType({
    ...objectType.data.attributes,
    customFieldsDefinition:
      objectType.data.attributes.custom_field_definition?.custom_fields,
    authorizations: objectType.data.meta?.authorizations || [],
  });
};

/* eslint complexity: [2, 10] */
export const getCaseRelations = async (
  uuid: string,
  setCaseRelations: (caseRelations: RelatedCaseRowType[]) => void
) => {
  const caseRelations = await fetchCaseRelations(uuid);

  setCaseRelations(
    (caseRelations.data || []).map((row: any) => ({
      uuid: row.id || '',
      name: row.id || '',
      nr: row.attributes?.number || 0,
      progress: (row.attributes?.progress || 0) * 100,
      status: row.attributes?.status || '',
      caseType: row.relationships?.case_type?.meta?.summary || '',
      result: row.attributes?.result || '',
      extra: row.meta?.summary || '',
      assignee: row.relationships?.assignee?.meta?.summary || '',
    }))
  );
};

export const getObjectRelations = async (
  uuid: string,
  setObjectRelations: (objectRelations: RelatedObjectRowType[]) => void
) => {
  const objectRelations = await fetchObjectRelations(uuid);

  setObjectRelations(
    (objectRelations.data || []).map((row: any) => ({
      uuid: row.id || '',
      name: row.meta?.summary || '',
      objectType: row.relationships?.object_type?.meta.summary,
      externalReference: 'test',
    }))
  );
};

export const getSubjectRelations = async (
  uuid: string,
  setSubjectRelations: (subjectRelations: RelatedSubjectRowType[]) => void
) => {
  const subjectRelations = await fetchSubjectRelations(uuid);

  setSubjectRelations(
    (subjectRelations.data || []).map((row: any) => ({
      uuid: row.id || '',
      name: row.meta?.summary || '',
      subjectType: row.attributes?.type,
      roleType: row.attributes?.roles,
    }))
  );
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from './CustomerContact.locale';
import { useUnauthorizedBanner } from './../../library/useUnauthorizedBanner';
import CustomerContact from './CustomerContact';

const CustomerContactModule: React.FunctionComponent = () => {
  const Banner = useUnauthorizedBanner(
    (systemRoles, capabilities) =>
      systemRoles?.includes('Behandelaar') &&
      capabilities?.includes('message_intake')
  );
  return Banner ? (
    Banner
  ) : (
    <I18nResourceBundle resource={locale} namespace="customerContact">
      <CustomerContact />
    </I18nResourceBundle>
  );
};

export default CustomerContactModule;

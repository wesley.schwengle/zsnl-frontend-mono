// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { CaseObjType, DialogsType } from '../../Case.types';
import { useCaseActionButtonStyles } from './CaseActionButton.style';

type CaseActionsButtonPropsType = {
  caseObj: CaseObjType;
  setDialog: (type: DialogsType) => void;
  folded: boolean;
};

const CaseActionsButton: React.ComponentType<CaseActionsButtonPropsType> = ({
  caseObj,
  setDialog,
  folded,
}) => {
  const [t] = useTranslation('case');
  const classes = useCaseActionButtonStyles();
  const label = t('caseActions.button');

  return (
    <Tooltip className={classes.wrapper} title={label} placement="right">
      {folded ? (
        <IconButton
          className={classes.caseActionsIcon}
          onClick={() => setDialog('caseActions')}
        >
          <Icon size="extraSmall">{iconNames.build}</Icon>
        </IconButton>
      ) : (
        <Button
          className={classes.caseActions}
          action={() => setDialog('caseActions')}
          presets={['contained', 'primary']}
          disabled={!caseObj.hasEditRights}
        >
          {label}
        </Button>
      )}
    </Tooltip>
  );
};

export default CaseActionsButton;

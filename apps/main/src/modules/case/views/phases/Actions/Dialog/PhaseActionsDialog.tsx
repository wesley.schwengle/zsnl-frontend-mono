// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { ActionType, SaveActionType } from '../Actions.types';
import { CaseObjType, CaseTypeType } from '../../../../Case.types';
import { usePhaseActionDialogStyles } from './PhaseActionsDialog.style';
import {
  getFormDefinition,
  getRules,
} from './PhaseActionsDialog.formDefinition';
import EmailPreview from './EmailPreview';
import { getTitle, isSubcaseInLastPhase } from './../Actions.library';

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
  tertiaryPresets: ['default', 'text'],
});

type CaseActionDialogPropsType = {
  action: ActionType;
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  phaseNumber: number;
  saveAction: SaveActionType;
  onClose: () => void;
  open: boolean;
};

/* eslint complexity: [2, 8] */
const CaseActionDialog: React.ComponentType<CaseActionDialogPropsType> = ({
  action,
  caseObj,
  caseType,
  phaseNumber,
  saveAction,
  onClose,
  open,
}) => {
  const [t] = useTranslation('phaseActions');
  const [previewing, setPreviewing] = useState<boolean>(false);
  const classes = usePhaseActionDialogStyles();
  const dialogEl = useRef();

  const noun = t(`${action.type}.noun`);
  const verb = t(`${action.type}.verb`);
  const title = `${action.label}: ${getTitle(t, action)}`;

  const formDefinition = getFormDefinition[action.type]({
    t,
    action,
    caseObj,
    caseType,
    phaseNumber,
  });
  const rules = getRules[action.type]();
  const validationMap = generateValidationMap(formDefinition);

  let {
    fields,
    formik: { isValid, values },
  } = useForm({
    formDefinition,
    validationMap,
    rules,
  });

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={`phase-action-${noun}-dialog`}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          title={title}
          icon={action.icon}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.wrapper}>
            {!previewing &&
              fields.map(
                ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
                  const props = cloneWithout(rest, 'mode');

                  return (
                    <FormControlWrapper
                      {...props}
                      label={suppressLabel ? false : props.label}
                      compact={true}
                      key={`${props.name}-formcontrol-wrapper`}
                    >
                      <FieldComponent
                        {...props}
                        t={t}
                        containerRef={dialogEl.current}
                      />
                    </FormControlWrapper>
                  );
                }
              )}
            {previewing && (
              <EmailPreview caseNumber={caseObj.number} values={values} />
            )}
          </div>
        </DialogContent>
        <>
          <DialogDivider />
          {action.type !== 'subject' && (
            <DialogActions>
              {getDialogActions(
                {
                  text: verb,
                  disabled:
                    !isValid ||
                    action.automatic ||
                    isSubcaseInLastPhase(
                      values.relaties_relatie_type,
                      caseType,
                      phaseNumber
                    ),
                  action() {
                    onClose();
                    saveAction('execute', { ...action, ...values });
                  },
                },
                {
                  text: t('common:dialog.save'),
                  disabled: !isValid,
                  action() {
                    onClose();
                    saveAction('commit', { ...action, ...values });
                  },
                },
                action.type === 'email'
                  ? {
                      text: previewing
                        ? t('email.preview.close')
                        : t('email.preview.open'),
                      action() {
                        setPreviewing(!previewing);
                      },
                    }
                  : {},
                `phase-action-${noun}-dialog`
              )}
            </DialogActions>
          )}
        </>
      </Dialog>
    </>
  );
};

export default CaseActionDialog;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useIconsStyles = makeStyles(
  ({ palette: { coral, primary } }: Theme) => ({
    allocation: {
      color: coral.main,
    },
    subject: {
      color: primary.main,
    },
  })
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useCasePhasesStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    flexGrow: 1,
    display: 'flex',
  },
  form: {
    flexGrow: 1,
  },
  sideBar: {
    width: '350px',
  },
  sideBarButtons: {
    display: 'flex',
    '& >*': {
      flexGrow: 1,
      borderRadius: 0,
      height: '100%',
      width: '100%',
    },
  },
  sideBarContent: {},
}));

export type ClassesType = ReturnType<typeof useCasePhasesStyles>;

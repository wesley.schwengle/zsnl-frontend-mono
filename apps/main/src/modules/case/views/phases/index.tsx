// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Phases, { PhasesPropsType } from './Phases';
import locale from './Phases.locale';

const PhasesView: React.ComponentType<PhasesPropsType> = ({
  caseObj,
  caseType,
}) => {
  return (
    <I18nResourceBundle resource={locale} namespace="casePhases">
      <Routes>
        <Route
          path=""
          element={<Navigate to={`${caseObj.phase}`} replace={true} />}
        />
        <Route
          path={':phaseNumber/*'}
          element={<Phases caseObj={caseObj} caseType={caseType} />}
        />
      </Routes>
    </I18nResourceBundle>
  );
};

export default PhasesView;

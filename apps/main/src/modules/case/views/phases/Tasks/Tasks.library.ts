// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { v4 } from 'uuid';
import {
  fetchTasks,
  createTaskRequest,
  setTaskCompletionRequest,
  updateTaskRequest,
  deleteTaskRequest,
} from './Tasks.requests';
import { HandleChangeType, TasksType, TaskType } from './Tasks.types';

export const sortTasks = (tasks: TasksType): TasksType => {
  const completed = tasks.filter(task => task.completed).reverse();
  const nonCompleted = tasks.filter(task => !task.completed).reverse();
  const sortedTasks = [...nonCompleted, ...completed];

  return sortedTasks;
};

export const countOpenTasks = (tasks: TasksType): number =>
  tasks.filter(task => !task.completed).length;

export const getTasks = async (
  caseUuid: string,
  phaseNumber: string,
  setTasks: (tasks: TasksType) => void,
  setOpenTasksCount: (openTasksCount: number) => void
) => {
  const response = await fetchTasks(caseUuid, phaseNumber);

  const tasks = response.data?.map(
    ({
      id,
      attributes: { completed, description, due_date, title },
      meta: { is_editable, can_set_completion },
      relationships: { assignee },
    }: any) => {
      return {
        task_uuid: id,
        title,
        completed,
        description,
        due_date,
        assignee:
          assignee && assignee.data
            ? {
                label: assignee.data.meta.display_name,
                value: assignee.data.id,
              }
            : null,
        is_editable,
        can_set_completion,
      };
    }
  );

  // because data is optional in the apidocs
  if (!tasks) return;

  setTasks(tasks);

  const openTasksCount = countOpenTasks(tasks);

  setOpenTasksCount(openTasksCount);
};

export const addTaskAction = async (
  caseUuid: string,
  phaseNumber: string,
  title: string,
  tasks: TasksType,
  handleChange: HandleChangeType
) => {
  const task_uuid = v4();
  const newTask: TaskType = {
    title,
    task_uuid,
    description: '',
    completed: false,
    due_date: null,
    assignee: null,
    is_editable: true,
    can_set_completion: true,
  };
  const request = () =>
    createTaskRequest(caseUuid, task_uuid, phaseNumber, title);

  handleChange(request, tasks, [...tasks, newTask]);
};

export const updateTaskAction = async (
  task_uuid: string,
  values: TaskType,
  tasks: TasksType,
  handleChange: HandleChangeType
) => {
  const assignee = values.assignee ? values.assignee.value : null;

  const request = () => updateTaskRequest({ ...values, task_uuid, assignee });

  const newTasks = tasks.reduce((acc: TasksType, task) => {
    if (task.task_uuid !== task_uuid) return [...acc, task];

    const newTask = { ...task, ...values };

    return [...acc, newTask];
  }, []);

  handleChange(request, tasks, newTasks);
};

export const deleteTaskAction = async (
  task_uuid: string,
  tasks: TasksType,
  handleChange: HandleChangeType
) => {
  const request = () => deleteTaskRequest(task_uuid);

  const newTasks = tasks.filter(task => task.task_uuid !== task_uuid);

  handleChange(request, tasks, newTasks);
};

export const setTaskCompletionAction = async (
  task_uuid: string,
  completed: boolean,
  tasks: TasksType,
  handleChange: HandleChangeType
) => {
  const request = () => setTaskCompletionRequest(task_uuid, completed);

  const newTasks = tasks.reduce((acc: TasksType, task) => {
    if (task.task_uuid !== task_uuid) return [...acc, task];

    const newTask = { ...task, completed };

    return [...acc, newTask];
  }, []);

  handleChange(request, tasks, newTasks);
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

const indicatorSize = 23;

export const useTaskItemStyles = makeStyles(
  ({ mintlab: { greyscale }, typography }: any) => ({
    container: {
      display: 'flex',
      position: 'relative',
      alignItems: 'center',
      justifyContent: 'left',
      paddingLeft: 10,
      transition: '0.2s ease-out',

      '&>button': {
        minWidth: indicatorSize,
      },
    },
    doneIndicator: {
      border: `1px solid ${greyscale.evenDarker}`,
      width: indicatorSize,
      height: indicatorSize,
      borderRadius: '50%',
      marginRight: 14,
      padding: 0,

      '&>span>span': {
        opacity: 0,
      },
    },
    doneIndicatorCompleted: {
      border: 'none',

      '&>span>span': {
        opacity: 1,
      },
    },
    body: {
      padding: '10px 0',
      borderBottom: `1px solid ${greyscale.dark}`,
      display: 'flex',
      width: '100%',
    },
    bodyCompleted: {
      borderBottom: `1px solid ${greyscale.light}`,
      color: greyscale.evenDarker,
    },
    title: {
      width: '100%',
      margin: '7px 0',
      fontFamily: typography.fontFamily,
      fontWeight: typography.fontWeightLight,
      overflowWrap: 'anywhere',
    },
    titleCompleted: {
      textDecoration: 'line-through',
    },
    titleSmaller: {
      fontSize: 10,
    },
    titleExtraSmall: {
      fontSize: 7,
    },
    details: {
      flexGrow: 2,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
    },
    detailsLink: {
      display: 'flex',
      alignItems: 'center',
      alignSelf: 'center',
      width: 52,
      height: 52,
    },
  })
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
// @ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
// @ts-ignore
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { useTaskCreatorStyles } from './TaskCreator.style';
import { AddTaskType } from './../../../Tasks.types';

const MAX_TITLE_LENGTH = 253;

export type TaskCreatorPropsType = {
  canEdit: boolean;
  addTask: AddTaskType;
};

const TaskCreator: React.ComponentType<TaskCreatorPropsType> = ({
  canEdit,
  addTask,
}) => {
  const [t] = useTranslation();
  const classes = useTaskCreatorStyles();
  const [creatingTask, setCreatingTask] = React.useState(false);
  const [taskTitle, setTaskTitle] = React.useState('');

  const submitTask = () => {
    if (taskTitle) {
      addTask(taskTitle.slice(0, MAX_TITLE_LENGTH));
    }
    setTaskTitle('');
  };

  const handleKeyPress = (ev: any) => {
    if (ev.key.toLowerCase() === 'enter') {
      submitTask();
    } else if (ev.key.toLowerCase() === 'escape') {
      setTaskTitle('');
      setCreatingTask(false);
    }
  };

  const handleBlur = () => {
    setTaskTitle('');
    setCreatingTask(false);
  };

  return (
    <div>
      {canEdit && (
        <Button
          icon="add"
          iconSize="medium"
          classes={{ root: classes.button }}
          action={() => setCreatingTask(true)}
        >
          {t('caseTasks:addNewTask')}
        </Button>
      )}
      {creatingTask && (
        <TextField
          // eslint-disable-next-line jsx-a11y/no-autofocus
          autoFocus={true}
          onBlur={handleBlur}
          value={taskTitle}
          onChange={(ev: any) => setTaskTitle(ev.target.value)}
          onKeyDown={handleKeyPress}
        />
      )}
    </div>
  );
};

export default TaskCreator;

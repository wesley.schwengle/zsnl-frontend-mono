// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import Details, { DetailsPropsType } from './Details';
import { TasksType } from './../../Tasks.types';

interface DetailsWrapperPropsType
  extends Pick<
    DetailsPropsType,
    | 'canEdit'
    | 'updateTask'
    | 'deleteTask'
    | 'setTaskCompletion'
    | 'exitEditMode'
  > {
  tasks: TasksType;
}

const DetailsWrapper: React.ComponentType<DetailsWrapperPropsType> = props => {
  const { task_uuid } = useParams();
  const task = props.tasks.find(task => task.task_uuid === task_uuid);

  if (!task) {
    props.exitEditMode();
    return null;
  }

  return <Details {...props} task={task} />;
};

export default DetailsWrapper;

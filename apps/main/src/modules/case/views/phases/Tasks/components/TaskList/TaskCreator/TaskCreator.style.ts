// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTaskCreatorStyles = makeStyles(
  ({ palette, typography }: any) => ({
    button: {
      ...typography.button,
      textTransform: 'unset',
      fontSize: 14,
      color: palette.primary.main,
    },
  })
);

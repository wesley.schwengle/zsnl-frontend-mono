// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { SubjectType } from '../../Relations.types';
import { isSystemRole } from './library';

type GetColumnsType = (
  t: i18next.TFunction,
  caseOpen: boolean,
  saving: boolean,
  startEdit: (subject: SubjectType) => void,
  unrelate: (relationUuid: string) => void
) => any;

type RowDataType = {
  rowData: SubjectType;
};

export const getColumns: GetColumnsType = (
  t,
  caseOpen,
  saving,
  startEdit,
  unrelate
) =>
  [
    {
      name: 'type',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) =>
        t(`subjects.cells.type.${rowData.subject.type}`),
    },
    {
      name: 'name',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) => (
        <a
          target="_parent"
          href={`/main/contact-view/${rowData.subject.type}/${rowData.subject.uuid}`}
        >
          {rowData.subject.name}
        </a>
      ),
    },
    {
      name: 'role',
      width: 1,
      flexGrow: 1,
    },
    {
      name: 'authorized',
      width: 1,
      flexGrow: 1,
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: RowDataType) =>
        rowData.authorized && <Icon>{iconNames.done}</Icon>,
    },
    ...(caseOpen
      ? [
          {
            name: 'edit',
            width: 70,
            minWidth: 70,
            // eslint-disable-next-line
            cellRenderer: ({ rowData }: RowDataType) =>
              !isSystemRole(rowData.role) && (
                <Button
                  presets={['icon', 'extraSmall']}
                  disabled={saving}
                  action={() => startEdit(rowData)}
                >
                  edit
                </Button>
              ),
          },
          {
            name: 'delete',
            width: 70,
            minWidth: 70,
            // eslint-disable-next-line
            cellRenderer: ({ rowData }: RowDataType) =>
              !isSystemRole(rowData.role) && (
                <Button
                  presets={['icon', 'extraSmall']}
                  disabled={saving}
                  action={() => unrelate(rowData.uuid)}
                >
                  close
                </Button>
              ),
          },
        ]
      : []),
  ].map(row => ({
    ...row,
    label: t(`subjects.columns.${row.name}`),
  }));

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Relations from './Relations';
import locale from './Relations.locale';
import { RelationsPropsType } from './Relations.types';

const RelationsView: React.ComponentType<RelationsPropsType> = props => (
  <I18nResourceBundle resource={locale} namespace="caseRelations">
    <Relations {...props} />
  </I18nResourceBundle>
);

export default RelationsView;

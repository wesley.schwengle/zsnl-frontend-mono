// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APIDocument } from '@zaaksysteem/generated';
import React from 'react';

export const useMsEditHandler = () => {
  let resetTabIcon: () => void;
  let form: HTMLFormElement;
  let msOfficeIframe: HTMLIFrameElement;

  const msEdit = async (doc: { uuid: string; extension: string }) => {
    const response = await request<APIDocument.EditDocumentOnlineResponseBody>(
      'GET',
      buildUrl<APIDocument.EditDocumentOnlineRequestParams>(
        '/api/v2/document/edit_document_online',
        {
          id: doc.uuid,
          editor_type: 'msonline',
          close_url: window.location.origin + window.location.pathname,
          host_page_url: window.location.origin + window.location.pathname,
        }
      )
    );

    resetTabIcon = setTabIcon(doc.extension);
    form = prepareMsFormElement(response);
    msOfficeIframe = prepareMsIframe();

    msOfficeIframe.addEventListener('load', function () {
      setTimeout(function () {
        (msOfficeIframe.contentWindow as any).postMessage(
          {
            MessageId: 'Host_PostmessageReady',
            SendTime: new Date().getTime(),
            Values: {},
          },
          '*'
        );
      }, 3000);
    });

    window.history.pushState({}, '', new URL(window.location as any));

    window.addEventListener('popstate', closeMsUi);

    document.body.appendChild(msOfficeIframe);
    document.body.appendChild(form);
    form.submit();
  };

  const closeMsUi = () => {
    resetTabIcon();
    document.body.removeChild(msOfficeIframe);
    document.body.removeChild(form);
    window.removeEventListener('popstate', closeMsUi);
    const documentTabWindow = (
      document.querySelector('#document-tab-iframe') as HTMLIFrameElement
    ).contentWindow;

    documentTabWindow &&
      documentTabWindow.postMessage(
        { type: 'reloadData' },
        window.location.origin
      );
  };

  const handleMsIframeClose = (event: MessageEvent) => {
    try {
      if (JSON.parse(event.data).MessageId === 'UI_Close') {
        closeMsUi();
      }
      // eslint-disable-next-line no-empty
    } catch (err) {}
  };

  const handleMsEdit = (event: MessageEvent) => {
    event.data.type === 'EditDocumentMs' && msEdit(event.data.value);
  };

  React.useEffect(() => {
    window.addEventListener('message', handleMsIframeClose);
    window.addEventListener('message', handleMsEdit);
    return () => {
      window.removeEventListener('message', handleMsIframeClose);
      window.removeEventListener('message', handleMsEdit);
    };
  }, []);
};

const prepareMsFormElement = (
  response: APIDocument.EditDocumentOnlineResponseBody
) => {
  const form = document.createElement('form');
  const inputTokenA = document.createElement('input');
  const inputTokenB = document.createElement('input');
  form.id = 'office_form';
  form.name = 'office_form';
  form.action = response.action_url;
  form.target = 'office_frame';
  form.method = 'post';
  inputTokenA.type = 'hidden';
  inputTokenA.name = 'access_token';
  inputTokenA.value = response.access_token;
  inputTokenB.type = 'hidden';
  inputTokenB.name = 'access_token_ttl';
  inputTokenB.value = response.access_token_ttl;

  form.appendChild(inputTokenA);
  form.appendChild(inputTokenB);

  return form;
};

const prepareMsIframe = () => {
  const msOfficeIframe = document.createElement('iframe');
  msOfficeIframe.name = 'office_frame';
  msOfficeIframe.id = 'office_frame';

  msOfficeIframe.title = 'Office Frame';

  msOfficeIframe.setAttribute('allowfullscreen', 'true');
  msOfficeIframe.setAttribute(
    'sandbox',
    'allow-scripts allow-same-origin allow-forms allow-popups allow-top-navigation allow-popups-to-escape-sandbox'
  );
  msOfficeIframe.style.position = 'absolute';
  msOfficeIframe.style.top = '0';
  msOfficeIframe.style.left = '0';
  msOfficeIframe.style.width = '100%';
  msOfficeIframe.style.height = '100%';
  msOfficeIframe.style.zIndex = '1000';

  return msOfficeIframe;
};

const setTabIcon = (extension: string) => {
  const iconSheet = document.querySelector(
    "link[rel*='icon']"
  ) as HTMLLinkElement;
  const originalSheetAddress = iconSheet.href;

  let msFavIconHref;

  if (['pptx', 'ppt'].includes(extension)) {
    msFavIconHref =
      'https://c1-powerpoint-15.cdn.office.net/p/resources/1033/FavIcon_Ppt.ico';
  } else if (['xls', 'xlsx'].includes(extension)) {
    msFavIconHref =
      'https://c1-excel-15.cdn.office.net/x/_layouts/resources/FavIcon_Excel.ico';
  } else {
    msFavIconHref =
      'https://c1-word-view-15.cdn.office.net/wv/resources/1033/FavIcon_Word.ico';
  }

  iconSheet.href = msFavIconHref;

  return () => {
    iconSheet.href = originalSheetAddress;
  };
};

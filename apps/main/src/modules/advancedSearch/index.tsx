// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import locale from './AdvancedSearch.locale';
import AdvancedSearch from './AdvancedSearch';
import { defaultOptions } from './library/config';

const moduleConfig = {
  id: 'advanced-search',
  reducerMap: {},
};

const queryClient = new QueryClient({
  defaultOptions,
});

type AdvancedSearchModulePropsType = {
  prefix: string;
};

const AdvancedSearchModule: FunctionComponent<
  AdvancedSearchModulePropsType
> = ({ prefix }) => {
  return (
    //@ts-ignore
    <DynamicModuleLoader modules={[moduleConfig]}>
      <I18nResourceBundle resource={locale} namespace="search">
        <QueryClientProvider client={queryClient}>
          <ReactQueryDevtools />
          <Routes>
            <Route
              path=""
              element={<Navigate to="custom_object" replace={true} />}
            />
            <Route
              path=":kind/*"
              element={<AdvancedSearch client={queryClient} prefix={prefix} />}
            />
          </Routes>
        </QueryClientProvider>
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
};

export default AdvancedSearchModule;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { Field, FieldInputProps } from 'formik';
import { FilterType } from '../../../../AdvancedSearch.types';
import Text from '../../Filters/Text/Text';
import { getFieldProps } from '../Filters.library';
import { useStyles } from './CustomField.styles';

const configMap: {
  [name: string]: {
    Component: FunctionComponent<any>;
    operator: boolean;
  };
} = {
  text: {
    Component: Text,
    operator: true,
  },
};

type CustomFieldFilterComponentPropsType = {
  field: FieldInputProps<any>;
  filter: FilterType;
  index: number;
  t: i18next.TFunction;
};
const CustomFieldFilterComponent: FunctionComponent<
  CustomFieldFilterComponentPropsType
> = ({ field, filter, index, t }) => {
  //@ts-ignore
  const config = configMap[filter.parameters.type];
  const { Component } = config;
  const fieldProps = getFieldProps({ t, filter });

  return (
    <Component index={index} filter={filter} t={t} {...field} {...fieldProps} />
  );
};

type OperatorComponentPropsType = {
  field: FieldInputProps<any>;
  t: i18next.TFunction;
};
const OperatorComponent: FunctionComponent<OperatorComponentPropsType> = ({
  field,
  t,
}) => {
  const translationBase = `editForm.fields.filters.operators`;
  const choices = ['eq', 'ne'].map((value: string) => ({
    label: t(`${translationBase}.${value}`),
    value,
  }));

  return (
    <div>
      <Select
        choices={choices}
        isClearable={false}
        generic={true}
        nestedValue={true}
        {...field}
      />
    </div>
  );
};

type CustomFieldPropsType = {
  filter: FilterType;
  name: string;
  t: i18next.TFunction;
};

const CustomField: FunctionComponent<CustomFieldPropsType> = ({
  filter,
  name,
  t,
}) => {
  const classes = useStyles();
  //@ts-ignore
  const type = filter?.parameters?.type as string;
  const config = configMap[type];

  if (!config) {
    return (
      <div>
        {
          t('editForm.fields.filters.typeNotSupported', {
            type,
          }) as string
        }
      </div>
    );
  }

  return (
    <div className={classes.wrapper}>
      {config.operator && (
        <div className={classes.operator}>
          <Field
            name={`${name}.parameters.operator`}
            component={OperatorComponent}
            filter={filter}
            t={t}
          />
        </div>
      )}
      <div>
        <Field
          name={`${name}.parameters.value`}
          component={CustomFieldFilterComponent}
          filter={filter}
          t={t}
        />
      </div>
    </div>
  );
};

export default CustomField;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import { parse } from 'tinyduration';
import * as i18next from 'i18next';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { DateEntryType, OperatorType } from '../../../../AdvancedSearch.types';
import {
  DialogPropertiesType,
  IntervalType,
  PeriodType,
} from './DateEntries.types';

const DATETIME_FORMAT = 'D MMMM YYYY HH:mm';
const DATE_FORMAT = 'DD MMMM YYYY';
export const TRANSLATION_BASE =
  'search:editForm.fields.filters.types.dateEntries.';

/* eslint complexity: [2, 20] */
export const getReadableValue = (
  entry: DateEntryType,
  t: i18next.TFunction
) => {
  let operator;
  if (entry.type === 'absolute' || entry.type === 'relative') {
    operator = `${TRANSLATION_BASE}operators.${entry.operator}`;
  }

  if (entry.type === 'absolute') {
    if (!entry.value) return '';
    const dateParsed = new Date(Date.parse(entry.value));
    const parsedDate = fecha.format(dateParsed, DATETIME_FORMAT);
    return `${t(operator as OperatorType)} ${parsedDate}`;
  } else if (entry.type === 'relative') {
    if (!entry.value) return '';
    let result;
    if (isPresetValue(entry.value, t)) {
      const preset = getPresets(t).find(preset => preset.value === entry.value);
      result = preset ? preset.label : '';
    } else {
      const parts = parse(entry.value) as any;
      const mapped = ['years', 'months', 'days', 'hours', 'minutes', 'seconds']
        .flatMap(element => {
          const intervalTranslation = t(
            `${TRANSLATION_BASE}intervals.${element}`
          );
          return parts[element] !== 0
            ? `${parts[element]} ${intervalTranslation}`
            : [];
        })
        .join(', ');

      result =
        mapped +
        ` ${t(`${TRANSLATION_BASE}inLabel`)} ` +
        (parts.negative
          ? t(`${TRANSLATION_BASE}periods.-`)
          : t(`${TRANSLATION_BASE}periods.+`));
    }

    return `${t(operator as OperatorType)} ${result}`;
  } else if (entry.type === 'range') {
    if (!entry.startValue || !entry.endValue) return '';

    const getValue = (value: string) => {
      const parsedObj = new Date(Date.parse(value));
      const format =
        entry.timeSetByUser === true ? DATETIME_FORMAT : DATE_FORMAT;
      return fecha.format(parsedObj, format);
    };

    return `${t(`${TRANSLATION_BASE}between`)} ${getValue(
      entry.startValue
    )} ${t(`${TRANSLATION_BASE}and`)} ${getValue(entry.endValue)}`;
  }

  return '';
};

export const hasValue = (value: any) => value !== undefined && value !== null;

export const getPresets = (t: i18next.TFunction): ValueType<string>[] => [
  {
    label: t(`${TRANSLATION_BASE}presets.now`) as string,
    value: '-P0Y0M0DT0H0M0S',
  },
  {
    label: t(`${TRANSLATION_BASE}presets.lastHour`) as string,
    value: '-P0Y0M0DT1H0M0S',
  },
  {
    label: t(`${TRANSLATION_BASE}presets.lastDay`) as string,
    value: '-P0Y0M1DT0H0M0S',
  },
  {
    label: t(`${TRANSLATION_BASE}presets.last7Days`) as string,
    value: '-P0Y0M7DT0H0M0S',
  },
  {
    label: t(`${TRANSLATION_BASE}presets.last30Days`) as string,
    value: '-P0Y0M30DT0H0M0S',
  },
  {
    label: t(`${TRANSLATION_BASE}presets.lastYear`) as string,
    value: '-P1Y0M0DT0H0M0S',
  },
];

export const getOperatorChoices = (
  t: i18next.TFunction
): ValueType<OperatorType>[] =>
  ['lt', 'le', 'gt', 'ge'].map(value => ({
    label: t(`${TRANSLATION_BASE}operators.${value}`) as string,
    value: value as OperatorType,
  }));

export const getIntervals = (t: i18next.TFunction): ValueType<IntervalType>[] =>
  ['minutes', 'hours', 'days', 'months', 'years'].map(value => ({
    label: t(`${TRANSLATION_BASE}intervals.${value}`) as string,
    value: value as IntervalType,
  }));

export const getPeriods = (t: i18next.TFunction): ValueType<PeriodType>[] =>
  ['-', '+'].map(value => ({
    label: t(`${TRANSLATION_BASE}periods.${value}`) as string,
    value: value as PeriodType,
  }));

export const isPresetValue = (value: any | null, t: i18next.TFunction) =>
  getPresets(t).some(element => element.value === value);

export const defaultDialogProperties: DialogPropertiesType = {
  open: false,
  index: null,
  entry: null,
};

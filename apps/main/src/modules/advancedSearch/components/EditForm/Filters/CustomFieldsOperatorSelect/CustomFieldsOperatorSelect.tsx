// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
//@ts-ignore
import { Field, FieldInputProps } from 'formik';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';

type SelectComponentPropsType = {
  field: FieldInputProps<any>;
  t: i18next.TFunction;
};
const SelectComponent: FunctionComponent<SelectComponentPropsType> = ({
  field,
  t,
}) => {
  const choices = ['and', 'or'].map((value: string) => ({
    label: t(`editForm.fields.filters.customFieldOperators.${value}`),
    value,
  }));

  return (
    <Select
      choices={choices}
      isClearable={false}
      generic={true}
      nestedValue={true}
      //@ts-ignore
      {...field}
    />
  );
};

type CustomFieldsOperatorSelectPropsType = {
  classes: any;
  t: i18next.TFunction;
};
const CustomFieldsOperatorSelect: FunctionComponent<
  CustomFieldsOperatorSelectPropsType
> = ({ classes, t }) => {
  return (
    <div className={classes.editFormSection}>
      <span className={classes.filterRowLabel}>
        {t('editForm.fields.CustomFieldsOperatorSelect.label') as string}
      </span>
      <div className={classes.filterRowContent}>
        <Field
          name="filters.customFieldsOperator"
          component={SelectComponent}
          t={t}
        />
      </div>
    </div>
  );
};

export default CustomFieldsOperatorSelect;

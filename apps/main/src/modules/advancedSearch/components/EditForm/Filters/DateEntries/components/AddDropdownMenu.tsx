// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import DropdownMenu, {
  DropdownMenuList,
  //@ts-ignore
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import { TRANSLATION_BASE } from '../DateEntries.library';
import { DialogPropertiesType } from '../DateEntries.types';

const defaultDialogAbsolute: DialogPropertiesType = {
  open: true,
  index: null,
  entry: {
    type: 'absolute',
    value: null,
    operator: null,
  },
};

const defaultDialogRelative: DialogPropertiesType = {
  open: true,
  index: null,
  entry: {
    type: 'relative',
    value: null,
    operator: null,
  },
};

const defaultDialogRange: DialogPropertiesType = {
  open: true,
  index: null,
  entry: {
    type: 'range',
    startValue: null,
    endValue: null,
    timeSetByUser: false,
  },
};

type AddDropdownMenuPropsType = {
  t: i18next.TFunction;
  setDialogProperties: (dialog: DialogPropertiesType) => void;
  classes: any;
};

const AddDropdownMenu: FunctionComponent<AddDropdownMenuPropsType> = ({
  t,
  setDialogProperties,
  classes,
}) => {
  const buttons = [
    {
      action: () => {
        setDialogProperties(defaultDialogAbsolute);
      },
      title: t(`${TRANSLATION_BASE}titles.absolute`),
    },
    {
      action: () => setDialogProperties(defaultDialogRelative),
      title: t(`${TRANSLATION_BASE}titles.relative`),
    },
    {
      action: () => setDialogProperties(defaultDialogRange),
      title: t(`${TRANSLATION_BASE}titles.range`),
    },
  ];

  return (
    <div className={classes.addDropdownMenu}>
      <DropdownMenu
        transformOrigin={{
          vertical: -130,
          horizontal: 'center',
        }}
        trigger={
          <Button
            presets={['primary', 'contained']}
            scope={`date-entries:add-dropdown-button`}
          >
            {`+ ${t('search:new')}`}
          </Button>
        }
      >
        <DropdownMenuList
          items={buttons.map(({ action, title }) => ({
            action,
            label: title,
            scope: `date-entries:add-dropdown-list`,
          }))}
        />
      </DropdownMenu>
    </div>
  );
};

export default AddDropdownMenu;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  FunctionComponent,
  useState,
  useEffect,
  useCallback,
} from 'react';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
import {
  Dialog as UIDialog,
  DialogTitle,
  DialogDivider,
  DialogContent,
} from '@mintlab/ui/App/Material/Dialog';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { DateTimePicker } from '@mintlab/ui/App/Material/DatePicker';
import { useStyles } from '../DateEntries.style';
import { getOperatorChoices, TRANSLATION_BASE } from '../DateEntries.library';
import {
  OperatorType,
  DateEntryTypeAbsolute,
} from '../../../../../AdvancedSearch.types';

type AbsoluteDialogPropsType = {
  onSubmit: (entry: DateEntryTypeAbsolute) => void;
  onClose: () => void;
  entry?: DateEntryTypeAbsolute | null;
  open: boolean;
  t: i18next.TFunction;
};

const AbsoluteDialog: FunctionComponent<AbsoluteDialogPropsType> = ({
  onSubmit,
  entry,
  open,
  onClose,
  t,
}) => {
  const [absoluteValue, setAbsoluteValue] = useState<string | null>(null);
  const [operator, setOperator] = useState<OperatorType | null>(null);
  const [dialogRef, setDialogRef] = useState<HTMLInputElement | null>(null);
  const classes = useStyles();

  useEffect(() => {
    setAbsoluteValue(entry?.value || new Date().toISOString());
    setOperator(entry?.operator || null);
  }, [open]);

  const isValid = () => {
    if (!absoluteValue || !operator) return false;
    return true;
  };

  const measuredRef = useCallback((node: any) => {
    if (node !== null) setDialogRef(node);
  }, []);

  return (
    <React.Fragment>
      <UIDialog onClose={onClose} open={open} ref={measuredRef}>
        <DialogTitle
          title={t(`${TRANSLATION_BASE}titles.absolute`)}
          icon="today"
        />
        <DialogDivider />
        <DialogContent padded={false}>
          <div className={classes.dialogWrapper}>
            <div className={classes.operator}>
              {dialogRef ? (
                <Select
                  key={`absolute-dialog-select-operator`}
                  name={`absolute-dialog-select-operator`}
                  choices={getOperatorChoices(t)}
                  value={operator || null}
                  isClearable={false}
                  generic={true}
                  onChange={(event: React.ChangeEvent<any>) =>
                    setOperator(event.target.value.value)
                  }
                  //@ts-ignore
                  translations={{
                    'form:choose': t(`${TRANSLATION_BASE}operator.placeholder`),
                  }}
                  containerRef={dialogRef}
                />
              ) : null}
            </div>
            <DialogDivider />
            <DateTimePicker
              //@ts-ignore
              value={new Date(absoluteValue)}
              name="absolute-dialog-dateTimePicker"
              staticVariant={true}
              onChange={({ target: { value } }: any) => {
                setAbsoluteValue(value);
              }}
            />
          </div>
        </DialogContent>
        <DialogDivider />
        <div className={classes.actionButtons}>
          <Button
            presets={['contained', 'default', 'medium']}
            action={() => {
              onClose();
            }}
          >
            {t(`${TRANSLATION_BASE}cancelButton`)}
          </Button>
          <Button
            presets={['contained', 'primary', 'medium']}
            disabled={!isValid()}
            action={() => {
              onSubmit({
                type: 'absolute',
                operator,
                value: absoluteValue,
              });
              onClose();
            }}
          >
            {t(`${TRANSLATION_BASE}okButton`)}
          </Button>
        </div>
      </UIDialog>
    </React.Fragment>
  );
};

export default AbsoluteDialog;

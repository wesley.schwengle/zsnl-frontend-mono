// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { APICaseManagement } from '@zaaksysteem/generated';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
//@ts-ignore
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Select, ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';

type AttributeFinderPropsType = {
  handleSelectOnChange: (event: any) => void;
  name: string;
  t: i18next.TFunction;
  openServerErrorDialog: OpenServerErrorDialogType;
  filterOption?: any;
  selectKey: string;
};

type SearchResultAPIType = APICaseManagement.EntityAttributeSearchList;

const provider = async (
  input: string,
  openServerErrorDialog: OpenServerErrorDialogType
): Promise<ValueType<string>[]> => {
  const result = await request<SearchResultAPIType>(
    'GET',
    buildUrl<APICaseManagement.SearchAttributeRequestParams>(
      '/api/v2/cm/attribute/search',
      {
        'filter[keyword]': input,
        page: 1,
        page_size: 50,
      }
    )
  ).catch(openServerErrorDialog);

  if (!result || !result.data || result.data.length === 0) return [];

  return result.data.map(result => ({
    label: result.attributes.label,
    value: ['attributes', 'custom_fields', result.attributes.magic_string].join(
      '.'
    ),
    data: result.attributes,
  }));
};

const AttributeFinder: FunctionComponent<AttributeFinderPropsType> = ({
  handleSelectOnChange,
  name,
  t,
  openServerErrorDialog,
  filterOption,
  selectKey,
}) => {
  const [input, setInput] = useState('');

  return (
    <DataProvider
      providerArguments={[input, openServerErrorDialog]}
      autoProvide={input !== ''}
      provider={provider}
    >
      {({ data, busy }) => (
        <Select
          name={name}
          key={selectKey}
          choices={data || []}
          isClearable={false}
          getChoices={setInput}
          onChange={(event: React.ChangeEvent<any>) => {
            const data = event.target.value.data as SearchResultAPIType['data'];
            handleSelectOnChange({
              target: {
                ...event.target,
                value: {
                  ...event.target.value,
                  data,
                },
              },
            });
          }}
          loading={busy}
          isSearchable={true}
          filterOption={filterOption}
          //@ts-ignore
          translations={{
            'form:choose': t('editForm.attributeFinder.placeholder'),
          }}
          generic={true}
          {...(filterOption && { filterOption })}
          cacheOptions={false}
        />
      )}
    </DataProvider>
  );
};

export default AttributeFinder;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';

type SetSortDialogPropsType = {
  onConfirm: any;
  t: i18next.TFunction;
  hide: any;
};

const SetSortDialog: FunctionComponent<SetSortDialogPropsType> = ({
  onConfirm,
  t,
  hide,
}) => {
  const formDefinition = [
    {
      name: 'sort',
      type: fieldTypes.SELECT,
      required: true,
      label: t('editForm.fields.columns.sortOrder'),
      choices: [
        {
          label: t('editForm.fields.columns.ascending'),
          value: 'asc',
        },
        {
          label: t('editForm.fields.columns.descending'),
          value: 'desc',
        },
      ],
    },
  ];

  return (
    <FormDialog
      /*
      // @ts-ignore */
      formDefinition={formDefinition}
      onSubmit={onConfirm}
      title={t('editForm.fields.columns.dialogTitle')}
      scope="column-entry-sort-dialog"
      icon="sort"
      onClose={hide}
    />
  );
};

export default SetSortDialog;

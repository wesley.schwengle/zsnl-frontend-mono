// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { FormikProps, FieldInputProps } from 'formik';
import * as i18next from 'i18next';
//@ts-ignore
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
//@ts-ignore
import { DepartmentFinder } from '@zaaksysteem/common/src/components/form/fields/DepartmentFinder/DepartmentFinder';
import RoleFinder from '@zaaksysteem/common/src/components/form/fields/RoleFinder/RoleFinder';
import classNames from 'classnames';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import {
  ModeType,
  ClassesType,
  EditFormStateType,
} from '../../../AdvancedSearch.types';
import { validateForm } from '../EditForm.library';

type PermissionsEntryPropsType = {
  field: FieldInputProps<any>;
  classes: ClassesType;
  mode: ModeType;
  index: number;
  arrayHelpers: any;
  t: i18next.TFunction;
  formik: FormikProps<EditFormStateType>;
};

/* eslint complexity: [2, 8] */
const PermissionsEntry: FunctionComponent<PermissionsEntryPropsType> = ({
  field,
  classes,
  mode,
  index,
  arrayHelpers,
  t,
  formik,
}) => {
  const departmentValueName = `${field.name}.groupID`;
  const roleValueName = `${field.name}.roleID`;
  const departmentValue = field?.value?.groupID;
  const saved = field?.value?.saved;
  const writeValueName = `${field.name}.writePermission`;
  const writeValue = field?.value?.writePermission;

  if (!field || !classes) return null;

  const handleDepartmentChange = (event: any) => {
    field.onChange({
      target: {
        name: departmentValueName,
        type: 'select',
        value: event.target.value.value,
      },
    });

    //reset role when changing department
    field.onChange({
      target: {
        name: roleValueName,
        type: 'select',
        value: null,
      },
    });
  };

  const handleRoleChange = (event: any) => {
    field.onChange({
      target: {
        name: roleValueName,
        type: 'select',
        value: event.target?.value || event.target?.value?.value,
      },
    });
  };

  return (
    <div className={classes.permissionsEntries}>
      <div className={classes.permissionsEntryWrapper}>
        <IconButton
          onClick={() => {
            arrayHelpers.remove(index);
            validateForm(formik);
          }}
          disableRipple={true}
          size="small"
          classes={{
            root: classes.rightCornerDeleteButton,
          }}
        >
          <Icon size="extraSmall" color="inherit">
            {iconNames.delete}
          </Icon>
        </IconButton>

        <div
          className={classNames(
            classes.permissionsEntryRow,
            classes.permissionsEntryRowSelect
          )}
        >
          <div>{t('editForm.fields.permissions.group.label') as string}</div>
          <div>
            <DepartmentFinder
              key={departmentValueName}
              {...field}
              name={departmentValueName}
              onChange={handleDepartmentChange}
              value={departmentValue}
              disabled={mode === 'edit' && saved === true}
              translations={{
                'form:choose': t(
                  'editForm.fields.permissions.group.placeholder'
                ),
              }}
            />
          </div>
        </div>
        {departmentValue && (
          <div
            className={classNames(
              classes.permissionsEntryRow,
              classes.permissionsEntryRowSelect
            )}
          >
            <div>{t('editForm.fields.permissions.role.label') as string}</div>
            <div>
              <RoleFinder
                key={roleValueName}
                {...field}
                name={roleValueName}
                value={field?.value?.roleID || null}
                onChange={handleRoleChange}
                config={{
                  parentRoleUuid: departmentValue,
                }}
                disabled={mode === 'edit' && saved === true}
                translations={{
                  'form:choose': t(
                    'editForm.fields.permissions.role.placeholder'
                  ),
                }}
              />
            </div>
          </div>
        )}
        <div className={classes.permissionsEntryRow}>
          <div>&nbsp;</div>
          <div>
            <Checkbox
              label={t('editForm.fields.permissions.mayEdit')}
              name={writeValueName}
              onChange={field.onChange}
              checked={writeValue}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default PermissionsEntry;

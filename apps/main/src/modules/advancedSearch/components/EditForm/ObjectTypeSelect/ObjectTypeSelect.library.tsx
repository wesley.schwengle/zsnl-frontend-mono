// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { KindType } from '../../../AdvancedSearch.types';

export const fetchObjectTypeChoices =
  (kind: KindType, onError: OpenServerErrorDialogType) =>
  async (keyword: string) => {
    // kind = custom_object, get from server
    // kind = case, static list

    if (kind === 'custom_object') {
      const body = await request<APICaseManagement.SearchResponseBody>(
        'GET',
        buildUrl<APICaseManagement.SearchRequestParams>('/api/v2/cm/search', {
          keyword,
          type: 'custom_object_type' as any,
        })
      ).catch(onError);

      if (!body || !body.data) return [];
      return body.data.map((el: any) => {
        return {
          value: el.id,
          label: el.meta?.summary,
        };
      });
    } else if (kind === 'case') {
      //TODO
      return [];
    }
  };

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useState, useEffect } from 'react';
import * as i18next from 'i18next';
import { FieldInputProps } from 'formik';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { KindType } from '../../../AdvancedSearch.types';
import { isUUID } from '../../../library/library';
import { fetchObjectTypeChoices } from './ObjectTypeSelect.library';

type ObjectTypeFinderPropsType = {
  kind: KindType;
  field: FieldInputProps<any>;
  t: i18next.TFunction;
};

const ObjectTypeFinder: FunctionComponent<ObjectTypeFinderPropsType> = ({
  kind,
  field: { onChange, value, name },
  t,
}) => {
  const [input, setInput] = useState('');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  //If loaded with a value, trigger the onchange
  useEffect(() => {
    if (value && value?.value && isUUID(value?.value)) {
      onChange({
        target: { name, type: 'select', value },
      });
    }
  }, []);

  return (
    <React.Fragment>
      <DataProvider
        autoProvide={input !== ''}
        provider={fetchObjectTypeChoices(kind, openServerErrorDialog)}
        providerArguments={[input]}
      >
        {({ data, busy }) => {
          const normalizedChoices = data || undefined;

          return (
            <Select
              name={name}
              value={value}
              choices={normalizedChoices}
              isClearable={true}
              loading={busy}
              getChoices={setInput}
              components={{
                Option: MultilineOption,
              }}
              filterOption={() => true}
              onChange={onChange}
              generic={true}
              //@ts-ignore
              translations={{
                'form:choose': t('editForm.fields.objectType.placeholder'),
              }}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default ObjectTypeFinder;

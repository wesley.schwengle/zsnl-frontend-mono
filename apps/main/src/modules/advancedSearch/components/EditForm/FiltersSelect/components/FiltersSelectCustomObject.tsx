// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { v4 } from 'uuid';
//@ts-ignore
import { Field, FormikProps, FieldInputProps } from 'formik';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { getFiltersChoices } from '../../Filters/Filters.library';
import { validateForm } from '../../EditForm.library';
import { getDefaultValue } from '../FiltersSelect.library';
import { EditFormStateType } from '../../../../AdvancedSearch.types';

type FiltersSelectCustomObjectPropsType = {
  classes: any;
  t: i18next.TFunction;
  arrayHelpers: any;
};

const FiltersSelectCustomObject: FunctionComponent<
  FiltersSelectCustomObjectPropsType
> = ({ classes, t, arrayHelpers }) => {
  const choices = getFiltersChoices({ t, kind: 'custom_object' });

  return (
    <div className={classes.filtersSelectCaseSelectors}>
      <span className={classes.filterRowLabel}>label</span>
      <div className={classes.filterRowContent}>
        <Field
          name={'selectedFilter'}
          component={SelectComponent}
          choices={choices}
          arrayHelpers={arrayHelpers}
          t={t}
        />
      </div>
    </div>
  );
};

type SelectComponentPropsType = {
  field: FieldInputProps<any>;
  form: FormikProps<EditFormStateType>;
  t: i18next.TFunction;
  choices: any;
  arrayHelpers: any;
};
const SelectComponent: FunctionComponent<SelectComponentPropsType> = ({
  field,
  form,
  t,
  choices,
  arrayHelpers,
}) => (
  <Select
    choices={choices}
    isClearable={false}
    nestedValue={true}
    disabled={!isPopulatedArray(choices)}
    filterOption={(option: any) => {
      const existing = form.values.filters?.filters.map(value => value.type);
      return !(existing || []).includes(option.value);
    }}
    generic={true}
    //@ts-ignore
    translations={{
      'form:choose': t('editForm.fields.filtersSelect.placeholder'),
    }}
    {...field}
    onChange={(event: React.ChangeEvent<any>) => {
      field.onChange(event);
      const targetValue = event.target?.value;
      const defaultValue = getDefaultValue(targetValue);
      arrayHelpers.insert(0, {
        type: targetValue,
        uuid: v4(),
        parameters: {
          label: '',
          value: defaultValue,
        },
      });
      validateForm(form);
    }}
  />
);

export default FiltersSelectCustomObject;

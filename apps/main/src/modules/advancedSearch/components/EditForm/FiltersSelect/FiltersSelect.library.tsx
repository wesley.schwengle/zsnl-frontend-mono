// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const getDefaultValue = (type: string) => {
  switch (type) {
    case 'keyword':
      return '';
    case 'attributes.last_modified':
      return null;
    default:
      return '';
  }
};

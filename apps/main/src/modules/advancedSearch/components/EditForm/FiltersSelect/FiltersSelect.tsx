// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import * as i18next from 'i18next';
import { FormikProps } from 'formik';
import { KindType, EditFormStateType } from '../../../AdvancedSearch.types';
import FiltersSelectCase from './components/FiltersSelectCase';
import FiltersSelectCustomObject from './components/FiltersSelectCustomObject';

type FiltersSelectPropsType = {
  arrayHelpersRef: any;
  formik: FormikProps<EditFormStateType>;
  t: i18next.TFunction;
  kind: KindType;
  openServerErrorDialog: OpenServerErrorDialogType;
  classes: any;
};

export const FiltersSelect: FunctionComponent<FiltersSelectPropsType> = ({
  formik,
  t,
  classes,
  arrayHelpersRef,
  kind,
  openServerErrorDialog,
}) => {
  if (!arrayHelpersRef) return null;

  return (
    <div className={classes.editFormSection}>
      {kind === 'case' && (
        <FiltersSelectCase
          t={t}
          classes={classes}
          arrayHelpers={arrayHelpersRef}
          formik={formik}
          openServerErrorDialog={openServerErrorDialog}
        />
      )}
      {kind === 'custom_object' && (
        <FiltersSelectCustomObject
          t={t}
          classes={classes}
          arrayHelpers={arrayHelpersRef}
        />
      )}
    </div>
  );
};

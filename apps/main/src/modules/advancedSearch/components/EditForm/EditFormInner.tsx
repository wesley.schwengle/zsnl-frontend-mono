// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, Dispatch, SetStateAction } from 'react';
import * as i18next from 'i18next';
import classNames from 'classnames';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
//@ts-ignore
import objectScan from 'object-scan';
import CircularProgress from '@mui/material/CircularProgress';
import { Field, Form, FormikProps } from 'formik';
import { QueryClient } from '@tanstack/react-query';
import Button from '@mintlab/ui/App/Material/Button';
import TextField from '@mintlab/ui/App/Material/TextField';
import { getFormikPassThrough } from '../FormikPassthrough';
import {
  AuthorizationsType,
  EditFormStateType,
  KindType,
  ModeType,
  ClassesType,
  IdentifierType,
} from '../../AdvancedSearch.types';
import { useTransition } from '../../hooks/useTransition';
import { QUERY_KEY_CUSTOM_FIELDS } from '../../query/constants';
import { usePrompt } from '../../hooks/usePrompt';
import { useCustomFields } from '../../query/useCustomFields';
import { isUUID } from '../../library/library';
import ObjectTypeSelect from './ObjectTypeSelect/ObjectTypeSelect';
import { resetForm, isFormValid } from './EditForm.library';
import EditFormContent from './EditFormContent';

const NameField = getFormikPassThrough(TextField);

type EditFormInnerPropsType = {
  formik: FormikProps<EditFormStateType>;
  classes: ClassesType;
  tabValue: number;
  handleTabChange: any;
  kind: KindType;
  client: QueryClient;
  setTabValue: Dispatch<SetStateAction<number>>;
  saveMutation: any;
  authorizations?: AuthorizationsType[];
  mode: ModeType;
  t: i18next.TFunction;
  identifier?: IdentifierType | null;
  openServerErrorDialog: OpenServerErrorDialogType;
};

/* eslint complexity: [2, 20] */
const EditFormInner: FunctionComponent<EditFormInnerPropsType> = ({
  formik,
  classes,
  tabValue,
  handleTabChange,
  kind,
  client,
  setTabValue,
  saveMutation,
  authorizations,
  mode,
  t,
  identifier,
  openServerErrorDialog,
}) => {
  const { values, errors } = formik;

  const errorPaths = objectScan(['**'], {
    joined: true,
    filterFn: ({ value }: any) => typeof value === 'string' && !isUUID(value),
  })(errors);

  const showEditForm =
    (kind === 'custom_object' && values.selectedObjectType) || kind === 'case';
  const customFieldsQuery = useCustomFields({
    uuid: values?.selectedObjectType?.value || null,
    config: {
      enabled:
        kind === 'custom_object' && Boolean(values?.selectedObjectType?.value),
      staleTime: 1000,
    },
  });

  // Resets when selected object type is deselected and set to null
  useTransition(
    (prev: any) => {
      if (values.selectedObjectType === null && prev !== null) {
        resetForm(formik);
        client.invalidateQueries([QUERY_KEY_CUSTOM_FIELDS]);
        setTabValue(0);
      }
    },
    [values.selectedObjectType]
  );

  usePrompt(t('dialogs.discardConfirm.content'), Boolean(formik.dirty));

  return (
    <>
      <Form
        className={classNames(
          classes.editFormWrapper,
          classes.editFormCentered
        )}
      >
        <div className={classes.editFormTop}>
          <div className={classes.editFormSection}>
            <span className={classes.filterRowLabel}>
              {t('editForm.fields.name.label') + ':'}
            </span>
            <div className={classes.filterRowContent}>
              <Field
                id="name"
                name="name"
                placeholder={t('editForm.fields.name.placeholder')}
                component={NameField}
                variant="generic1"
                validate={(value: string) =>
                  !value || value === ''
                    ? t('editForm.fields.name.errorMessage')
                    : null
                }
              />
            </div>
          </div>
          {kind === 'custom_object' ? (
            <div className={classes.editFormSection}>
              <span className={classes.filterRowLabel}>
                {t('editForm.fields.objectType.label') + ':'}
              </span>
              <div className={classes.filterRowContent}>
                <div className={classes.filterRowContent}>
                  <Field
                    name="selectedObjectType"
                    component={ObjectTypeSelect}
                    kind={kind}
                    t={t}
                  />
                </div>
              </div>
            </div>
          ) : null}
        </div>
        <div className={classes.editFormMiddle}>
          {showEditForm ? (
            <EditFormContent
              classes={classes}
              handleTabChange={handleTabChange}
              authorizations={authorizations}
              mode={mode}
              t={t}
              kind={kind}
              formik={formik}
              client={client}
              tabValue={tabValue}
              customFields={customFieldsQuery?.data}
              identifier={identifier}
              openServerErrorDialog={openServerErrorDialog}
            />
          ) : null}
        </div>
        <div className={classes.editFormBottom}>
          {isPopulatedArray(errorPaths) && (
            <div className={classes.editFormBottomErrors}>
              {get(errors, errorPaths[0])}
            </div>
          )}

          <div className={classes.editFormBottomButtons}>
            <Button
              action={() => formik.submitForm()}
              presets={['primary', 'contained']}
              disabled={!isFormValid({ formik, saveMutation })}
              {...(saveMutation.isLoading && {
                //@ts-ignore
                endIcon: <CircularProgress size={15} color="info" />,
              })}
            >
              {t('save')}
            </Button>
          </div>
        </div>
      </Form>
    </>
  );
};

export default EditFormInner;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, {
  FunctionComponent,
  useState,
  Dispatch,
  SetStateAction,
} from 'react';
import { QueryClient } from '@tanstack/react-query';
import * as i18next from 'i18next';
import DropdownMenu, {
  DropdownMenuList,
  //@ts-ignore
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
import Button from '@mintlab/ui/App/Material/Button';
import { useNavigate } from 'react-router-dom';
import {
  SavedSearchType,
  ClassesType,
  SnackType,
  IdentifierType,
} from '../../AdvancedSearch.types';
import { useSingleDeleteMutation } from '../../query/useSingle';
import { invalidateAllQueries } from '../../query/library';
import { hasAccess } from '../../library/library';

type RowPropsType = {
  data: SavedSearchType;
  classes: ClassesType;
  client: QueryClient;
  setSnack: Dispatch<SetStateAction<SnackType | null>>;
  t: i18next.TFunction;
  identifier?: IdentifierType;
};

const Row: FunctionComponent<RowPropsType> = ({
  data,
  classes,
  client,
  setSnack,
  t,
  identifier,
}) => {
  const active = identifier === data.uuid;
  const deleteMutation = useSingleDeleteMutation();
  const navigate = useNavigate();
  const [deleteUUID, setDeleteUUID] = useState<IdentifierType | null>(null);
  const { authorizations } = data;

  const startDelete = () => {
    deleteMutation.mutate(data.uuid, {
      onSuccess: () => {
        navigate('');
        invalidateAllQueries(client);
        setSnack({
          message: t('snacks.deleted'),
          open: true,
        });
      },
    });
  };
  const actions = [
    {
      action: () => navigate(`edit/${data.uuid}`),
      title: t('actions.edit'),
    },
    {
      action: () => setDeleteUUID(data.uuid),
      title: t('actions.delete'),
    },
  ];

  return (
    <>
      <ConfirmDialog
        open={Boolean(deleteUUID)}
        onConfirm={() => startDelete()}
        onClose={() => setDeleteUUID(null)}
        title={t('confirm')}
        body={
          <div>
            {
              t('deleteConfirm', {
                name: data.name,
              }) as string
            }
          </div>
        }
      />
      <div
        className={classNames(classes.leftBarRowWrapper, {
          [classes.savedSearchLinkActive]: active,
          [classes.savedSearchLinkInactive]: !active,
        })}
      >
        <Link
          className={classes.savedSearchLink}
          key={data.uuid}
          to={`view/${data.uuid}`}
        >
          {data.name}
        </Link>

        {hasAccess(authorizations, 'readwrite') && (
          <DropdownMenu
            transformOrigin={{
              vertical: -50,
              horizontal: 'center',
            }}
            trigger={
              <Button
                presets={['icon', 'extraSmall']}
                scope={`catalog-header:button-bar:advanced`}
                iconSize={'small'}
                className={classes.savedSearchLinkMoreButton}
              >
                more_vert
              </Button>
            }
          >
            <DropdownMenuList
              items={actions.map(({ action, title }) => ({
                action,
                label: title,
                scope: `catalog-header:button-bar:${title}`,
              }))}
            />
          </DropdownMenu>
        )}
      </div>
    </>
  );
};

export default Row;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useEffect, useState, useRef } from 'react';
import * as i18next from 'i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import classNames from 'classnames';
import {
  AuthorizationsType,
  ClassesType,
  IdentifierType,
  KindType,
  ModeType,
  ViewType,
} from '../../../AdvancedSearch.types';
import { DEFAULT_RESULTS_PER_PAGE } from '../../../library/config';
import { getLabel } from '../Main.library';
import MainButtons from '../MainButtons';
import ResultsTopBar from './ResultsTopBar';
import ResultsView from './ResultsView';
import ResultsPagination from './ResultsPagination';

type ResultsType = {
  classes: ClassesType;
  identifier: IdentifierType;
  kind: KindType;
  t: i18next.TFunction;
  mode: ModeType;
  authorizations?: AuthorizationsType[];
  currentQuery: any;
};

const Results: FunctionComponent<ResultsType> = ({
  classes,
  identifier,
  kind,
  t,
  mode,
  authorizations,
  currentQuery,
}) => {
  const [page, setPage] = useState(1);
  const topBarRightRef = useRef(null);
  const [resultsPerPage, setResultsPerPage] = useState(
    DEFAULT_RESULTS_PER_PAGE
  );
  const [totalResults, setTotalResults] = useState(0);
  const [isFetching, setIsFetching] = useState<boolean>(false);
  const [view, setView] = useState<ViewType>('table');

  useEffect(() => setPage(1), [resultsPerPage, identifier]);

  const topLabel = getLabel({ mode, currentQuery, t });

  return (
    <>
      <div className={classNames(classes.mainTopBar)}>
        <div className={classes.mainTopBarLeft}>
          <span className={classes.mainTopBarName}>{topLabel}</span>
          {
            <MainButtons
              identifier={identifier}
              mode={mode}
              authorizations={authorizations}
              t={t}
            />
          }
        </div>
        <div className={classes.mainTopBarRight} ref={topBarRightRef} />
      </div>

      <div className={classes.resultsWrapper}>
        {isFetching && <Loader className={classes.resultsTableLoader} />}
        <ResultsTopBar
          classes={classes}
          menuRef={topBarRightRef}
          view={view}
          t={t}
          setView={setView}
        />
        <div className={classes.resultsMain}>
          <ResultsView
            kind={kind}
            identifier={identifier}
            view={view}
            page={page}
            resultsPerPage={resultsPerPage}
            setTotalResults={setTotalResults}
            setIsFetching={setIsFetching}
          />
        </div>
        <ResultsPagination
          classes={classes}
          totalResults={totalResults}
          page={page}
          resultsPerPage={resultsPerPage}
          setPage={setPage}
          setResultsPerPage={setResultsPerPage}
        />
      </div>
    </>
  );
};

export default Results;

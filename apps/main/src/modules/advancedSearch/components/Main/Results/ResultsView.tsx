// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useEffect } from 'react';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { Geojson } from '@mintlab/ui/types/MapIntegration';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useSingleQuery } from '../../../query/useSingle';
import { useResultsQuery } from '../../../query/useResults';
import {
  IdentifierType,
  KindType,
  ViewType,
  ColumnType,
} from '../../../AdvancedSearch.types';
import {
  useResultsCountQuery,
  GetResultsReturnType,
} from '../../../query/useResults';
import { getColumns } from './Results.library';
import ResultsTable from './ResultsTable';
import ResultsMap from './ResultsMap';

type ResultsViewPropsType = {
  identifier: IdentifierType;
  kind: KindType;
  page: number;
  view: ViewType;
  resultsPerPage: number;
  setTotalResults?: any;
  setName?: any;
  setIsFetching?: any;
  keyword?: string;
};

/* eslint complexity: [2, 10] */
const ResultsView: FunctionComponent<ResultsViewPropsType> = ({
  identifier,
  kind,
  page,
  resultsPerPage,
  setTotalResults,
  setName,
  setIsFetching,
  view = 'table',
  keyword,
}) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const currentQuery = useSingleQuery({
    identifier,
    openServerErrorDialog,
  });
  const columns = currentQuery?.data?.columns;
  const sortColumn = currentQuery?.data?.sortColumn;
  const sortOrder = currentQuery?.data?.sortOrder;
  const filters = currentQuery?.data?.filters?.filters;

  const queryParams = {
    kind,
    identifier,
    page,
    resultsPerPage,
    openServerErrorDialog,
    columns,
    sortColumn,
    sortOrder,
    filters,
    geo: view === 'map',
    currentSuccess: currentQuery.isSuccess,
    keyword,
  };

  const resultsQuery = useResultsQuery(queryParams);
  const resultsQueryCount = useResultsCountQuery(
    cloneWithout(
      queryParams,
      'page',
      'resultsPerPage',
      'sortColumn',
      'sortOrder'
    )
  );

  useEffect(() => {
    if (setTotalResults) setTotalResults(resultsQueryCount.data);
  }, [resultsQueryCount.data]);

  useEffect(() => {
    if (setIsFetching) setIsFetching(resultsQuery.isFetching);
  }, [resultsQuery.isFetching]);

  if (currentQuery.status === 'loading' || resultsQuery.status === 'loading')
    return <Loader />;

  if (!resultsQuery?.data) {
    return null;
  }

  const resultsQueryData = (resultsQuery?.data as GetResultsReturnType).data;
  const rows = resultsQueryData || [];
  const features: Geojson[] = rows.reduce<Geojson[]>((acc, current) => {
    if (current?.geoFeatures && current?.geoFeatures.length) {
      acc = [...acc, ...current.geoFeatures];
    }
    return acc;
  }, []);

  return (
    <>
      {ServerErrorDialog}
      {view === 'table' && (
        <ResultsTable
          columns={getColumns(columns as ColumnType[])}
          rows={rows}
          sortColumn={sortColumn}
          sortOrder={sortOrder}
          kind={kind}
        />
      )}
      {view === 'map' && <ResultsMap features={features} />}
    </>
  );
};

export default ResultsView;

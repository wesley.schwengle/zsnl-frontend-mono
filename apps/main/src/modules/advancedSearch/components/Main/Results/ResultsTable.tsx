// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { ColumnType } from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';
import {
  SortDirectionType,
  ResultRowType,
  KindType,
} from '../../../AdvancedSearch.types';
import { useStyles } from './ResultsTable.styles';

type ResultsTablePropsType = {
  columns: ColumnType[];
  rows: ResultRowType[];
  sortColumn?: any;
  sortOrder?: SortDirectionType;
  kind: KindType;
};

const ResultsTable: FunctionComponent<ResultsTablePropsType> = ({
  columns,
  rows,
  sortColumn,
  sortOrder,
  kind,
}) => {
  const classes = useStyles();
  if (!rows) return null;

  const handleRowClick = ({ rowData }: any) => {
    const url =
      kind === 'custom_object'
        ? `/main/object/${rowData.id}`
        : `/redirect/case?uuid=${rowData.id}`;
    top?.window.open(url, '_new');
  };

  return (
    <div className={classes.resultsTableWrapper}>
      <div className={classes.resultsTable}>
        <SortableTable
          rows={rows.map(row => {
            return { ...row.columns };
          })}
          columns={columns}
          noRowsMessage={'[TR] Geen resultaten gevonden.'}
          rowHeight={50}
          loading={false}
          onRowClick={handleRowClick}
          onRowDoubleClick={() => {}}
          // No dynamic table sorting, providing sortBy and sortDirection as props
          sorting="none"
          sortInternal={false}
          {...(sortColumn && { sortColumn })}
          {...(sortOrder && { sortOrder: sortOrder.toUpperCase() })}
        />
      </div>
    </div>
  );
};

export default React.memo(ResultsTable);

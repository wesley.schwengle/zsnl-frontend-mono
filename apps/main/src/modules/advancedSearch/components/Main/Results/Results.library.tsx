// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { ColumnType as ColumnTypeTable } from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';
import { ColumnType } from '../../../AdvancedSearch.types';

export const getColumns = (columns: ColumnType[]): ColumnTypeTable[] => {
  return (columns || []).map(column => {
    const getSizeProps = () => {
      if (column.type === 'id' || column.type === 'uuid') {
        return {
          width: 150,
        };
      } else {
        return {
          width: 1,
          flexGrow: 1,
          flexShrink: 0,
        };
      }
    };

    const uniqueIdentifier = column.source.join('.');

    return {
      label: column.label,
      name: uniqueIdentifier,
      cellRenderer: ({ rowData }: any) => {
        return (
          <div>
            <Tooltip title={rowData[uniqueIdentifier]} enterNextDelay={1500}>
              {rowData[uniqueIdentifier]}
            </Tooltip>
          </div>
        );
      },
      ...getSizeProps(),
    };
  });
};

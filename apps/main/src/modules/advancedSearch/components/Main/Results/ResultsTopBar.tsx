// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { createPortal } from 'react-dom';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { ViewType, ClassesType } from '../../../AdvancedSearch.types';

type ResultsTopBarPropsType = {
  classes: ClassesType;
  menuRef: any;
  view: ViewType;
  t: i18next.TFunction;
  setView: any;
};

const ResultsTopBar: FunctionComponent<ResultsTopBarPropsType> = ({
  classes,
  menuRef,
  view,
  setView,
  t,
}) => {
  if (menuRef.current) {
    const Buttons = (
      <>
        <div>
          <Tooltip
            enterDelay={200}
            title={t('results.tableView')}
            placement={'bottom'}
          >
            <Button
              action={() => setView('table')}
              presets={['icon', 'small']}
              {...(view === 'table' && {
                className: classes.resultsViewSwitchActive,
              })}
            >
              table_chart
            </Button>
          </Tooltip>
        </div>
        <div>
          <Tooltip
            enterDelay={200}
            title={t('results.mapView')}
            placement={'bottom'}
          >
            <Button
              action={() => setView('map')}
              presets={['icon', 'small']}
              {...(view === 'map' && {
                className: classes.resultsViewSwitchActive,
              })}
            >
              map
            </Button>
          </Tooltip>
        </div>
      </>
    );

    return createPortal(Buttons, menuRef.current);
  }

  return null;
};

export default ResultsTopBar;

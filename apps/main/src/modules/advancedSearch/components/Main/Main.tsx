// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, Dispatch, SetStateAction } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { QueryClient } from '@tanstack/react-query';
import * as i18next from 'i18next';
//@ts-ignore
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar.cmp';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import EditForm from '../EditForm/EditForm';
import { isUUID } from '../../library/library';
import {
  AuthorizationsType,
  ClassesType,
  AdvancedSearchParamsType,
  SnackType,
} from '../../AdvancedSearch.types';
import { hasAccess } from '../../library/library';
import { useSingleQuery } from '../../query/useSingle';
import Results from './Results/Results';

type MainPropsType = {
  classes: ClassesType;
  snack: SnackType | null;
  setSnack: Dispatch<SetStateAction<SnackType | null>>;
  openServerErrorDialog: OpenServerErrorDialogType;
  client: QueryClient;
  authorizations?: AuthorizationsType[];
  t: i18next.TFunction;
};

/* eslint complexity: [2, 20] */
const Main: FunctionComponent<MainPropsType> = ({
  classes,
  snack,
  setSnack,
  openServerErrorDialog,
  client,
  t,
}) => {
  const { kind, mode, identifier } = useParams<
    keyof AdvancedSearchParamsType
  >() as AdvancedSearchParamsType;
  const currentQuery = useSingleQuery({
    identifier,
    openServerErrorDialog,
  });
  const navigate = useNavigate();
  const inSavedSearch = isUUID(currentQuery?.data?.uuid);
  const authorizations = currentQuery?.data?.authorizations;

  //General authorizations checks
  if (
    authorizations &&
    !hasAccess(authorizations, 'readwrite') &&
    (mode === 'edit' || mode === 'new')
  ) {
    navigate('../');
  }

  return (
    <>
      <Snackbar
        autoHideDuration={4000}
        handleClose={(event: React.ChangeEvent, reason: string) => {
          if (reason === 'clickaway') return;
          setSnack({
            message: null,
            open: false,
          });
        }}
        message={snack?.message || ''}
        open={snack?.open === true}
        classes={{
          root: classes.snack,
        }}
      />
      {mode === 'new' || mode === 'edit' ? (
        <EditForm
          classes={classes}
          mode={mode}
          identifier={identifier}
          currentQuery={currentQuery}
          kind={kind}
          setSnack={setSnack}
          client={client}
          openServerErrorDialog={openServerErrorDialog}
          authorizations={authorizations}
          t={t}
        />
      ) : null}
      {mode === 'view' && identifier && inSavedSearch && (
        <Results
          classes={classes}
          identifier={identifier}
          kind={kind}
          t={t}
          mode={mode}
          authorizations={authorizations}
          currentQuery={currentQuery}
        />
      )}
    </>
  );
};

export default Main;

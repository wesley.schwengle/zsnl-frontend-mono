// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import * as i18next from 'i18next';
import { useNavigate } from 'react-router-dom';
import {
  AuthorizationsType,
  ModeType,
  IdentifierType,
} from '../../AdvancedSearch.types';
import { hasAccess } from '../../library/library';

type MainButtonsPropsType = {
  identifier?: IdentifierType | null;
  mode: ModeType;
  authorizations?: AuthorizationsType[];
  t: i18next.TFunction;
};

const MainButtons = ({
  identifier,
  mode,
  authorizations,
  t,
}: MainButtonsPropsType) => {
  const navigate = useNavigate();

  return mode === 'view' &&
    identifier &&
    authorizations &&
    hasAccess(authorizations, 'readwrite') ? (
    <Button
      presets={['primary', 'contained', 'small']}
      action={() => {
        navigate(`../edit/${identifier}`);
      }}
    >
      {t('buttons.edit')}
    </Button>
  ) : null;
};

export default MainButtons;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    save: 'Opslaan',
    new: 'Nieuw',
    searchIn: 'Zoeken in',
    selectMessage:
      'Selecteer een opgeslagen zoekopdracht om de resultaten op te vragen, of maak een nieuwe zoekopdracht aan.',
    kind: {
      objects: 'Objecten',
      cases: 'Zaken',
    },
    dialogs: {
      discardConfirm: {
        title: 'Bevesting',
        content:
          'Uw wijzigingen zullen verloren gaan. Weet u zeker dat u door wilt gaan?',
      },
    },
    columns: {
      id: 'Identificatienummer',
      title: 'Titel',
      subtitle: 'Subtitel',
      externalReference: 'Externe referentie',
      dateCreated: 'Datum aangemaakt',
      lastModified: 'Laatst gewijzigd',
    },
    snacks: {
      deleted: 'De zoekopdracht is verwijderd.',
      saved: 'De zoekopdracht is opgeslagen.',
    },
    actions: {
      edit: 'Wijzigen',
      delete: 'Verwijderen',
    },
    buttons: {
      edit: 'Wijzigen',
    },
    newSavedSearch: 'Nieuwe zoekopdracht aanmaken',
    loadMore: 'meer laden',
    confirm: 'Bevestigen',
    deleteConfirm:
      'U staat op het punt om de opgeslagen zoekopdracht met de naam "{{ name }}" te verwijderen. Weet u zeker dat u door wilt gaan?',
    editForm: {
      tabs: {
        filters: 'filters',
        permissions: 'delen',
        columns: 'kolommen',
      },
      methods: {
        systemAttributes: 'Systeemkenmerken',
        searchAttributes: 'Zoek kenmerken',
      },
      fields: {
        name: {
          label: 'Naam',
          placeholder: 'Zoekopdracht naam',
          errorMessage: 'Vul een naam in voor de zoekopdracht.',
        },
        objectType: {
          label: 'Objecttype',
          placeholder: 'Zoek een objecttype…',
        },
        filtersSelect: {
          label: 'Filters toevoegen',
          placeholder: 'Selecteer een filter…',
        },
        filters: {
          label: 'Filters',
          noFiltersLabel: 'Deze zoekopdracht heeft nog geen filters.',
          typeNotSupported: 'Filtertype nog niet ondersteund: {{ type }}',
          fields: {
            status: {
              label: 'Status',
              placeholder: 'Kies een waarde…',
              errorMessage: 'Kies een waarde',
              choices: {
                active: 'Actief',
                inactive: 'Inactief',
                draft: 'Opzet',
              },
            },
            archiveStatus: {
              label: 'Archiveerstatus',
              placeholder: 'Kies een waarde…',
              errorMessage: 'Kies een waarde',
              choices: {
                archived: 'Gearchiveerd',
                toDestroy: 'Vernietigen',
                toPreserve: 'Bewaren',
              },
            },
            modified: {
              label: 'Laatst aangepast',
            },
            keyword: {
              label: 'Trefwoord',
              placeholder: 'Vul {{ name }} in',
              errorMessage: 'Vul een waarde voor {{ name }}',
            },
            archivalState: {
              label: 'Archiveerstatus',
              choices: {
                vernietigen: 'Vernietigen',
                overdragen: 'Overdragen',
              },
            },
          },
          types: {
            text: {
              errorMessage: 'Vul een waarde in',
              placeholder: 'Vul een waarde in',
            },
            dateEntries: {
              datePlaceholder: 'Selecteer een datum',
              date: {
                placeholder: 'Selecteer een datum…',
                errorMessage: 'Selecteer een datum',
              },
              operators: {
                lt: 'Eerder dan',
                le: 'Eerder dan of op',
                gt: 'Later dan',
                ge: 'Later dan of op',
                eq: 'Op',
                ne: 'Niet op',
              },
              operator: {
                placeholder: 'Kies een optie…',
              },
              titles: {
                absolute: 'Vast datumpunt',
                relative: 'Relatief datumpunt',
                range: 'Periode',
              },
              manualInput: 'Handmatige invoer',
              quickSelect: 'Snelle selectie',
              intervals: {
                minutes: 'minuten',
                hours: 'uur',
                days: 'dagen',
                months: 'maanden',
                years: 'jaar',
              },
              inLabel: 'in',
              between: 'Tussen',
              and: 'en',
              from: 'Van',
              untill: 'Tot',
              presets: {
                now: 'Nu',
                lastDay: ' Sinds gisteren',
                lastHour: 'Afgelopen uur',
                last7Days: 'Afgelopen 7 dagen',
                last30Days: 'Afgelopen 30 dagen',
                lastYear: 'Afgelopen jaar',
              },
              placeholders: {
                relativeNr: 0,
                relativeInterval: 'Keuze…',
                time: 'Kies tijd…',
              },
              periods: {
                '-': 'het verleden',
                '+': 'de toekomst',
              },
              includeTimes: 'Inclusief start- en eindtijd',
              errors: {
                timeRange:
                  'De einddatum en -tijd moeten na de begindatum en -tijd liggen.',
                dateRange: 'De einddatum moet op of na de begindatum liggen.',
                minimumEntries: 'Voeg minimaal één datumpunt toe.',
              },
              okButton: 'OK',
              cancelButton: 'Annuleren',
            },
            select: {},
          },
          operators: {
            eq: 'Gelijk aan',
            ne: 'Ongelijk aan',
          },
          addFilters: {
            systemAttributes: 'Selecteer om toe te voegen…',
            searchAttributes: 'Typ om kenmerk te zoeken…',
          },
          customFieldOperators: {
            and: 'En',
            or: 'En/of',
          },
        },
        CustomFieldsOperatorSelect: {
          label: 'Kenmerkfilters combineren met',
        },
        permissions: {
          hint: "Deze zoekopdracht is nog niet gedeeld. Maak een nieuwe afdeling/rol-combinatie aan via de 'Nieuw' knop.",
          mayEdit: 'Mag bewerken',
          add: 'Toevoegen',
          group: {
            label: 'Afdeling',
            placeholder: 'Kies een afdeling…',
            errorMessage: 'Kies een afdeling.',
          },
          role: {
            label: 'Rol',
            placeholder: 'Kies een rol…',
            errorMessage: 'Kies een rol.',
          },
          helpInfo: {
            intro:
              'Bij het aanmaken en bewerken van deze rechten gelden de volgende regels per Afdeling/Rol-combinatie:',
            owner:
              'De eigenaar van de zoekopdracht mag alle aspecten van de zoekopdracht bekijken en wijzigen. Bij het aanmaken van een nieuwe zoekopdracht wordt u automatisch de eigenaar.',
            sharedEditing:
              'De zoekopdracht is met een gebruiker gedeeld *met* bewerken aangevinkt: de gebruiker mag de zoekopdracht gebruiken en wijzigen, behalve het tabblad Delen.',
            sharedNotEditing:
              'De zoekopdracht is met een gebruiker gedeeld *zonder* bewerken aangevinkt: de gebruiker mag alleen de zoekopdracht gebruiken.',
          },
        },
        columns: {
          sortOn: 'Sorteer op {{ column }} ',
          ascending: 'Oplopend',
          descending: 'Aflopend',
          sortOrder: 'Sorteervolgorde',
          dialogTitle: 'Zet sorteervolgorde',
          errorMessage: 'Kies minimaal één kolom.',
          selectColumnPlaceholder: 'Voeg kolom toe…',
          categories: {
            systemAttributes: 'Systeemkenmerken',
            requestor: 'Aanvrager',
            assignee: 'Behandelaar',
            coordinator: 'Coördinator',
            correspondence: 'Correspondentie',
            dates: 'Datums',
            result: 'Resultaat',
            case: 'Zaak',
            other: 'Overig',
          },
        },
      },
      attributeFinder: {
        placeholder: 'Typ om kenmerk te zoeken…',
      },
    },
    results: {
      tableView: 'Tabelweergave',
      mapView: 'Kaartweergave',
    },
  },
};

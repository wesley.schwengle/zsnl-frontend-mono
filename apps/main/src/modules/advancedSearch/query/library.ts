// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { QueryClient } from '@tanstack/react-query';
import {
  QUERY_KEY_SAVEDSEARCHES,
  QUERY_KEY_SAVEDSEARCH,
  QUERY_KEY_RESULTS,
  QUERY_KEY_RESULTS_COUNT,
  QUERY_KEY_CUSTOM_FIELDS,
} from './constants';

export const invalidateAllQueries = (client: QueryClient) => {
  client.invalidateQueries([QUERY_KEY_SAVEDSEARCHES]);
  client.invalidateQueries([QUERY_KEY_SAVEDSEARCH]);
  client.invalidateQueries([QUERY_KEY_RESULTS]);
  client.invalidateQueries([QUERY_KEY_RESULTS_COUNT]);
  client.invalidateQueries([QUERY_KEY_CUSTOM_FIELDS]);
};

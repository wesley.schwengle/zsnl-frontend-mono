// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  V2ServerErrorsType,
  V2ServerErrorType,
} from '@zaaksysteem/common/src/types/ServerError';
//@ts-ignore
import addDuration from 'date-fns-duration';
import { useQuery } from '@tanstack/react-query';
import fecha from 'fecha';
import add from 'date-fns/add';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { get } from '@mintlab/kitchen-sink/source';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { APIGeo } from '@zaaksysteem/generated/types/APIGeo.types';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import {
  ColumnType,
  FilterType,
  IdentifierType,
  KindType,
  SortDirectionType,
  ResultRowType,
  DateEntryType,
} from '../AdvancedSearch.types';
import { getFromQueryValues } from '../library/library';
import { QUERY_KEY_RESULTS, QUERY_KEY_RESULTS_COUNT } from './constants';

const COUNT_STALE_TIME = 60 * 1000;
export type GetResultsReturnType = {
  data: ResultRowType[];
};

// Rows from the search API --> Table rows
const APIResultsToResults = (
  rows: APICaseManagement.SearchCustomObjectsResponseBody['data'],
  columns: ColumnType[]
): ResultRowType[] => {
  const resultValueObjToColumnValue = (resultValue: any) => {
    const actualValue = () => {
      return resultValue?.type && resultValue?.value
        ? resultValue.value
        : resultValue;
    };
    const safeValue = (value: any) => `${value}`;
    const type = resultValue?.type;
    const isDate = (value: any) => {
      const testDate = new Date(value);
      //@ts-ignore
      return testDate instanceof Date && !isNaN(testDate);
    };

    if (resultValue === null || resultValue === undefined) {
      return '';
    } else if (isDate(actualValue())) {
      const dateObj = new Date(actualValue());
      return fecha.format(dateObj, 'DD-M-YYYY HH:mm');
    } else if (
      typeof resultValue === 'string' ||
      resultValue instanceof String
    ) {
      return resultValue;
    } else if (type) {
      if (type === 'text') {
        return safeValue(actualValue());
      } else if (type === 'address_v2') {
        return actualValue()?.address?.full || '';
      } else {
        return safeValue(actualValue());
      }
    } else {
      return safeValue(actualValue());
    }
  };

  return rows.map(row => {
    let rowObj: any = {};
    const {
      id,
      attributes: { version_independent_uuid = '' },
    } = row;
    columns.map(column => {
      const joined = column.source.join('.');
      const resultValue = get(row, joined, '');
      if (resultValue) {
        rowObj[joined as string] = resultValueObjToColumnValue(resultValue);
      }
    });
    return {
      columns: rowObj,
      uuid: id,
      versionIndependentUuid: version_independent_uuid,
    };
  });
};

/* eslint complexity: [2, 20] */
export const filtersToAPIFilters = (
  filters: FilterType[] | null,
  keyword?: string | null
) => {
  const lastModifiedToSearchFilters = (
    filterValues: DateEntryType[] | null
  ) => {
    if (!filterValues) return;
    return filterValues.flatMap(filter => {
      if (filter.type === 'absolute') {
        return `${filter.operator} ${filter.value}`;
      } else if (filter.type === 'relative') {
        const modifiedDate = addDuration(new Date(), filter.value);
        return `${filter.operator} ${modifiedDate.toISOString()}`;
      } else if (filter.type === 'range') {
        if (filter.timeSetByUser === true) {
          return [`ge ${filter.startValue}`, `le ${filter.endValue}`];
        } else {
          const dayLater = add(new Date(filter.endValue as string), {
            days: 1,
          });
          return [`gt ${filter.startValue}`, `lt ${dayLater.toISOString()}`];
        }
      }
    });
  };

  const filterValueToAPIValue = (filter: FilterType) => {
    switch (filter.type) {
      case 'keyword':
        return filter.parameters.value;
      case 'relationship.custom_object_type':
      case 'attributes.status':
      case 'attributes.archive_status':
      case 'attributes.archival_state':
        return filter.parameters.value;
      case 'attributes.last_modified':
        return lastModifiedToSearchFilters(filter.parameters.value);
    }
  };

  if (!filters) return [];
  let dbFilters = filters.reduce((acc: any, filter: FilterType) => {
    const { type } = filter;
    const dbFiltersMap: any = {
      'relationship.custom_object_type': 'relationship.custom_object_type.id',
    };

    if (!type) return acc;
    const mappedName = dbFiltersMap[type] || type;
    acc[mappedName] = filterValueToAPIValue(filter);
    return acc;
  }, {});

  if (keyword) {
    dbFilters.keyword = [dbFilters.keyword, keyword].filter(Boolean).join(' ');
  }

  return dbFilters;
};

type GetResultsPropsType = {
  count: boolean;
};

/* eslint complexity: [2, 20] */
export const getResults =
  ({ count }: GetResultsPropsType) =>
  async ({
    queryKey,
  }: {
    queryKey: any;
  }): Promise<GetResultsReturnType | number> => {
    const kind = getFromQueryValues<KindType>(queryKey[1], 'kind');
    const page = getFromQueryValues<number>(queryKey[1], 'page');
    const page_size = getFromQueryValues<number>(queryKey[1], 'resultsPerPage');
    const sortColumn = getFromQueryValues<string>(queryKey[1], 'sortColumn');
    const sortOrder = getFromQueryValues<SortDirectionType>(
      queryKey[1],
      'sortOrder'
    );
    const geo = getFromQueryValues<boolean>(queryKey[1], 'geo');
    const filters = getFromQueryValues<FilterType[]>(queryKey[1], 'filters');
    const columns = getFromQueryValues<ColumnType[]>(queryKey[1], 'columns');
    const keyword = getFromQueryValues<string>(queryKey[1], 'keyword');

    const baseUrl = `${
      kind === 'custom_object'
        ? '/api/v2/cm/custom_object/search'
        : '/api/v2/cm/case/search'
    }${count ? '/count' : ''}`;

    const sort =
      sortColumn && sortOrder
        ? `${sortOrder === 'desc' ? '-' : ''}${sortColumn.replace(
            'attributes.',
            ''
          )}`
        : null;

    const parsedFilters = filtersToAPIFilters(filters, keyword);

    const url = buildUrl(baseUrl, {
      ...(page && { page }),
      ...(page_size && { page_size }),
      ...(filters && { filter: parsedFilters }),
      ...(sort && { sort }),
    });

    const results = await request<any>('GET', url).catch(
      (serverError: V2ServerErrorType) => {
        if (!count) throw serverError;
      }
    );

    if (count) return results?.data?.attributes?.total_results || 0;

    const parsedResults =
      results.data && isPopulatedArray(results?.data)
        ? APIResultsToResults(results.data, columns || [])
        : [];

    // If geo coordinates is also requested, retrieve the
    // geo features and merge those with the results
    if (geo && parsedResults && parsedResults.length) {
      const geoURL = buildUrl('/api/v2/geo/get_geo_features', {
        uuid: parsedResults.map(result => result.versionIndependentUuid),
      });
      const geoResults = await request<APIGeo.GetGeoFeaturesResponseBody>(
        'GET',
        geoURL
      ).catch((serverError: V2ServerErrorType) => {
        throw serverError;
      });

      const mergedResults = parsedResults.map(result => {
        const features =
          geoResults.data.find(
            geoResult => geoResult.id === result.versionIndependentUuid
          )?.attributes?.geo_json?.features || null;

        return {
          ...result,
          ...(features &&
            Array.isArray(features) &&
            features.length && { geoFeatures: features }),
        };
      });

      return {
        data: mergedResults,
      };
    } else {
      return {
        data: parsedResults,
      };
    }
  };

type UseResultsQueryParamsType = {
  identifier: IdentifierType;
  page: number;
  resultsPerPage: number;
  openServerErrorDialog: OpenServerErrorDialogType;
  kind: KindType;
  geo: boolean;
  currentSuccess: boolean;
  sortOrder?: SortDirectionType;
  sortColumn?: string;
  filters?: FilterType[] | null;
  columns?: ColumnType[];
  keyword?: string;
};
export const useResultsQuery = ({
  identifier,
  page,
  resultsPerPage,
  openServerErrorDialog,
  kind,
  geo,
  sortColumn,
  sortOrder,
  filters,
  columns,
  keyword,
  currentSuccess,
}: UseResultsQueryParamsType) =>
  useQuery(
    [
      QUERY_KEY_RESULTS,
      {
        identifier,
        page,
        resultsPerPage,
        kind,
        sortColumn,
        sortOrder,
        filters,
        geo,
        columns,
        keyword,
      },
    ],
    getResults({ count: false }),
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
      enabled: currentSuccess && Boolean(identifier),
      keepPreviousData: true,
    }
  );

export const useResultsCountQuery = ({
  identifier,
  openServerErrorDialog,
  kind,
  geo,
  filters,
  columns,
  keyword,
  currentSuccess,
}: UseResultsQueryParamsType) =>
  useQuery(
    [
      QUERY_KEY_RESULTS_COUNT,
      {
        identifier,
        kind,
        filters,
        geo,
        columns,
        keyword,
      },
    ],
    getResults({ count: true }),
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
      enabled: currentSuccess && Boolean(identifier),
      keepPreviousData: true,
      staleTime: COUNT_STALE_TIME,
    }
  );

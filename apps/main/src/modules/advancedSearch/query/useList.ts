// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useInfiniteQuery } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { V2ServerErrorType } from '@zaaksysteem/common/src/types/ServerError';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { SAVED_SEARCHES_PAGE_LENGTH } from '../library/config';
import { SavedSearchType, KindType } from '../AdvancedSearch.types';
import { mapAPISavedSearch } from './transforming';
import { QUERY_KEY_SAVEDSEARCHES } from './constants';

export type GetSavedSearchesReturnType = {
  data: SavedSearchType[];
  nextPage: number | undefined;
};
export const getSavedSearches = async ({
  pageParam = 1,
  queryKey,
}: any): Promise<GetSavedSearchesReturnType> => {
  const kind = queryKey[1].kind;
  const url = buildUrl('/api/v2/cm/saved_search/list', {
    page_size: SAVED_SEARCHES_PAGE_LENGTH,
    page: pageParam,
    'filter[kind]': kind,
  });

  const results = await request<APICaseManagement.ListSavedSearchResponseBody>(
    'GET',
    url
  ).catch((serverError: V2ServerErrorType) => {
    throw serverError;
  });

  const parsedResults =
    results.data && isPopulatedArray(results?.data)
      ? results.data.map(mapAPISavedSearch)
      : [];

  const nextPage = Boolean(parsedResults.length === SAVED_SEARCHES_PAGE_LENGTH);

  return {
    data: parsedResults,
    nextPage: nextPage ? pageParam + 1 : undefined,
  };
};

export const useSavedSearchesQuery = ({
  config = {},
  kind,
  openServerErrorDialog,
}: {
  config?: any;
  kind: KindType | null;
  openServerErrorDialog: OpenServerErrorDialogType;
}) => {
  return useInfiniteQuery<GetSavedSearchesReturnType, V2ServerErrorType>(
    [QUERY_KEY_SAVEDSEARCHES, { kind }],
    getSavedSearches,
    {
      ...config,
      getNextPageParam: lastPage => lastPage.nextPage,
      enabled: Boolean(kind),
      onError: (error: any) => openServerErrorDialog(error),
    }
  );
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery, useMutation } from '@tanstack/react-query';
import { v4 } from 'uuid';
import { V2ServerErrorType } from '@zaaksysteem/common/src/types/ServerError';
import { buildUrl } from '@mintlab/kitchen-sink/source';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
//@ts-ignore
import { asArray } from '@mintlab/kitchen-sink/source';

import {
  SavedSearchType,
  IdentifierType,
  ModeType,
  KindType,
  AuthorizationsType,
  EditFormStateType,
  PermissionType,
  FilterType,
} from '../AdvancedSearch.types';
import { replaceKeysInJSON } from '../library/library';
import { isUUID } from '../library/library';
import { hasAccess } from '../library/library';
import { getFromQueryValues } from '../library/library';
import { filtersKeyNamesReplacements } from '../library/config';
import { mapAPISavedSearch } from './transforming';
import { QUERY_KEY_SAVEDSEARCH } from './constants';

/* eslint complexity: [2, 20] */
export const formikPermissionsToAPIPermissions = (
  permissions: PermissionType[]
) =>
  permissions.map(permission => ({
    group_id: permission.groupID,
    role_id: permission.roleID,
    permission:
      permission.writePermission === true ? ['read', 'write'] : ['read'],
  }));

/* eslint complexity: [2, 20] */
const formikValuesToAPIPayload = ({
  values,
  kind,
  identifier,
  mode,
  authorizations,
}: {
  values: EditFormStateType;
  kind: KindType;
  identifier?: IdentifierType | null;
  mode: ModeType;
  authorizations?: AuthorizationsType[];
}) => {
  const processFilters = (filters: FilterType[]) =>
    filters.map(filter => cloneWithout(filter, 'uuid'));

  const uuid = mode === 'edit' ? identifier : v4();
  const { name, filters, permissions, columns, sortColumn, sortOrder } = values;
  const allFilters = [...(filters?.filters || [])];
  const payload: any = {
    uuid,
    name,
  };

  if (mode === 'new') payload.kind = kind;

  // Re-assemble all filters
  if (
    kind === 'custom_object' &&
    values.selectedObjectType &&
    isUUID(values?.selectedObjectType?.value)
  ) {
    allFilters.push({
      type: 'relationship.custom_object_type',
      uuid: v4(),
      parameters: {
        value: values.selectedObjectType.value as string,
        label: values.selectedObjectType.label,
      },
    });
  }

  if (
    mode === 'new' ||
    (mode === 'edit' && authorizations && hasAccess(authorizations, 'admin'))
  ) {
    payload.permissions = formikPermissionsToAPIPermissions(permissions);
  }

  payload.filters = {
    ...(kind === 'case' && {
      custom_fields_operator: filters?.customFieldsOperator,
    }),
    kind_type: kind,
    filters: processFilters(allFilters),
  };
  payload.columns = columns;
  payload.sort_column = sortColumn;
  payload.sort_order = sortOrder;

  return replaceKeysInJSON(payload, filtersKeyNamesReplacements);
};

export const saveSavedSearch = async ({
  payload,
  mode,
}: {
  payload: any;
  mode: ModeType;
}) => {
  const url =
    mode === 'new'
      ? '/api/v2/cm/saved_search/create'
      : '/api/v2/cm/saved_search/update';

  return request<APICaseManagement.CreateSavedSearchResponseBody>('POST', url, {
    ...payload,
  })
    .then(() => payload.uuid)
    .catch((serverError: V2ServerErrorType) => {
      throw serverError;
    });
};

export const deleteSavedSearch = async (uuid: string) => {
  const url = '/api/v2/cm/saved_search/delete';
  return request<APICaseManagement.DeleteSavedSearchResponseBody>('POST', url, {
    uuid,
  }).catch((serverError: V2ServerErrorType) => {
    throw serverError;
  });
};

export const getSavedSearch = async ({
  queryKey,
}: {
  queryKey: any;
}): Promise<SavedSearchType> => {
  const identifier = getFromQueryValues<string>(queryKey[1], 'identifier');
  const url = buildUrl('/api/v2/cm/saved_search/get', {
    uuid: identifier,
  });

  const results = await request<APICaseManagement.GetSavedSearchResponseBody>(
    'GET',
    url
  ).catch((serverError: V2ServerErrorType) => {
    throw serverError;
  });

  const result = results.data
    ? asArray(results.data).map(mapAPISavedSearch)
    : [];

  return result[0];
};

export const useSingleQuery = ({
  identifier,
  openServerErrorDialog,
}: {
  identifier?: IdentifierType | null;
  openServerErrorDialog: OpenServerErrorDialogType;
}) => {
  return useQuery<SavedSearchType, V2ServerErrorType>(
    [QUERY_KEY_SAVEDSEARCH, { identifier }],
    getSavedSearch,
    {
      enabled: Boolean(identifier),
      onError: (error: any) => openServerErrorDialog(error),
    }
  );
};

export const useSingleSaveMutation = ({
  kind,
  identifier,
  mode,
  authorizations,
}: {
  kind: KindType;
  identifier?: IdentifierType | null;
  mode: ModeType;
  authorizations?: AuthorizationsType[];
}) => {
  return useMutation((values: any) => {
    const payload = formikValuesToAPIPayload({
      values,
      kind,
      identifier,
      mode,
      authorizations,
    });

    return saveSavedSearch({
      payload,
      mode,
    });
  });
};

export const useSingleDeleteMutation = () =>
  useMutation((uuid: string) => deleteSavedSearch(uuid));

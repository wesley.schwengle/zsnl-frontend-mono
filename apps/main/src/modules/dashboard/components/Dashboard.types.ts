// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type DashboardState = {
  layout: Layout[];
  components: {
    component: any;
    params: any;
  }[];
  widgets: [] | Widget[];
};

export type Widget = {
  dimensions: {
    height: number;
    width: number;
    x: number;
    y: number;
  };
  parameters:
    | {
        saved_search_id: string;
        type: 'saved_search';
      }
    | {
        type: 'favorite_case_type';
      }
    | {
        type: 'tasks';
        filters: { label: string; value: string; data: any }[];
        columns: {
          id: string;
          value: string;
          active: boolean;
          isNew: boolean;
        }[];
      }
    | {
        title: string;
        type: 'external_url';
        url: string;
      };
};

export type Layout = {
  i: string;
  x: number;
  y: number;
  h: number;
  w: number;
  minW?: number;
  minH?: number;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles(({ mintlab: { greyscale } }: Theme) => ({
  wrapper: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexFlow: 'column',
  },
  content: {
    flexGrow: 1,
    height: 1,
    background: '#f2f2f2',
  },
  viewWrapper: {
    display: 'flex',
    flexDirection: 'column',
  },
  viewToolbar: {
    padding: '20px',
    borderBottom: `1px solid ${greyscale.dark}`,
  },
  view: {
    flexGrow: 1,
  },
}));

export const useDashboardStyles = makeStyles({
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: 0,
    height: '100%',
  },
});

export const useDashboardSortableTableStyles = makeStyles(
  ({ palette: { basalt }, mintlab: { greyscale } }: Theme) => ({
    tableHeader: {
      backgroundColor: '#F6F6F6',
      borderBottom: '1px solid #e5e5e5',
      color: basalt.lightest,
      fontSize: 13.3333,
      paddingLeft: 0,
      '&:hover': {
        backgroundColor: 'rgba(0, 0, 0, 0.05)',
      },
    },
    sortHeader: {
      color: greyscale.black,
    },
  })
);

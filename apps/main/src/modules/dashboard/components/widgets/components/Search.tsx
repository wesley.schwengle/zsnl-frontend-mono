// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDebouncedCallback } from 'use-debounce';
import TextField from '@mintlab/ui/App/Material/TextField';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Button from '@mintlab/ui/App/Material/Button';
import { useWidgetSearchStyles } from '../Widget.style';

type SearchPropsType = {
  hasIcon?: boolean;
  onSearch: (value: string) => void;
};

const DELAY = 300;
const MINLENGTH = 3;

const Search = ({ hasIcon, onSearch }: SearchPropsType) => {
  const [t] = useTranslation('widget');
  const [doSearch, setDoSearch] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [debouncedCallback] = useDebouncedCallback(value => {
    if (!value.length || value.length >= MINLENGTH) onSearch(value);
  }, DELAY);
  const classes = useWidgetSearchStyles();

  const handleChange = (value: React.ChangeEvent<HTMLInputElement>) => {
    const searchValue = value.target.value;
    setSearchTerm(searchValue);
    debouncedCallback(searchValue.trim());
  };

  const handleClose = () => {
    setSearchTerm('');
    setDoSearch(false);
    onSearch('');
  };

  const handleSearchKeyPress = (input: React.KeyboardEvent) => {
    const key = input.key.toLowerCase();

    if (key === 'enter') onSearch(searchTerm);
    if (key === 'escape') handleClose();
  };

  const maybeIcon = hasIcon ? (
    <Icon size="small">{iconNames.search}</Icon>
  ) : null;

  return (
    <>
      <Button presets={['icon', 'small']} action={() => setDoSearch(true)}>
        search
      </Button>
      {doSearch && (
        <div className={classes.overlay}>
          <TextField
            // eslint-disable-next-line jsx-a11y/no-autofocus
            autoFocus={true}
            value={searchTerm}
            id="search"
            name="search"
            onChange={handleChange}
            onKeyDown={handleSearchKeyPress}
            closeAction={handleClose}
            placeholder={t('search.placeholder')}
            classes={classes}
            variant="generic1"
            {...(maybeIcon && { startAdornment: maybeIcon })}
          />
        </div>
      )}
    </>
  );
};

export default Search;

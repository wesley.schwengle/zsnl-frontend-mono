// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FormikProps } from 'formik';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { useTheme } from '@mui/material';
import { Theme } from '@mintlab/ui/types/Theme';
import Button from '@mintlab/ui/App/Material/Button';
import { FilterType } from '../Tasks/types/Tasks.types';
import { useWidgetFilterStyles, useWidgetHeaderStyles } from '../Widget.style';
import { getIcon } from '../Tasks/Tasks.library';
import WidgetHeader from './WidgetHeader';
import {
  multiValueLabelStyles,
  widgetHeaderMultiStyles,
} from './MultiValueLabel.style';
import FilterForm from './FilterForm/FilterForm';

type FilterPropsType = {
  title: string;
  filters: null | FilterType[];
  setFilters: (values: FilterType[]) => void;
};

const Filter = ({ title, filters, setFilters }: FilterPropsType) => {
  const [filterForm, setFilterForm] = useState(false);
  const [formik, setFormik] = useState(null as any);
  const [t] = useTranslation('widget');
  const theme = useTheme<Theme>();
  const classes = useWidgetFilterStyles();
  const headerClasses = useWidgetHeaderStyles();

  const filterHeaderOptions = (
    <>
      <div className={headerClasses.flexibleSpace} />
      <Button
        className={classes.deleteLink}
        presets={['text', 'default']}
        action={() => {
          if (!formik) return;
          formik.setValues({
            assignee: [],
            case_number: [],
            keyword: [],
            department: [],
            case_type: [],
          });
          setFilters([]);
        }}
      >
        {t('removeFilters')}
      </Button>
    </>
  );

  return (
    <>
      <Select
        isMulti
        isClearable={false}
        isSearchable={false}
        openMenuOnClick={false}
        placeholder=""
        value={filters}
        styles={{
          ...multiValueLabelStyles({
            theme,
          }),
          ...widgetHeaderMultiStyles({ theme }),
        }}
        onChange={({ target: { value } }: any) => {
          setFilters(value);
        }}
        multiValueLabelIcon={({ option }: { option?: FilterType }) => {
          if (!option?.data?.type) return null;
          return getIcon(option.data.type);
        }}
      />
      <Button
        className={classes.open}
        presets={['text', 'small']}
        action={() => setFilterForm(true)}
        icon="add"
      >
        {t('filter')}
      </Button>
      {filterForm && (
        <div className={classes.overlay}>
          <WidgetHeader title={title}>{filterHeaderOptions}</WidgetHeader>

          <FilterForm
            filters={filters}
            onSetFilters={setFilters}
            setFilterForm={setFilterForm}
            onMount={(formik: FormikProps<any>) => setFormik(formik)}
          />
        </div>
      )}
    </>
  );
};

export default Filter;

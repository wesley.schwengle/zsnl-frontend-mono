// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import fecha from 'fecha';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ColumnType } from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { ItemValueType } from '@zaaksysteem/common/src/components/form/fields/Options/Options.types';
import { GetDataReturnType } from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import {
  CasesRowType,
  ClassesType,
  TasksRowType,
  FilterType,
  Types,
  GetDataParamsType,
} from './types/Tasks.types';

const sortMapping: any = {
  display_number: 'relationships.case.number',
  display_name: 'relationships.assignee.name',
  title: 'attributes.title',
  due_date: 'attributes.due_date',
  days_left: 'attributes.due_date',
};

/* eslint complexity: [2, 13] */
export const getData = async ({
  pageNum,
  pageLength,
  filters,
  searchTerm,
  sortBy,
  sortDirection,
  openServerErrorDialog,
}: { pageNum: number } & GetDataParamsType): Promise<
  GetDataReturnType<TasksRowType>
> => {
  const queryParams: any = {
    page: pageNum,
    page_size: pageLength,
    filter: {
      'attributes.completed': false,
    },
  };

  if (filters && filters.length) {
    const filtersObj = filters.reduce((acc: any, current: FilterType) => {
      if (!current.data?.type) return acc;
      const filterType = current.data.type;
      if (!acc[filterType]) acc[filterType] = [];
      acc[filterType].push(current.value);
      return acc;
    }, {});

    if (filtersObj.assignee)
      queryParams['filter']['relationships.assignee.id'] = filtersObj.assignee;
    if (filtersObj.case_number)
      queryParams['filter']['relationships.case.number'] =
        filtersObj.case_number.join(',');
    if (filtersObj.case_type)
      queryParams['filter']['relationships.case_type.id'] =
        filtersObj.case_type.join(',');
    if (filtersObj.department)
      queryParams['filter']['relationships.department.id'] =
        filtersObj.department;
    if (filtersObj.keyword)
      queryParams['filter']['attributes.title.contains'] = filtersObj.keyword;
  }

  if (searchTerm) queryParams['keyword'] = searchTerm;

  if (sortBy && sortDirection) {
    let sorting = sortMapping[sortBy];
    if (sortDirection === 'DESC') sorting = '-' + sorting;
    queryParams['sort'] = sorting;
  }

  const url = buildUrl<APICaseManagement.GetTaskListRequestParams>(
    '/api/v2/cm/task/get_task_list',
    queryParams
  );

  const results = await request<APICaseManagement.GetTaskListResponseBody>(
    'GET',
    url
  ).catch(openServerErrorDialog);

  if (!results?.data) return { rows: [] };

  const newData = results.data.map(row => {
    return {
      uuid: row.id,
      name: `tasktable`,
      display_name: row.relationships?.assignee?.data.meta.display_name,
      assignee_id: row.relationships?.assignee?.data.id,
      display_number: row.relationships?.case.meta?.display_number,
      case_id: row.relationships?.case.data.id,
      title: row.attributes.title,
      due_date: row.attributes.due_date,
      case_type: row.relationships?.case_type?.data.id,
      department: row.relationships?.department?.data.id,
      days_left: getDays(row.attributes.due_date),
    };
  });

  return { rows: newData };
};

const getDays = (end_date?: string) => {
  if (!end_date) return;

  const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
  const today = new Date().setHours(0, 0, 0, 0);
  const [year, month, day] = end_date.split('-').map(part => Number(part));
  const monthIndex = month - 1;
  const deadline = new Date(year, monthIndex, day, 0, 0, 0, 0).getTime();

  const isLate = today > deadline;
  const daysLeft = Math.round(Math.abs((today - deadline) / oneDay));

  return isLate ? -Math.abs(daysLeft) : daysLeft;
};

const getDaysCell = (classes: ClassesType, daysLeft: number) => (
  <div
    className={`${classes.daysLeft} ${
      Number(daysLeft) < 0 ? classes.hasPassed : ''
    }`}
  >
    {daysLeft}
  </div>
);

export const getAllColumns = ({
  classes,
  t,
}: {
  classes?: ClassesType;
  t: i18next.TFunction;
}): ColumnType[] => {
  return [
    {
      name: 'display_number',
      width: 140,
      label: t('tasks:columns.display_number.label'),
    },
    {
      name: 'title',
      width: 200,
      label: t('tasks:columns.title.label'),
      flexGrow: 1,
    },
    {
      name: 'display_name',
      width: 200,
      label: 'Behandelaar',
    },
    {
      name: 'due_date',
      width: 200,
      label: t('tasks:columns.due_date.label'),
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CasesRowType }) => (
        <div>
          {rowData.due_date &&
            fecha.format(
              new Date(rowData.due_date as string),
              t('common:dates.dateFormatTextShort')
            )}
        </div>
      ),
    },
    {
      name: 'days_left',
      width: 100,
      label: t('tasks:columns.days_left.label'),
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: CasesRowType }) =>
        classes && (rowData.days_left || rowData.days_left === 0)
          ? getDaysCell(classes, rowData.days_left)
          : '',
    },
  ];
};

export const getWidgetData = async (
  widgetUuid: string
): Promise<{
  filters: FilterType[];
  columns: ItemValueType[];
}> => {
  const url = buildUrl<any>('/api/v1/dashboard/widget', {
    page: 1,
    rows_per_page: 20,
  });

  const data = await request('GET', url);

  const widgetFilters = data.result.instance.rows.find(
    (option: any) => option.reference === widgetUuid
  );

  return {
    filters: widgetFilters?.instance?.data?.filters || [],
    columns: widgetFilters?.instance?.data?.columns || [],
  };
};

export const getIcon = (type: Types) => {
  const map: { [index: string]: React.ReactElement } = {
    assignee: <Icon size="extraSmall">{iconNames.person}</Icon>,
    keyword: <Icon size="extraSmall">{iconNames.spellcheck}</Icon>,
    case_number: <Icon size="extraSmall">{iconNames.hash}</Icon>,
    case_type: <Icon size="extraSmall">{iconNames.label}</Icon>,
    department: <Icon size="extraSmall">{iconNames.people}</Icon>,
  };
  return map[type];
};

export const defaultTasksWidgetData = {
  columns: [
    {
      id: 'display_number',
      value: 'Zaaknummer',
      active: true,
      isNew: false,
      is_new: false,
    },
    {
      id: 'title',
      value: 'Taak',
      active: true,
      isNew: false,
      is_new: false,
    },
    {
      id: 'display_name',
      value: 'Behandelaar',
      active: true,
      isNew: false,
      is_new: false,
    },
    {
      id: 'due_date',
      value: 'Deadline',
      active: true,
      isNew: false,
      is_new: false,
    },
    {
      id: 'days_left',
      value: 'Dagen',
      active: true,
      isNew: false,
      is_new: false,
    },
  ],
  filters: [],
};

export const getFilteredColumns = ({
  allColumns,
  columns,
}: {
  allColumns: ColumnType[];
  columns: ItemValueType[];
}) =>
  columns
    .map(filter =>
      filter.active ? allColumns.find(col => col.name === filter.id) : null
    )
    .filter(Boolean);

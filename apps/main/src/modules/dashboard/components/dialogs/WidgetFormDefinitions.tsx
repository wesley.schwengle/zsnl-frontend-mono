// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  hideFields,
  Rule,
  showFields,
  valueOneOf,
} from '@zaaksysteem/common/src/components/form/rules';
import { KindType } from '../../../advancedSearch/AdvancedSearch.types';

const createGetChoices = (kind: KindType) => async () => {
  const url = buildUrl('/api/v2/cm/saved_search/list', {
    page_size: 50,
    page: 1,
    'filter[kind]': kind,
  });

  const results = await request<APICaseManagement.ListSavedSearchResponseBody>(
    'GET',
    url
  );

  return results.data.map(savedSearch => ({
    value: savedSearch.id,
    label: savedSearch.attributes.name,
  }));
};

export const externalUrlFormDefinition = [
  {
    name: 'title',
    type: 'text',
    value: 'Externe URL',
    required: true,
    creatable: true,
    isMulti: false,
    clearable: true,
    triggerChangeWhenBlurred: true,
    label: 'dashboard:widget.fields.title.label',
    placeholder: 'dashboard:widget.fields.title.label',
  },
  {
    name: 'url',
    type: 'text',
    value: 'https://xxllnc.nl/',
    required: true,
    creatable: true,
    isMulti: false,
    clearable: true,
    triggerChangeWhenBlurred: true,
    label: 'dashboard:widget.fields.url.label',
    placeholder: 'dashboard:widget.fields.url.label',
  },
];

const advancedSearchChoices = [
  { value: 'case', label: 'dashboard:widget.fields.searchTypeCase' },
  { value: 'custom_object', label: 'dashboard:widget.fields.searchTypeObject' },
];

export const advancedSearchFormRules = [
  new Rule()
    .when('chooseSearchType', valueOneOf(['case']))
    .then(showFields(['savedSearchSelectCase']))
    .else(hideFields(['savedSearchSelectCase'])),
  new Rule()
    .when('chooseSearchType', valueOneOf(['custom_object']))
    .then(showFields(['savedSearchSelectObject']))
    .else(hideFields(['savedSearchSelectObject'])),
];

export const advancedSearchFormDefinition = [
  {
    name: 'chooseSearchType',
    type: 'select',
    value: advancedSearchChoices[0],
    required: true,
    isMulti: false,
    clearable: false,
    triggerChangeWhenBlurred: true,
    label: 'dashboard:widget.fields.chooseSearchType.label',
    placeholder: 'dashboard:widget.fields.chooseSearchType.label',
    generic: true,
    choices: advancedSearchChoices,
  },
  {
    name: 'savedSearchSelectCase',
    type: 'select',
    value: null,
    required: true,
    isMulti: false,
    clearable: true,
    triggerChangeWhenBlurred: true,
    label: 'dashboard:widget.fields.savedSearchSelect.label',
    placeholder: 'dashboard:widget.fields.savedSearchSelect.label',
    generic: true,
    autoLoad: true,
    getChoices: createGetChoices('case'),
  },
  {
    name: 'savedSearchSelectObject',
    type: 'select',
    value: null,
    required: true,
    isMulti: false,
    clearable: true,
    triggerChangeWhenBlurred: true,
    label: 'dashboard:widget.fields.savedSearchSelect.label',
    placeholder: 'dashboard:widget.fields.savedSearchSelect.label',
    generic: true,
    autoLoad: true,
    getChoices: createGetChoices('custom_object'),
    hidden: true,
  },
];

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { hot } from 'react-hot-loader/root';
import { Provider } from 'react-redux';
import DialogRenderer from '@zaaksysteem/common/src/components/DialogRenderer/DialogRenderer';
import SnackbarRenderer from '@zaaksysteem/common/src/components/SnackbarRenderer/SnackbarRenderer';
import useDialogs from '@zaaksysteem/common/src/library/useDialogs';
//@ts-ignore
import MaterialUiThemeProvider from '@mintlab/ui/App/Material/MaterialUiThemeProvider/MaterialUiThemeProvider';
import { configureStore } from './configureStore';
import Routes from './Routes';
import { DIALOG_ERROR } from './constants/dialog.constants';
import ErrorDialog from './components/ErrorDialog/ErrorDialog';

const store = configureStore();

function App() {
  const [, addDialogs] = useDialogs();

  useEffect(() => {
    addDialogs({
      [DIALOG_ERROR]: ErrorDialog,
    });
  }, []);

  return (
    <Provider store={store}>
      <MaterialUiThemeProvider>
        <Routes prefix={process.env.APP_CONTEXT_ROOT || '/my-pip'} />
        <DialogRenderer />
        <SnackbarRenderer />
      </MaterialUiThemeProvider>
    </Provider>
  );
}

export default process.env.NODE_ENV === 'development' ? hot(App) : App;

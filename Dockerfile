FROM node:latest

ARG BUILD_TIMESTAMP=undefined

WORKDIR /usr/mono

COPY build/apps/ /usr/mono/build/apps/
COPY apps/objection-app /usr/mono/build/apps/objection-app
COPY apps/external-components/src/arcgis.html /usr/mono/build/apps/arcgis/index.html
COPY apps/external-components/src/arcgis.js /usr/mono/build/apps/arcgis/arcgis.js 
COPY apps/external-components/src/arcgis.css /usr/mono/build/apps/arcgis/arcgis.css 
COPY node/ /usr/mono/node
COPY node/package.json /usr/mono/package.json

RUN npm install

ENV PORT=80
ENV NODE_ENV=production
CMD ["node", "node/server.js"]

RUN echo "$BUILD_TIMESTAMP" > /build-timestamp

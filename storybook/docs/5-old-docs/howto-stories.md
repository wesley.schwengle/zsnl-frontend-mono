# How to write stories

> While you _can_ write stories with the
> [Storybook API](https://storybook.js.org/basics/writing-stories/)
> any way you like, there are a couple of convenience
> methods in the `./App/story` module (when you create a new component
> with `npm run plop`, a boilerplate story is automatically generated).

## The `stories` function

    import {
      React,
      stories,
    } from '../../story';

Note that since
[`React` must be in scope](https://reactjs.org/docs/jsx-in-depth.html#react-must-be-in-scope)
when you use JSX anyway, the `story` module exports it for brevity.

`stories` takes at least three arguments:

Two of the [`webpack` polyfills for `node` globals](https://github.com/webpack/docs/wiki/configuration#node)

- `module`
- `__dirname`

that enable constructing the stories tree view in the sidebar from the file system,
and a map of methods that have the name of the story and return the story's JSX.

    stories(module, __dirname, {
      MyStory() {
        return (
          <div/>
        );
      },
      'My weird story #name :-)': function MyStoryIdentifier() {
        return (
          <div/>
        );
      }
    });

There is an optional fourth parameter that is a map of
story names to story states and automagically sets up
[@dump247/storybook-state](https://www.npmjs.com/package/@dump247/storybook-state)
for matching stories.

    const storage = {
      MyStoryWithState: {
        stateful: true,
      },
    }

    stories(module, __dirname, {
      MyStoryWithState({ store }) {
        return (
          <div/>
        );
      },
      MyStoryWithoutState() {
        return (
          <div/>
        );
      },
    }, storage);

# Coding style

## 🎵 Because you gotta have RULES!

> Rules are meant to be broken, but please consult your co-workers before doing so.

## Formatting

> Formatting is useless mental overhead and is best left for computers

All code formatting is handled by `Prettier`. You can find the settings in the `.prettierrc` file in the project root.

For more informatie about `Prettier` please visit the [docs](https://prettier.io/docs/en/)

## Exporting your component

- Use **both** a _named_ and a `default` export
  for every component.
  - Always import the named export in tests.
  - If you wrap the component in a `HoC`,
    only do that in the `default` export.

## Functional component

We always prefer functional components. Using an arrow function
is usually concise and readable.

```javascript
import React from 'react';

export const MyComponent: React.ComponentType<{}> = ({ amazing, children }) => (
  <div>
    My {children} are {amazing}.
  </div>
);

export default MyComponent;
```

Sometimes you need to do something based on the passed props
before you `return`. Using a function declaration is also
fine in that case.

```javascript
import React from 'react';

export const MyComponent: React.ComponentType<{}> = ({ children }) => {
  // do stuff based on the passed props

  return <div>{children}</div>;
};

export default MyComponent;
```

## Hooks

If you **do** need lifecycle methods, e.g. for DOM access, use `hooks`.

```javascript
import React, { useRef, useEffect, useCallback } from 'react';

export const MyComponent: React.ComponentType<{}> = ({ children }) => {
  const domNodeRef = useRef();
  const handleEvent = useCallback((nativeDomEvent: React.SyntaticEvent) => {
    // do stuff when the `exoticEventType` event fires
  });

  useEffect(() => {
    // the component's DOM is ready
    domNodeRef.current.addEventListener('exoticEventType', handleEvent);

    return () => {
      // always clean up behind manual operations
      domNodeRef.current.removeEventListener('exoticEventType', handleEvent);
    };
  }, [domNodeRef, handleEvent]); // Always declare dependencies

  return <div ref={domNodeRef}>{children}</div>;
};
```

## Class components

Don't use class components unless you have a very good explaination why this could not be achieved with hooks.

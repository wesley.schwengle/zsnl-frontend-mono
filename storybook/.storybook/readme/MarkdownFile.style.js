import { makeStyles } from '@mui/styles';

const font = {
  fontFamily:
    '"Helvetica Neue", Helvetica, "Segoe UI", Arial, freesans, sans-serif',
  fontSize: 16,
  lineHeight: 1.5,
  fontWeight: 200,
};

const codeBlock = {
  fontSize: 15,
  padding: 2,
  backgroundColor: 'rgb(248, 248, 248)',
  color: 'rgba(214, 86, 23, 1)',
  border: '1px solid rgb(242, 242, 242)',
};

const inlineCode = {
  '& > code': codeBlock,
};

export const useStyles = makeStyles(() => ({
  h1: {
    ...font,
    paddingBottom: 30,
    marginBottom: 50,
    borderBottom: `1px solid #eee`,
    fontSize: 35,
    fontWeight: 'bold',

    '& + h2': {
      ...font,
      fontSize: 20,
      fontWeight: 200,
      marginTop: -88,
      marginBottom: 58,
    },
  },
  h2: {
    ...font,
    fontSize: 22,
    fontWeight: 400,
    marginTop: 50,
  },
  h3: {
    ...font,
    fontSize: 20,
    fontWeight: 300,
    marginTop: 50,
  },
  h4: {
    ...font,
    fontSize: 18,
    fontWeight: 300,
    marginTop: 50,
  },
  p: {
    ...font,
    letterSpacing: 0.8,
    ...inlineCode,
  },
  list: {
    ...font,
    ...inlineCode,
  },
  listItem: {
    ...inlineCode,
  },
  code: {
    fontSize: 15,
    lineHeight: 1.6,

    '& > pre': codeBlock,
  },
  quoteWrapper: {
    display: 'flex',
    flexDirection: 'row',
  },
  quote: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: 'rgb(242, 242, 242)',
    fontStyle: 'italic',
    alignItems: 'center',

    '& > p': {
      padding: 10,
    },
  },
  quoteSign: {
    fontSize: 60,
    width: 40,
    fontFamily: 'cursive',
    maxHeight: 70,
    overflow: 'hidden',
    color: '#ccc',
  },
}));

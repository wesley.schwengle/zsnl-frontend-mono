import { initReactI18next } from 'react-i18next';
import { addParameters, addDecorator, configure } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { withA11y } from '@storybook/addon-a11y';
import { DocsPage, DocsContainer } from '@storybook/addon-docs/blocks';
import { themes } from '@storybook/theming';
import { initI18n } from '@zaaksysteem/common/src/i18n';
import { addReadmeStory } from './readme/addReadmeStory';
import './style.css';

addDecorator(withKnobs);
addDecorator(withA11y);

addParameters({
  options: {
    brandTitle: 'Zaaksysteem.nl',
    theme: themes.light,
    showRoots: true,
  },
  docs: {
    container: DocsContainer,
    page: DocsPage,
  },
});

const reqPackage = require.context(
  '../../packages',
  true,
  /\.story\.(js|tsx)$/
);
const reqApps = require.context('../../apps', true, /\.story\.(js|tsx)$/);
const reqReadmeFiles = require.context('../docs', true, /.md$/);

function loadStories() {
  reqReadmeFiles
    .keys()
    .forEach(file => addReadmeStory(file, reqReadmeFiles(file)));
  reqPackage.keys().forEach(filename => reqPackage(filename));
  reqApps.keys().forEach(filename => reqApps(filename));
}

async function start() {
  await initI18n(
    {
      lng: 'nl',
      keySeparator: '.',
      defaultNS: 'common',
      fallbackNS: 'common',
      interpolation: {
        escapeValue: false,
      },
    },
    [initReactI18next]
  );
  configure(loadStories, module);
}

start();

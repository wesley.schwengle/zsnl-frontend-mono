// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

type BreakpointKeyType = 'xs' | 'sm' | 'md' | 'lg' | 'xl';

export type Theme = {
  breakpoints: {
    keys: ['xs', 'sm', 'md', 'lg', 'xl'];
    values: { xs: 0; sm: 600; md: 960; lg: 1280; xl: 1920 };
    up: (breakpointKey: BreakpointKeyType) => string;
    down: (breakpointKey: BreakpointKeyType) => string;
  };
  direction: 'ltr';
  mixins: {
    toolbar: {
      minHeight: 56;
      '@media (min-width:0px) and (orientation: landscape)': { minHeight: 48 };
      '@media (min-width:600px)': { minHeight: 64 };
    };
  };
  overrides: {
    MuiButton: {
      root: { borderRadius: 8; textTransform: 'none' };
      label: {
        paddingTop: '0.1rem';
        '& > span:first-child > svg': { marginRight: '0.4rem' };
      };
      containedSecondary: { color: '#fff' };
    };
    MuiDialog: { paper: { borderRadius: 10 } };
    MuiDialogActions: { root: { '& > button': { marginRight: '12px' } } };
    MuiPaper: { root: { padding: '8px' } };
    MuiDivider: { root: { margin: '6px 0px' } };
  };
  palette: {
    common: { black: '#2A2E33'; white: '#FFFFFF' };
    type: 'light';
    primary: {
      darkest: '#161559';
      darker: '#134991';
      dark: '#1860C3';
      main: '#1B73E7';
      light: '#97C1EF';
      lighter: '#E1EDFF';
      lightest: '#EFF5FF';
      contrastText: '#fff';
    };
    secondary: {
      darkest: '#14441F';
      darker: '#1D652E';
      dark: '#26883E';
      main: '#30AA4D';
      light: '#83CC94';
      lighter: '#ACDCB8';
      lightest: '#D6EEDB';
      contrastText: '#fff';
    };
    error: {
      dark: '#DD2D4F';
      main: '#FF345B';
      light: '#FBE9E7';
      contrastText: '#fff';
    };
    grey: {
      '50': '#fafafa';
      '100': '#f5f5f5';
      '200': '#eeeeee';
      '300': '#e0e0e0';
      '400': '#bdbdbd';
      '500': '#9e9e9e';
      '600': '#757575';
      '700': '#616161';
      '800': '#424242';
      '900': '#212121';
      A100: '#d5d5d5';
      A200: '#aaaaaa';
      A400: '#303030';
      A700: '#616161';
    };
    contrastThreshold: 3;
    tonalOffset: 0.2;
    text: {
      primary: 'rgba(0, 0, 0, 0.87)';
      secondary: 'rgba(0, 0, 0, 0.54)';
      disabled: 'rgba(0, 0, 0, 0.38)';
      hint: 'rgba(0, 0, 0, 0.38)';
    };
    divider: 'rgba(0, 0, 0, 0.12)';
    background: { paper: '#fff'; default: '#fafafa' };
    action: {
      active: 'rgba(0, 0, 0, 0.54)';
      hover: 'rgba(0, 0, 0, 0.08)';
      hoverOpacity: 0.08;
      selected: 'rgba(0, 0, 0, 0.14)';
      disabled: 'rgba(0, 0, 0, 0.26)';
      disabledBackground: 'rgba(0, 0, 0, 0.12)';
    };
    elephant: {
      darkest: '#1C1C1C';
      darker: '#383838';
      dark: '#606060';
      main: '#9B9B9B';
      light: '#C6C6C6';
      lighter: '#DDDDDD';
      lightest: '#E8E8E8';
    };
    thundercloud: {
      darkest: '#0F0F29';
      darker: '#4E5763';
      dark: '#606979';
      main: '#8E95A0';
      light: '#ACB4BE';
      lighter: '#C5CAD1';
      lightest: '#DFDFE5';
    };
    cloud: {
      darkest: '#E4E4EA';
      darker: '#E8E8ED';
      dark: '#EFEFEF';
      main: '#F1F3F4';
      light: '#F5F7F7';
      lighter: '#F7F8F9';
      lightest: '#FFFFFF';
    };
    northsea: {
      darkest: '#0F0F29';
      darker: '#5A6477';
      dark: '#67718A';
      main: '#6A768B';
      light: '#8E95A0';
      lighter: '#C4C7CD';
      lightest: '#E0E2E5';
    };
    basalt: {
      darkest: '#1B1C1E';
      darker: '#252C31';
      dark: '#3B424B';
      main: '#4E5B68';
      light: '#637082';
      lighter: '#707D8E';
      lightest: '#8C949F';
    };
    coral: {
      darkest: '#661816';
      darker: '#992522';
      dark: '#CC312D';
      main: '#FF3D38';
      light: '#FF8B88';
      lighter: '#FFB1AF';
      lightest: '#FFD8D7';
    };
    pansy: {
      darkest: '#29174A';
      darker: '#3E226E';
      dark: '#522D94';
      main: '#6739B9';
      light: '#A488D5';
      lighter: '#C2B0E3';
      lightest: '#E1D7F1';
    };
    bluebird: {
      darkest: '#003662';
      darker: '#005292';
      dark: '#006DC3';
      main: '#0088F4';
      light: '#66B8F8';
      lighter: '#99CEFB';
      lightest: '#CCE7FD';
    };
    jade: {
      darkest: '#003B37';
      darker: '#005B52';
      dark: '#00796D';
      main: '#009789';
      light: '#66C1B8';
      lighter: '#99D5D0';
      lightest: '#CBEAE7';
    };
    lizard: {
      darkest: '#1B4F3A';
      darker: '#287756';
      dark: '#369E73';
      main: '#43C690';
      light: '#8EDDBC';
      lighter: '#B4E8D3';
      lightest: '#D9F4E9';
    };
    grass: {
      darkest: '#1D461F';
      darker: '#2D692E';
      dark: '#3A8D3D';
      main: '#48B04C';
      light: '#92D094';
      lighter: '#B6DFB7';
      lightest: '#DBEFDB';
    };
    sahara: {
      darkest: '#654B00';
      darker: '#977001';
      dark: '#CA9600';
      main: '#FCBB00';
      light: '#FDD666';
      lighter: '#FDE499';
      lightest: '#FEF1CC';
    };
    review: {
      dark: '#D18100';
      main: '#FF9D00';
      light: '#FFF5D9';
      support: '#FFE3DB';
    };
    danger: {
      dark: '#DD2D4F';
      main: '#FF345B';
      light: '#FBE9E7';
      support: '#FFE5E2';
    };
    support: { main: '#004EE3' };
    vivid: {
      '1': '#0058FF';
      '2': '#F4B949';
      '3': '#0AD7E8';
      '4': '#088DFF';
      '5': '#6066FF';
      '6': '#872FFF';
    };
  };
  props: {};
  shadows: [
    'none',
    '0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 3px 0px rgba(0,0,0,0.12)',
    '0px 3px 1px -2px rgba(0,0,0,0.2),0px 2px 2px 0px rgba(0,0,0,0.14),0px 1px 5px 0px rgba(0,0,0,0.12)',
    '0px 3px 3px -2px rgba(0,0,0,0.2),0px 3px 4px 0px rgba(0,0,0,0.14),0px 1px 8px 0px rgba(0,0,0,0.12)',
    '0px 2px 4px -1px rgba(0,0,0,0.2),0px 4px 5px 0px rgba(0,0,0,0.14),0px 1px 10px 0px rgba(0,0,0,0.12)',
    '0px 3px 5px -1px rgba(0,0,0,0.2),0px 5px 8px 0px rgba(0,0,0,0.14),0px 1px 14px 0px rgba(0,0,0,0.12)',
    '0px 3px 5px -1px rgba(0,0,0,0.2),0px 6px 10px 0px rgba(0,0,0,0.14),0px 1px 18px 0px rgba(0,0,0,0.12)',
    '0px 4px 5px -2px rgba(0,0,0,0.2),0px 7px 10px 1px rgba(0,0,0,0.14),0px 2px 16px 1px rgba(0,0,0,0.12)',
    '0px 5px 5px -3px rgba(0,0,0,0.2),0px 8px 10px 1px rgba(0,0,0,0.14),0px 3px 14px 2px rgba(0,0,0,0.12)',
    '0px 5px 6px -3px rgba(0,0,0,0.2),0px 9px 12px 1px rgba(0,0,0,0.14),0px 3px 16px 2px rgba(0,0,0,0.12)',
    '0px 6px 6px -3px rgba(0,0,0,0.2),0px 10px 14px 1px rgba(0,0,0,0.14),0px 4px 18px 3px rgba(0,0,0,0.12)',
    '0px 6px 7px -4px rgba(0,0,0,0.2),0px 11px 15px 1px rgba(0,0,0,0.14),0px 4px 20px 3px rgba(0,0,0,0.12)',
    '0px 7px 8px -4px rgba(0,0,0,0.2),0px 12px 17px 2px rgba(0,0,0,0.14),0px 5px 22px 4px rgba(0,0,0,0.12)',
    '0px 7px 8px -4px rgba(0,0,0,0.2),0px 13px 19px 2px rgba(0,0,0,0.14),0px 5px 24px 4px rgba(0,0,0,0.12)',
    '0px 7px 9px -4px rgba(0,0,0,0.2),0px 14px 21px 2px rgba(0,0,0,0.14),0px 5px 26px 4px rgba(0,0,0,0.12)',
    '0px 8px 9px -5px rgba(0,0,0,0.2),0px 15px 22px 2px rgba(0,0,0,0.14),0px 6px 28px 5px rgba(0,0,0,0.12)',
    '0px 8px 10px -5px rgba(0,0,0,0.2),0px 16px 24px 2px rgba(0,0,0,0.14),0px 6px 30px 5px rgba(0,0,0,0.12)',
    '0px 8px 11px -5px rgba(0,0,0,0.2),0px 17px 26px 2px rgba(0,0,0,0.14),0px 6px 32px 5px rgba(0,0,0,0.12)',
    '0px 9px 11px -5px rgba(0,0,0,0.2),0px 18px 28px 2px rgba(0,0,0,0.14),0px 7px 34px 6px rgba(0,0,0,0.12)',
    '0px 9px 12px -6px rgba(0,0,0,0.2),0px 19px 29px 2px rgba(0,0,0,0.14),0px 7px 36px 6px rgba(0,0,0,0.12)',
    '0px 10px 13px -6px rgba(0,0,0,0.2),0px 20px 31px 3px rgba(0,0,0,0.14),0px 8px 38px 7px rgba(0,0,0,0.12)',
    '0px 10px 13px -6px rgba(0,0,0,0.2),0px 21px 33px 3px rgba(0,0,0,0.14),0px 8px 40px 7px rgba(0,0,0,0.12)',
    '0px 10px 14px -6px rgba(0,0,0,0.2),0px 22px 35px 3px rgba(0,0,0,0.14),0px 8px 42px 7px rgba(0,0,0,0.12)',
    '0px 11px 14px -7px rgba(0,0,0,0.2),0px 23px 36px 3px rgba(0,0,0,0.14),0px 9px 44px 8px rgba(0,0,0,0.12)',
    '0px 11px 15px -7px rgba(0,0,0,0.2),0px 24px 38px 3px rgba(0,0,0,0.14),0px 9px 46px 8px rgba(0,0,0,0.12)'
  ];
  typography: {
    htmlFontSize: 16;
    fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
    fontSize: 14;
    fontWeightLight: 300;
    fontWeightRegular: 400;
    fontWeightMedium: 500;
    fontWeightBold: 700;
    h1: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '2.187rem';
      lineHeight: 1;
      letterSpacing: '-0.01562em';
      color: '#2A2E33';
    };
    h2: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '1.875rem';
      lineHeight: 1;
      letterSpacing: '-0.00833em';
      color: '#2A2E33';
    };
    h3: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '1.5625rem';
      lineHeight: 1.04;
      letterSpacing: '0em';
      color: '#2A2E33';
    };
    h4: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '1.375rem';
      lineHeight: 1.17;
      letterSpacing: '0.00735em';
      color: '#2A2E33';
    };
    h5: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '1.25rem';
      lineHeight: 1.33;
      letterSpacing: '0em';
      color: '#2A2E33';
    };
    h6: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '1.125rem';
      lineHeight: 1.6;
      letterSpacing: '0.0075em';
      color: '#2A2E33';
    };
    subtitle1: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '1rem';
      lineHeight: 1.75;
      letterSpacing: '0.00938em';
    };
    subtitle2: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 500;
      fontSize: '0.875rem';
      lineHeight: 1.57;
      letterSpacing: '0.00714em';
    };
    body1: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '1rem';
      lineHeight: 1.5;
      letterSpacing: '0.00938em';
    };
    body2: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '0.875rem';
      lineHeight: 1.43;
      letterSpacing: '0.01071em';
    };
    button: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 500;
      fontSize: '0.875rem';
      lineHeight: 1.75;
      letterSpacing: '0.02857em';
      textTransform: 'uppercase';
    };
    caption: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '0.75rem';
      lineHeight: 1.66;
      letterSpacing: '0.03333em';
    };
    overline: {
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif';
      fontWeight: 400;
      fontSize: '0.75rem';
      lineHeight: 2.66;
      letterSpacing: '0.08333em';
      textTransform: 'uppercase';
    };
    useNextVariants: true;
  };
  shape: { borderRadius: 4 };
  transitions: {
    easing: {
      easeInOut: 'cubic-bezier(0.4, 0, 0.2, 1)';
      easeOut: 'cubic-bezier(0.0, 0, 0.2, 1)';
      easeIn: 'cubic-bezier(0.4, 0, 1, 1)';
      sharp: 'cubic-bezier(0.4, 0, 0.6, 1)';
    };
    duration: {
      shortest: 150;
      shorter: 200;
      short: 250;
      standard: 300;
      complex: 375;
      enteringScreen: 225;
      leavingScreen: 195;
    };
    create: (property: string, options: { [key: string]: any }) => string;
  };
  zIndex: {
    mobileStepper: 1000;
    speedDial: 1050;
    appBar: 1100;
    drawer: 1200;
    modal: 1300;
    snackbar: 1400;
    tooltip: 1500;
    inputLabel: 100;
    statusBar: 1250;
    options: 1260;
  };
  mintlab: {
    greyscale: {
      black: '#2A2E33';
      offBlack: '#2F3338';
      darkest: '#4E5764';
      evenDarker: '#AEB3B9';
      darker: '#D7D7DB';
      dark: '#F2F2F2';
      main: '#878787';
      light: '#F7F7F8';
      lighter: '#FFFFFF';
    };
    iconSizes: ['tiny', 'extraSmall', 'small', 'medium', 'large', 'huge'];
    icon: {
      tiny: 14;
      extraSmall: 18;
      small: 23;
      medium: 28;
      large: 35;
      huge: 44;
    };
    radius: {
      button: 8;
      buttonSmall: 5;
      card: 2;
      horizontalMenuButton: 30;
      notificationButton: 30;
      verticalMenuButton: 30;
      bannerButton: 30;
      dialog: 10;
      genericInput: 30;
      sheet: 8;
      snackbar: 30;
      dropdownMenu: 15;
      dropdownButton: 8;
      chip: 12;
      chipRemove: '50%';
      wysiwyg: 8;
      textField: 8;
      tooltip: 8;
      tableRow: 8;
      select: 8;
      defaultFormElement: 8;
    };
    shadows: {
      insetTop: 'inset 0.1px 1px 3px 0.1px rgba(0,0,0,0.10)';
      flat: '0.1px 1px 3px 0.1px rgba(0,0,0,0.12)';
      medium: '3px 3px 10px 2px rgba(0,0,0,0.05)';
      sharp: '0 1px 2px rgba(0,0,0,0.3)';
    };
  };
};

// export default Theme;

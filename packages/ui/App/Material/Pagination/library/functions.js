// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Icon from '../../Icon';
import { addScopeProp } from '../../../library/addScope';

const FIRST_PAGE = 0;

/**
 * The number of pages around the selected page.
 * ZS-TODO: configuration
 * This would better come from a parameter with a default
 * value higher up the business logic hierarchy.
 *
 * @type {number}
 */
const ADJACENT_PAGES = 2;

/**
 * @type {number}
 */
const INTEGER_OFFSET = 1;

/**
 * @param {number} value
 * @param {number} [offset=1]
 * @return {number}
 */
export const increment = (value, offset = INTEGER_OFFSET) => value + offset;

/**
 * @param {number} value
 * @param {number} [offset=1]
 * @return {number}
 */
export const decrement = (value, offset = INTEGER_OFFSET) => value - offset;

/**
 * Get the button configuration for a section.
 *
 * @param {String} name
 * @param {Number} page
 * @param {Number} pageCount
 * @param {Object} styles
 * @param {string} [scope]
 * @return {Array}
 */
export const getButtonConfig = (name, page, pageCount, styles, scope) => {
  // ZS-TODO: configuration
  // This would better be a parameter with a default
  // value higher up the business logic hierarchy.
  // Beware: check if there is a hard coupling with an actual icon
  const seekOffset = 10;

  const buttons = {
    stepBackButtons: [
      {
        destination: decrement(page),
        icon: <Icon>navigate_before</Icon>,
        rootClass: 'smallStep',
        ...addScopeProp(scope, 'before'),
      },
      {
        destination: decrement(page, seekOffset),
        icon: `-${seekOffset}`,
        rootClass: 'bigStep',
        ...addScopeProp(scope, 'before-big'),
      },
    ],
    pageJumpButtons: getPageButtons(page, pageCount, scope),
    stepForwardButtons: [
      {
        destination: increment(page, seekOffset),
        icon: `+${seekOffset}`,
        rootClass: 'bigStep',
        ...addScopeProp(scope, 'next'),
      },
      {
        destination: increment(page),
        icon: <Icon>navigate_next</Icon>,
        rootClass: 'smallStep',
        ...addScopeProp(scope, 'next-big'),
      },
    ],
  };

  let buttonConfig = buttons[name];

  if (styles) {
    buttonConfig.forEach(button => {
      button.buttonStyles = {
        ...styles.paginatorButton,
        '& .Mui-diabled': {
          ...(button.destination === page
            ? styles.currentPage
            : styles.disabled),
        },
      };

      return button;
    });
  }

  return buttonConfig;
};

/**
 * Create button configurations for the page jump buttons.
 *
 * @param {number} page
 * @param {number} pageCount
 * @param {string} [scope]
 * @return {Array}
 */
export const getPageButtons = (page, pageCount, scope) =>
  symmetricallyIncrementByOneFromZeroUpTo(ADJACENT_PAGES)
    .filter(index => pageExists(page + index, pageCount))
    .map(index => ({
      destination: page + index,
      icon: increment(page + index),
      rootClass: 'pageJump',
      ...addScopeProp(scope, `page-${page + index}`),
    }));

/**
 * Check whether the current page number
 * exists between zero and `pageCount`.
 *
 * @param {number} destination
 * @param {number} pageCount
 * @return {boolean}
 */
export const pageExists = (destination, pageCount) =>
  destination >= FIRST_PAGE && destination < pageCount;

/**
 * @param {number} length
 * @return {IterableIterator<number>}
 */
export const createIterator = length => Array(length).keys();

/**
 * Create a sequence of integers.
 *
 * @param {number} length
 * @return {Array}
 */
export const createSequence = length => [...createIterator(length)];

/**
 * Get the total length of a symmetrical array
 * from the offset length of either side.
 *
 * @param {number} offset
 * @return {number}
 */
export function createSymmetricalLength(offset) {
  const factor = 2;

  return increment(offset * factor);
}

/**
 * Create a symmetrical array of numbers around zero.
 *
 * @example
 * symmetricallyIncrementByOneFromZeroUpTo(2) // [-2, -1, 0, 1, 2]
 *
 * @param {number} length
 * @return {Array}
 */
export const symmetricallyIncrementByOneFromZeroUpTo = length =>
  createSequence(createSymmetricalLength(length)).map(item => item - length);

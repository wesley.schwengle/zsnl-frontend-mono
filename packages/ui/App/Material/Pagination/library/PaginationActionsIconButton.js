// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import IconButton from '@mui/material/IconButton';
import { addScopeAttribute } from '../../../library/addScope';
import { pageExists } from './functions';

/**
 * *Material UI* `IconButton` facade component for pagination.
 *
 * @param {Object} props
 * @param {Object} props.button
 * @param {Number} props.button.destination
 * @param {Function} props.button.icon
 * @param {Object} props.button.buttonStyles
 * @param {string} [props.button.scope]
 * @param {Number} props.page
 * @param {Number} props.pageCount
 * @param {Function} props.onChangePage
 * @return {ReactElement}
 */
export const PaginationActionsIconButton = ({
  button: { destination, icon, buttonStyles, scope },
  onChangePage,
  page,
  pageCount,
}) => (
  <IconButton
    sx={buttonStyles}
    disabled={!pageExists(destination, pageCount) || destination === page}
    onClick={() => {
      onChangePage(destination);
    }}
    {...addScopeAttribute(scope, 'button')}
  >
    {icon}
  </IconButton>
);

export default PaginationActionsIconButton;

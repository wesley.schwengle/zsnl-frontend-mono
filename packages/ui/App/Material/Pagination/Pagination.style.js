// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTheme } from '@mui/material';

const tableMaxWidth = '1600px';
const buttonSize = '28px';
const buttonMargin = '2px';
const nextButtonMargin = '10px';
const buttonSpaceTotal = `${buttonSize} + ${buttonMargin} * 2`;
const nextButtonSpaceTotal = `${buttonSize} + ${nextButtonMargin} * 2`;
const paginatorHeight = '45px';
const tablePadding = '24px';
const tableMargin = '20px';
const cardPadding = '8px';
const tableSideSpace = `${tableMargin} + ${cardPadding}`;
const paginatorSideSpace = `${tableSideSpace} + ${tablePadding}`;
const pageButtonMargin = '20px';
const fontSize = '14px';
const selectRadius = '6px';
const hoverBackgroundColor = '#414852';

/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Pagination component
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const usePaginationStyle = () => {
  const {
    mintlab: { greyscale },
  } = useTheme();

  return {
    width: '100%',
    height: paginatorHeight,
    minHeight: paginatorHeight,
    backgroundColor: greyscale.offBlack,

    '& .MuiTablePagination-toolbar': {
      height: paginatorHeight,
      minHeight: paginatorHeight,
      padding: '0px',
      width: `calc(100% - (${paginatorSideSpace}) * 2)`,
      maxWidth: `calc(${tableMaxWidth} - (${tablePadding}) * 2)`,
      margin: 'auto',
      overflow: 'hidden',
    },
    '& .MuiTablePagination-spacer': {
      display: 'none',
    },
    '& .MuiTablePagination-caption': {
      color: greyscale.lighter,
      fontSize,
      textAlign: 'right',
      paddingRight: '10px',
      '&:empty': {
        display: 'none',
      },
    },
    '& .MuiTablePagination-select': {
      backgroundColor: greyscale.darkest,
      padding: '8px 30px 8px 10px',
      borderRadius: selectRadius,
      width: '50px',
      color: greyscale.lighter,
      fontWeight: 'bold',
      fontSize,
      '&:focus': {
        background: greyscale.darkest,
        borderRadius: selectRadius,
      },
    },
    '& .MuiSelect-icon': {
      backgroundColor: greyscale.darkest,
      borderRadius: selectRadius,
      height: '28px',
      color: greyscale.evenDarker,
    },
    '& .MuiSelect-nativeInput': {
      flexShrink: '0',
    },
    '& .MuiTablePagination-menuItem': {
      color: greyscale.lighter,
      fontWeight: 'bold',
      backgroundColor: greyscale.darkest,
      borderRadius: selectRadius,
      '&:hover': {
        backgroundColor: hoverBackgroundColor,
      },
    },
    '& .MuiMenu-list': {
      backgroundColor: greyscale.darkest,
      borderRadius: selectRadius,
    },
    '& .MuiTablePagination-menuList': {
      padding: '0px',
    },
    '& .MuiTablePagination-selectLabel': {
      color: 'white',
    },
    '& .MuiTablePagination-displayedRows': {
      color: 'white',
    },
  };
};

/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the PaginationActions component
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const usePageButtonStyle = () => {
  const {
    mintlab: { greyscale, radius },
    palette: { common },
  } = useTheme();

  return {
    paginatorButton: {
      padding: '0px',
      margin: `0px ${buttonMargin}`,
      height: buttonSize,
      width: buttonSize,
      borderRadius: radius.buttonSmall,
      fontSize,
      '&:hover': {
        backgroundColor: hoverBackgroundColor,
      },
      fontWeight: 'bold',
      '&&': {
        color: greyscale.lighter,
      },
    },
    currentPage: {
      '&&': {
        color: greyscale.darkest,
        backgroundColor: common.white,
      },
    },
    smallStep: {
      borderRadius: `calc(${buttonSize} / 2)`,
      backgroundColor: greyscale.darkest,
      margin: `0px calc(${nextButtonMargin})`,
    },
    bigStep: {
      fontWeight: 'normal',
    },
    pageButtonsWrapper: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginLeft: 'auto',
      minWidth: `calc((${buttonSpaceTotal}) * 7 + ${pageButtonMargin} * 2 + (${nextButtonSpaceTotal}) * 2)`,
    },
    pageJumpButtons: {
      width: `calc((${buttonSpaceTotal}) * 5)`,
      margin: `0px ${pageButtonMargin}`,
      textAlign: 'right',
    },
    disabled: {
      opacity: '0.4',
      backgroundColor: greyscale.offBlack,
    },
  };
};

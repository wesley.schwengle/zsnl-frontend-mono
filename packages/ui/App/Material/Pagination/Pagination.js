// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import TablePagination from '@mui/material/TablePagination';
import { addScopeAttribute, addScopeProp } from '../../library/addScope';
import PaginationActions from './library/PaginationActions';
import { usePaginationStyle } from './Pagination.style';

// ZS-TODO: check MUI documentation
// `onChangePage` is never called but required
// (we pass it to `ActionsComponent`)
const noop = () => {};

/**
 * Facade for *Material UI* `TablePagination`.
 *
 * @param {Object} props
 * @param {Function} props.changeRowsPerPage
 * @param {Object} props.classes
 * @param {String} props.component
 * @param {Number} props.count
 * @param {Function} props.getNewPage
 * @param {String} props.labelDisplayedRows
 * @param {Number} props.page
 * @param {Number} props.rowsPerPage
 * @param {Array} props.rowsPerPageOptions
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const CustomPaginationActionsTable = ({
  changeRowsPerPage,
  component,
  count,
  getNewPage,
  labelRowsPerPage,
  labelDisplayedRows,
  page,
  rowsPerPageOptions,
  rowsPerPage,
  scope,
}) => {
  const pageCount = Math.ceil(count / rowsPerPage);
  const sx = usePaginationStyle();

  // note: SelectProps and ActionsComponent are UpperCamelCase
  return (
    <TablePagination
      sx={sx}
      component={component}
      count={count}
      labelDisplayedRows={labelDisplayedRows}
      labelRowsPerPage={labelRowsPerPage}
      onRowsPerPageChange={changeRowsPerPage}
      onPageChange={noop}
      page={page}
      rowsPerPage={rowsPerPage}
      rowsPerPageOptions={rowsPerPageOptions}
      ActionsComponent={() => (
        <PaginationActions
          onChangePage={getNewPage}
          page={page}
          pageCount={pageCount}
          {...addScopeProp(scope, 'pagination-actions')}
        />
      )}
      {...addScopeAttribute(scope, 'pagination')}
    />
  );
};

export default CustomPaginationActionsTable;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiCheckbox from '@mui/material/Checkbox';
import MuiFormControlLabel from '@mui/material/FormControlLabel';
import { addScopeAttribute } from '../../library/addScope';
/**
 * *Material Design* **Checkbox** selection control.
 * - facade for *Material-UI* `Checkbox`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Checkbox
 * @see /npm-mintlab-ui/documentation/consumer/manual/Checkbox.html
 *
 * @param {Object} props
 * @param {boolean} [props.checked=false]
 * @param {boolean} [props.disabled=false]
 * @param {string} [props.label]
 * @param {string} [props.name]
 * @param {string} [props.color='primary']
 * @param {Function} [props.onChange]
 * @param {Function} [props.onClick]
 * @param {string} [props.size]
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const Checkbox = ({
  checked = false,
  formControlClasses,
  disabled = false,
  label,
  name,
  color = 'primary',
  onChange,
  onClick,
  size,
  scope,
}) => (
  <MuiFormControlLabel
    classes={formControlClasses}
    onClick={onClick}
    control={
      <MuiCheckbox
        checked={checked}
        disabled={disabled}
        name={name}
        color={color}
        onChange={onChange}
        size={size}
        inputProps={{
          ...addScopeAttribute(scope, 'checkbox'),
        }}
      />
    }
    label={label}
  />
);

export default Checkbox;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTheme } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';
import { SvgIconProps } from '@mui/material/SvgIcon/SvgIcon';
import * as iconMap from './library';

export type IconNameType = keyof typeof iconMap;
export type IconSizeType = keyof Theme['mintlab']['icon'] | number;

export const iconNames = Object.keys(iconMap).reduce(
  (acc, key) => ({ [key]: key, ...acc }),
  {}
) as Record<IconNameType, IconNameType>;

export const Icon: React.ComponentType<{
  children: IconNameType;
  color?: SvgIconProps['color'];
  size?: IconSizeType;
  classes?: Record<string, string>;
  style?: any;
  sx?: Object;
}> = ({
  children,
  classes = {},
  color = 'inherit',
  size = 'medium',
  sx,
  style,
}) => {
  const {
    mintlab: { icon },
  } = useTheme<Theme>();
  const IconComponent = iconMap[children as IconNameType];
  const iconSize = typeof size === 'string' ? icon[size] : size;

  return (
    <span
      className={classes.wrapper}
      style={{
        fontSize: `${iconSize}px`,
        height: `${iconSize}px`,
        display: 'flex',
        ...style,
      }}
    >
      <IconComponent sx={sx} color={color as any} fontSize="inherit" />
    </span>
  );
};

export default Icon;

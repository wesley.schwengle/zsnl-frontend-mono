// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { stories, select, text } from '../../story';
import LoadableDateTimePicker from '.';

stories(module, __dirname, {
  Default() {
    const [value, setValue] = useState(null);
    const handleChange = event => setValue(event?.target?.value);
    const handleClose = () => setValue(null);

    return (
      <div style={{ backgroundColor: '#f5f5f5' }}>
        <LoadableDateTimePicker
          name="story"
          value={value}
          onChange={handleChange}
          onClose={handleClose}
          variantProp={select(
            'Type',
            ['dialog', '@mui/x-date-pickers', 'static'],
            'dialog'
          )}
          placeholder={text('Placeholder', 'Datum en tijd toevoegen')}
          outputFormat="ISO"
          fullWidth
          hideTabs={true}
        />
      </div>
    );
  },
});

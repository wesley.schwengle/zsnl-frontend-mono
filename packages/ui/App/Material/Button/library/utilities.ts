// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

// @see https://material-ui.com/api/button/

export type ButtonColorType =
  | 'default'
  | 'inherit'
  | 'primary'
  | 'secondary'
  | 'review'
  | 'danger';

export type ButtonVariantType =
  | 'text'
  | 'icon'
  | 'outlined'
  | 'contained'
  | 'semiContained';

export type ButtonPresetType =
  | ButtonVariantType
  | 'extraSmall'
  | 'extraLarge'
  | 'primary'
  | 'tiny'
  | 'small'
  | 'medium'
  | 'large'
  | 'huge'
  | 'fullWidth'
  | 'mini'
  | 'default'
  | 'link'
  | 'inherit';

export const colors: ButtonColorType[] = [
  'default',
  'inherit',
  'primary',
  'secondary',
  'review',
  'danger',
];

export const sizes = ['small', 'medium', 'large'];
export const iconSizes = [
  'tiny',
  'extraSmall',
  'small',
  'medium',
  'large',
  'extraLarge',
  'huge',
];

export const variants: ButtonVariantType[] = [
  'text',
  'icon',
  'outlined',
  'contained',
  'semiContained',
];

/**
 * Get a mutually exlcusive array intersection value.
 */
export function oneOf(haystack: string[], needles: string[], fallback: string) {
  const [result, overflow] = needles.filter(name => haystack.includes(name));

  if (result && !overflow) {
    return result;
  }

  return fallback;
}

export function getSizes(presets: ButtonPresetType[]) {
  if (presets.includes('icon')) {
    return iconSizes;
  }

  return sizes;
}

export const parsePresets = (presets: ButtonPresetType[]) =>
  [
    {
      fullWidth: presets.includes('fullWidth'),
      mini: presets.includes('mini') ? 'true' : 'false',
      size: oneOf(getSizes(presets), presets, 'medium'),
      variant: oneOf(variants, presets, 'text'),
    },
    oneOf(colors, presets, 'default'),
  ] as const;

export const parseAction = (action: any) => ({
  href: typeof action === 'string' ? action : undefined,
  onClick: typeof action === 'function' ? action : undefined,
});

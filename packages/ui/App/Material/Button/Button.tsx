// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import IconButton from '@mui/material/IconButton';
import MuiButton from '@mui/material/Button';
import { addScopeAttribute } from '../../library/addScope';
import Icon, { IconSizeType, IconNameType } from '../Icon';
import {
  parseAction,
  parsePresets,
  ButtonPresetType,
  ButtonVariantType,
  ButtonColorType,
} from './library/utilities';
import { useButtonStyle } from './Button.style';

const SemiContainedButton = (properties: any) => (
  <MuiButton {...properties} variant="contained" />
);

type ButtonPropsType = {
  action?: (ev: React.MouseEvent) => any;
  onClick?: (ev: React.MouseEvent) => any;
  component?:
    | React.ReactElement
    | React.ForwardRefExoticComponent<any>
    | string;
  disabled?: boolean;
  label?: string;
  presets?: ButtonPresetType[];
  scope?: string;
  icon?: IconNameType;
  iconSize?: number | IconSizeType;
  color?: ButtonColorType;
  variant?: ButtonVariantType;
  className?: string;
  classes?: Record<string, string>;
  title?: string;
  fullWidth?: boolean;
  endIcon?: any;
  iconTransform?: string;
  [key: string]: any;
};

export const Button: React.ComponentType<ButtonPropsType> = ({
  action,
  children,
  disabled = false,
  icon,
  iconSize = 'extraSmall',
  label,
  presets = [],
  scope,
  classes = {},
  iconTransform = '',
  ...rest
}) => {
  const defaultClasses = useButtonStyle();
  const [props, color] = parsePresets(presets);
  //@ts-ignore
  const { variant } = props;

  function createInstance(instanceProps: any) {
    const compact = {
      icon() {
        const iconStyle = rest.endIcon ? { marginTop: '-7px' } : {};

        return (
          <IconButton
            aria-label={label}
            color={instanceProps.color}
            disabled={disabled}
            onClick={action}
            {...addScopeAttribute(scope, 'button')}
            {...rest}
          >
            <Icon style={iconStyle} size={instanceProps.size}>
              {children as any}
            </Icon>
          </IconButton>
        );
      },
    };

    if (Object.prototype.hasOwnProperty.call(compact, variant)) {
      //@ts-ignore
      return compact[variant]();
    }

    function getChildren() {
      if (icon) {
        return (
          <Fragment>
            <Icon style={{ transform: iconTransform }} size={iconSize}>
              {icon}
            </Icon>
            {children}
          </Fragment>
        );
      }

      return children;
    }

    const baseProps = {
      ...parseAction(action),
      ...instanceProps,
      children: getChildren(),
      classes,
      disabled,
      ...rest,
    };

    if (variant === 'semiContained') {
      return (
        <SemiContainedButton
          {...baseProps}
          {...addScopeAttribute(scope, 'button')}
          classes={{ ...classes, ...defaultClasses }}
        />
      );
    }

    return <MuiButton {...baseProps} {...addScopeAttribute(scope, 'button')} />;
  }

  if (color === 'review') {
    return createInstance({
      ...props,
      color: 'primary',
    });
  }

  if (color === 'danger') {
    return createInstance({
      ...props,
      color: 'secondary',
    });
  }

  return createInstance(props);
};

export default Button;

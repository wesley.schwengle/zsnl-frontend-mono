// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { withStyles } from '@mui/styles';
import Divider from '@mui/material/Divider';

const dialogDividerStyleSheet = () => ({
  root: {
    margin: '1px 0px',
  },
});

export default withStyles(dialogDividerStyleSheet)(Divider);

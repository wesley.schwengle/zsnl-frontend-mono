// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { withStyles } from '@mui/styles';
import DialogActions from '@mui/material/DialogActions';

const dialogActionsStyleSheet = () => ({
  root: {
    margin: 0,
    padding: '16px 8px',

    '& button:first-child': {
      color: 'inherit',
    },
  },
});

export default withStyles(dialogActionsStyleSheet)(DialogActions);

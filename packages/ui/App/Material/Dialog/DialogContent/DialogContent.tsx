// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MUIDialogContent from '@mui/material/DialogContent';

export const DialogContent: React.ComponentType<{
  padded?: boolean;
  children?: any;
}> = ({ children }) => {
  return <MUIDialogContent>{children}</MUIDialogContent>;
};

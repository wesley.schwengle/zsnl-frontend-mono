// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import toJson from 'enzyme-to-json';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
import { Dialog } from './Dialog';

const classes = {
  content: '',
};

/**
 * @test {Dialog}
 */
describe('The `Dialog` component', () => {
  test('renders correctly', () => {
    const component = shallow(<Dialog classes={classes}>test</Dialog>);

    expect(toJson(component)).toMatchSnapshot();
  });
});

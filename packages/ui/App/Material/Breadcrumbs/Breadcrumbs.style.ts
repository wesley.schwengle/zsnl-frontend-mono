// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useBreadcrumbStyles = makeStyles(
  ({ palette: { common, elephant }, typography }: any) => {
    const item = {
      ...typography.h6,
      color: elephant.dark,
    };

    return {
      item,
      separator: {
        fill: elephant.dark,
      },
      link: {
        ...item,
        textDecoration: 'none',
        '&:hover': {
          opacity: 1,
        },
        cursor: 'pointer',
      },
      last: {
        ...typography.h6,
        color: common.black,
        textDecoration: 'none',
      },
    };
  }
);

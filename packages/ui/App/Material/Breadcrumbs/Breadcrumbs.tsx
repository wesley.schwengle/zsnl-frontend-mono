// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MUIBreadcrumbs from '@mui/material/Breadcrumbs';
import { addScopeAttribute, addScopeProp } from '../../library/addScope';
import Icon, { iconNames } from '../Icon/Icon';
import { useBreadcrumbStyles } from './Breadcrumbs.style';
import { getItemsToRender } from './library/functions';
import {
  BreadcrumbRendererType,
  BreadcrumbsPropsType,
  BreadcrumbItemPropsType,
} from './Breadcrumbs.types';

const ZERO = 0;
const ONE = 1;

const Separator = ({ classes }: { classes: any }) => (
  <Icon
    size="small"
    classes={{
      root: classes.separator,
    }}
  >
    {iconNames.navigate_next}
  </Icon>
);

/**
 * Default item renderer
 */
export const defaultItemRenderer: BreadcrumbRendererType = (
  { classes, onItemClick, item, index, scope },
  isLastItem
) => {
  const className = isLastItem ? classes.last : classes.link;

  return (
    <a
      key={index}
      href={item.path}
      onClick={event => onItemClick && onItemClick(event, item)}
      className={className}
      {...(isLastItem && { 'aria-current': 'page' })}
      {...addScopeAttribute(scope, `item-${index === ZERO ? 'first' : index}`)}
    >
      {item.label}
    </a>
  );
};

/**
 * Default last item renderer
 */
export const defaultLastItemRenderer: BreadcrumbRendererType = ({
  item,
  classes,
  index,
  scope,
}) => (
  <span
    key={index}
    className={classes.last}
    {...addScopeAttribute(scope, 'item-last')}
  >
    {item.label}
  </span>
);

/**
 * *Material Design* **Breadcrumbs**.
 * - facade for a subset of `@material-ui/lab/Breadcrumbs`
 */
export const Breadcrumbs: React.ComponentType<BreadcrumbsPropsType> = ({
  maxItems,
  items,
  onItemClick,
  itemRenderer = defaultItemRenderer,
  lastItemRenderer = defaultItemRenderer,
  scope,
  ...restProps
}) => {
  const classes = useBreadcrumbStyles();
  const itemsToRender = maxItems ? getItemsToRender(items, maxItems) : items;

  return (
    <MUIBreadcrumbs
      separator={<Separator classes={classes} />}
      {...addScopeAttribute(scope, 'breadcrumbs')}
      {...restProps}
    >
      {itemsToRender &&
        itemsToRender.map((item, index) => {
          const itemProps: BreadcrumbItemPropsType = {
            item,
            index,
            onItemClick,
            classes,
            ...addScopeProp(scope, 'breadcrumbs'),
          };

          return index < itemsToRender.length - ONE
            ? itemRenderer(itemProps, false)
            : lastItemRenderer(itemProps, true);
        })}
    </MUIBreadcrumbs>
  );
};

export default Breadcrumbs;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, select } from '../../story';
import * as icons from './../Icon/library/';
import IconRounded from '.';

const { keys } = Object;
const colors = ['inherit', 'primary', 'secondary', 'disabled', 'error'];

stories(module, __dirname, {
  Default() {
    const color = select('Color', colors, 'inherit');
    const size = select(
      'Size',
      ['extraSmall', 'small', 'medium', 'large'],
      'medium'
    );
    const identifiers = keys(icons);

    return (
      <div>
        {identifiers.map((name, index) => (
          <span
            key={index}
            style={{
              display: '@mui/x-date-pickers-flex',
              alignItems: 'center',
              margin: '0.2em 0.5em',
              padding: '0.2em 0.5em',
              border: '1px solid #ddd',
              fontFamily: 'Menlo, Consolas',
              fontSize: '0.8rem',
            }}
          >
            <IconRounded color={color} size={size} backgroundColor="lightgreen">
              {name}
            </IconRounded>
            <span
              style={{
                marginLeft: '0.5em',
                color: '#666',
              }}
            >
              {name}
            </span>
          </span>
        ))}
      </div>
    );
  },
});

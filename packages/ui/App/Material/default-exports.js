// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export { withStyles, withTheme } from '@mui/styles';
export { default as Avatar } from '@mui/material/Avatar';
export { default as CssBaseline } from '@mui/material/CssBaseline';
export { default as Accordion } from '@mui/material/Accordion';
export { default as AccordionSummary } from '@mui/material/AccordionSummary';
export { default as AccordionDetails } from '@mui/material/AccordionDetails';
export { default as InputAdornment } from '@mui/material/InputAdornment';

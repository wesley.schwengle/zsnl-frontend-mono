// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { createElement } from 'react';
import classNames from 'classnames';
import NumberFormat from 'react-number-format';
import InputAdornment from '@mui/material/InputAdornment';
import CloseIndicator from '../../Shared/CloseIndicator/CloseIndicator';
import { TextFieldPropsType } from './TextField.types';

export const formatTypes = {
  eurCurrency: {
    prefix: '€',
    thousandSeparator: '.',
    decimalSeparator: ',',
    fixedDecimalScale: true,
    isNumericString: true,
    allowNegative: false,
    decimalScale: 2,
  },
};
export const getEndAdornment = ({
  focusClass,
  endAdornmentClass,
  closeAction,
  closeName,
  endAdornment,
  focus,
}: {
  focusClass?: string;
  endAdornmentClass?: string;
  focus: boolean;
} & Pick<TextFieldPropsType, 'closeAction' | 'closeName' | 'endAdornment'>) => {
  const closeButton = createElement(CloseIndicator, {
    action: closeAction,
    name: closeName,
  });
  const props = {
    position: 'end' as const,
    classes: {
      root: classNames(endAdornmentClass, { [focusClass as any]: focus }),
    },
  };

  if (endAdornment) {
    return createElement(InputAdornment, props, endAdornment);
  } else if (closeAction) {
    return createElement(InputAdornment, props, closeButton);
  }

  return null;
};

export const NumberFormatWrapper = (props: {
  inputRef: any;
  name: string;
  onChange: (ev: any) => void;
  formatType: keyof typeof formatTypes;
  displayType?: 'text' | 'input';
}) => {
  const { inputRef, onChange, formatType, name, ...rest } = props;

  return (
    <NumberFormat
      {...rest}
      getInputRef={inputRef}
      onValueChange={(values: any) => {
        onChange({
          target: {
            value: values.value,
            name,
          },
        });
      }}
      isNumericString={true}
      //@ts-ignore
      {...formatType}
    />
  );
};

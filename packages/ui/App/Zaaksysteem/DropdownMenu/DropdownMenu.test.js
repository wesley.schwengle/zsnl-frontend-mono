// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
// eslint-disable-next-line import/no-unresolved
import toJson from 'enzyme-to-json';
import { DropdownMenu } from './DropdownMenu';

/**
 * @test {DropdownMenu}
 */
describe('The `DropdownMenu` component', () => {
  test('renders correctly', () => {
    const items = [{ label: 'foo' }, { label: 'bar' }];
    const component = shallow(
      <DropdownMenu items={items} classes={{}} manualId="testId">
        <b>Toggle</b>
      </DropdownMenu>
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});

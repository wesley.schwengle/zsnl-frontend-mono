// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

// ZS-FIXME: this is imported from three other components.
// It should not be tied to the Form component (but not be public either).
import React from 'react';
import { withStyles } from '@mui/styles';
import Typography from '@mui/material/Typography';
import { errorLabelStyleSheet } from './ErrorLabel.style';

/**
 * @param {Object} props
 * @param {string} props.label
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const ErrorLabel = ({ label, classes }) => (
  <Typography variant="caption" classes={classes}>
    {label}
  </Typography>
);

export default withStyles(errorLabelStyleSheet)(ErrorLabel);

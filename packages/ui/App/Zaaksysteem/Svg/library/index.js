// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export { getStyle } from './getStyle';
export { isResponsiveStyle } from './isResponsiveStyle';
export { toViewBox } from './toViewBox';

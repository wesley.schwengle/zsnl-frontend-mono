// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withTheme } from '@mui/styles';
import { get } from '@mintlab/kitchen-sink/source';
import IconRounded from '../../Material/IconRounded';
import Icon from '../../Material/Icon';
import getPresets from './library/presets';

/**
 * Zaaksysteem Icon component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Icon
 * @see /npm-mintlab-ui/documentation/consumer/manual/Icon.html
 *
 * @param {Object} props
 * @param {Object} [props.classes]
 * @param {Object} props.theme
 * @param {string} [props.children]
 * @param {string | number} [props.size]
 * @param {string} [props.backgroundColor]
 * @param {string} [props.color]
 * @return {ReactElement}
 */
/* eslint complexity: [2, 8] */
export const ZsIcon = props => {
  const { theme, children, color, backgroundColor, ...restProps } = props;
  const size = props.size || 'medium';
  const preset = get(getPresets(theme), children);
  const { name } = preset;
  const iconColor = color || preset.color;
  const iconBackgroundColor = backgroundColor || preset.backgroundColor;
  const white = theme.palette.common.white;
  const rounded = children.includes('inverted') || props.rounded;

  return rounded ? (
    <IconRounded
      {...restProps}
      backgroundColor={iconBackgroundColor}
      style={{
        display: 'inline-flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: iconColor || white,
        ...props.style,
      }}
      size={size}
    >
      {name}
    </IconRounded>
  ) : (
    <Icon
      {...restProps}
      style={{ color: iconColor || white, ...props.style }}
      size={size}
    >
      {name}
    </Icon>
  );
};

export default withTheme(ZsIcon);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import toJson from 'enzyme-to-json';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
// eslint-disable-next-line import/no-unresolved
import { fill } from '../../test';
import { Layout } from '.';

/**
 * @test {Layout}
 */
describe('The `Layout` component', () => {
  test('renders correctly', () => {
    const drawer = {
      primary: fill(3, {
        icon: 'add',
      }),
      secondary: fill(2, { icon: 'add' }),
    };

    const component = shallow(
      <Layout classes={{ root: '' }} drawer={drawer} />
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});

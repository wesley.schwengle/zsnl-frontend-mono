// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Drawer from '@mui/material/Drawer';
import { addScopeProp } from '../../../../library/addScope';
import CompactButton from './CompactButton';

export const PermanentDrawer = ({
  className,
  navigation: { primary, secondary },
  scope,
}) => (
  <Drawer
    sx={{
      '& .MuiDrawer-paper': {
        position: 'static',
        background: 'transparent',
        border: 'none',
        padding: '8px 2px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
      },
    }}
    className={className}
    variant="permanent"
  >
    <nav>
      <ul
        style={{
          margin: 0,
          padding: 0,
          listStyle: 'none',
          marginBottom: '15px',
        }}
      >
        {primary.map(({ action, icon, label, href, target }, index) => (
          <li key={index}>
            <CompactButton
              action={() => action(label)}
              active={window.location.href.includes(href)}
              icon={icon}
              href={href}
              label={label}
              target={target}
              {...addScopeProp(scope, label)}
            />
          </li>
        ))}
      </ul>
    </nav>
    <nav>
      <ul
        style={{
          margin: 0,
          padding: 0,
          listStyle: 'none',
        }}
      >
        {secondary.map(({ icon, label, href, target }, index) => (
          <li key={index}>
            <CompactButton
              href={href}
              icon={icon}
              label={label}
              target={target}
              {...addScopeProp(scope, label)}
            />
          </li>
        ))}
      </ul>
    </nav>
  </Drawer>
);

export default PermanentDrawer;

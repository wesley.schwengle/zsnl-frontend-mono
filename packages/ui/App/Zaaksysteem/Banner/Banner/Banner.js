// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import ButtonBase from '@mui/material/ButtonBase';
import { withStyles } from '@mui/styles';
import Typography from '@mui/material/Typography';
import Icon from '../../../Material/Icon/Icon';
import { addScopeAttribute, addScopeProp } from '../../../library/addScope';
import DropdownMenu, {
  DropdownMenuList,
} from '../../DropdownMenu/DropdownMenu';
import BannerButton from '../BannerButton/BannerButton';
import { bannerStylesheet } from './Banner.style';

const { isArray } = Array;

/**
 * A single banner. A label must be provided.
 * If an Action is passed to props.primary, a styled button will be rendered.
 * If a single Action is passed to props.secondary, an icon button will be
 * rendered.
 * If an array of Actions is passed to props.secondary, a
 * {@link DropdownMenu} will be rendered with the Actions as menu-items.
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} [props.variant='secondary']
 *    A valid variant name
 * @param {string} props.label
 * @param {Action} [props.primary]
 *    An Action object
 * @param {Action|Array<Action>} [props.secondary]
 *    A single Action object, or an array of Action objects
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const Banner = ({
  classes,
  variant = 'secondary',
  label,
  primary,
  secondary,
  scope,
}) => (
  <div
    className={classNames(classes.outer, {
      [classes[`outer-${variant}`]]: true,
    })}
    {...addScopeAttribute(scope, 'banner')}
  >
    <div
      className={classNames(classes.inner, {
        [classes[`inner-${variant}`]]: true,
      })}
    >
      <Typography
        variant="body2"
        classes={{
          root: classes.label,
        }}
      >
        {label}
      </Typography>

      {getPrimary({
        variant,
        primary,
        scope,
      })}
      {getSecondary({
        secondary,
        scope,
      })}
    </div>
  </div>
);

const getPrimary = ({ variant, primary, scope }) => {
  if (!primary) return null;

  return (
    <BannerButton
      variant={variant}
      action={primary.action}
      {...addScopeProp(scope, 'banner-primary')}
    >
      {primary.label}
    </BannerButton>
  );
};

const getSecondary = ({ secondary, scope }) => {
  if (!secondary) return null;

  if (isArray(secondary)) {
    const triggerButton = (
      <ButtonBase {...addScopeProp(scope, 'banner-secondary-list-button')}>
        <Icon size="small">more_vert</Icon>
      </ButtonBase>
    );

    return (
      <DropdownMenu trigger={triggerButton}>
        <DropdownMenuList items={secondary} />
      </DropdownMenu>
    );
  }

  return (
    <ButtonBase
      onClick={secondary.action}
      {...addScopeProp(scope, 'banner-secondary-button')}
    >
      <Icon size="small">{secondary.icon}</Icon>
    </ButtonBase>
  );
};

export default withStyles(bannerStylesheet)(Banner);

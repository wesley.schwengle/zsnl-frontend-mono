// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useProgressStyles = makeStyles((theme: Theme) => {
  const {
    palette: { cloud, lizard },
  } = theme;

  return {
    wrapper: {
      display: 'flex',
      width: '100%',
      borderRadius: 6,
      backgroundColor: cloud.dark,
      height: 10,
    },
    progress: {
      borderRadius: 6,
      backgroundColor: lizard.main,
    },
  };
});

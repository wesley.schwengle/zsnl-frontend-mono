// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import ReactSelect from 'react-select';
import { useTheme } from '@mui/material';
import { Theme } from '@mintlab/ui/types/Theme';
import defaultFilterOptions from '../library/filterOption';
import { subcomponents } from '../library/subcomponents';
import { BaseSelectPropsType } from '../types/BaseSelectPropsType';
import { useSelectBase } from '../hooks/useSelectBase';
import styleSheet from './GenericSelect.style';

/* eslint-disable complexity */
export const GenericSelect: React.ComponentType<BaseSelectPropsType<any>> = ({
  autoLoad,
  disabled,
  loading,
  focus,
  hasInitialChoices,
  isClearable,
  isMulti,
  value,
  choices,
  getChoices,
  filterOption,
  formatCreateLabel,
  name,
  placeholder,
  translations,
  styles,
  createStyleSheet,
  components,
  startAdornment,
  onBlur,
  onChange,
  onKeyDown,
  menuIsOpen,
  inputValue,
  onInputChange,
  containerRef,
  nestedValue,
}) => {
  const theme = useTheme<Theme>();
  const {
    normalizedValue,
    focusState,
    defaultedTranslations: { loadingMessage, choose },
    eventHandlers,
    fetchedInitialChoices,
  } = useSelectBase({
    autoLoad: Boolean(autoLoad),
    hasInitialChoices: Boolean(hasInitialChoices),
    focus: Boolean(focus),
    name,
    translations,
    getChoices,
    eventType: 'select',
    onBlur,
    onChange,
    onKeyDown,
    value,
    isMulti,
    choices,
    onInputChange,
    nestedValue,
  });

  return (
    <ReactSelect
      {...eventHandlers}
      focus={focusState}
      isClearable={isClearable}
      isDisabled={disabled}
      isLoading={loading}
      isMulti={isMulti}
      value={normalizedValue}
      options={choices || fetchedInitialChoices}
      filterOption={filterOption || defaultFilterOptions}
      formatCreateLabel={formatCreateLabel}
      cacheOptions={true}
      placeholder={placeholder || choose}
      name={name}
      loadingMessage={() => loadingMessage}
      noOptionsMessage={() => null}
      classNamePrefix="react-select"
      startAdornment={startAdornment}
      components={{ ...subcomponents, ...(components || {}) }}
      styles={
        styles ||
        (createStyleSheet || styleSheet)({
          theme,
          focus: focusState,
        })
      }
      menuIsOpen={menuIsOpen}
      inputValue={inputValue}
      {...(containerRef && {
        menuPortalTarget: containerRef,
        menuPosition: 'fixed',
      })}
    />
  );
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { KeyboardEventHandler, InputActionMeta } from 'react-select/src/types';
import { ValueType } from './ValueType';
import { TranslationsType } from './TranslationsType';
import { ReactSelectSubcomponentRecord } from './ReactSelectSubcomponentNames';
import { StyleSheetCreatorType } from './SelectStyleSheetType';

export type MultiValueLabelIconType = ({
  option,
}: {
  option?: any;
}) => React.ReactElement | null;

export type BaseSelectPropsType<T> = {
  autoLoad?: boolean;
  creatable?: boolean;
  createOnBlur?: boolean;
  disabled?: boolean;
  error?: boolean | string;
  loading?: boolean;
  focus?: Boolean;
  generic?: boolean;
  hasInitialChoices?: boolean;
  isClearable?: boolean;
  isMulti?: boolean;
  isSearchable?: boolean;
  value?: ValueType<T> | ValueType<T>[] | string | null;
  choices?: ValueType<T>[];
  options?: ValueType<T>[];
  getChoices?: (input: string) => void;
  filterOption?: (option: ValueType<T>, inputValue?: string) => boolean;
  formatCreateLabel?: (inputValue: string) => React.ReactElement | string;
  name?: string;
  placeholder?: string;
  translations?: TranslationsType;
  styles?: any;
  createStyleSheet?: StyleSheetCreatorType;
  containerRef?: any;
  startAdornment?: React.ReactElement;
  components?: ReactSelectSubcomponentRecord;
  noOptionsMessage?: () => string;
  onBlur?: (event: React.SyntheticEvent) => void;
  onChange?: (event: React.ChangeEvent<any>) => void;
  onKeyDown?: KeyboardEventHandler;
  multiValueLabelIcon?: MultiValueLabelIconType;
  openMenuOnClick?: boolean;
  menuIsOpen?: boolean;
  inputValue?: string;
  onInputChange?: (newValue: string, action: InputActionMeta) => void;
  inputId?: string;
  isOptionDisabled?: any;
  nestedValue?: boolean;
};

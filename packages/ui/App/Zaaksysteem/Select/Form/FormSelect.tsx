// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import ReactSelect from 'react-select';
import { useTheme } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';
import defaultFilterOptions from '../library/filterOption';
import { subcomponents } from '../library/subcomponents';
import { BaseSelectPropsType } from '../types/BaseSelectPropsType';
import { useSelectBase } from '../hooks/useSelectBase';
import { createSharedStyleSheet } from './Shared.style';

/* eslint complexity: [2,8] */
export const FormSelect: React.ComponentType<BaseSelectPropsType<any>> = ({
  autoLoad,
  disabled,
  error,
  loading,
  hasInitialChoices,
  isClearable,
  isMulti = false,
  isSearchable,
  value,
  choices,
  getChoices,
  filterOption,
  formatCreateLabel,
  name,
  translations,
  styles,
  components,
  noOptionsMessage,
  onBlur,
  onChange,
  onKeyDown,
  containerRef,
  multiValueLabelIcon,
  openMenuOnClick = true,
  inputId,
  placeholder,
  isOptionDisabled,
  nestedValue,
}) => {
  const theme = useTheme<Theme>();

  const {
    focusState,
    defaultedTranslations: { loadingMessage, choose },
    eventHandlers,
    normalizedValue,
  } = useSelectBase({
    autoLoad: Boolean(autoLoad),
    hasInitialChoices: Boolean(hasInitialChoices),
    focus: Boolean(focus),
    name,
    translations,
    getChoices,
    eventType: 'select',
    onBlur,
    onChange,
    onKeyDown,
    value,
    isMulti,
    choices,
    placeholder,
    nestedValue,
  });

  return (
    <ReactSelect
      {...eventHandlers}
      isOptionDisabled={isOptionDisabled}
      isClearable={isClearable}
      isDisabled={disabled}
      isLoading={loading}
      isMulti={isMulti}
      isSearchable={isSearchable}
      value={normalizedValue}
      name={name}
      options={choices}
      filterOption={filterOption || defaultFilterOptions}
      formatCreateLabel={formatCreateLabel}
      cacheOptions={true}
      loadingMessage={() => loadingMessage}
      noOptionsMessage={noOptionsMessage || (() => null)}
      placeholder={choose}
      multiValueLabelIcon={multiValueLabelIcon}
      styles={
        styles ||
        createSharedStyleSheet({
          theme,
          error,
        })
      }
      classNamePrefix="react-select"
      id={name}
      inputId={inputId || `form-select-${name}-input`}
      components={{
        ...subcomponents,
        ...(components || {}),
      }}
      focus={focusState}
      {...(containerRef && {
        menuPortalTarget: containerRef,
        menuPosition: 'fixed',
      })}
      openMenuOnClick={openMenuOnClick}
    />
  );
};

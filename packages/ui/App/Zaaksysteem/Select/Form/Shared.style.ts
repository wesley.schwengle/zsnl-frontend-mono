// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { StyleSheetCreatorType } from '../types/SelectStyleSheetType';

export const createSharedStyleSheet: StyleSheetCreatorType = ({
  theme: {
    palette: { primary, common, text, error: errorPalette },
    typography: { fontFamily, body2 },
    zIndex,
    mintlab: { greyscale, radius },
  },
  error,
  focus,
}) => {
  const borderBottom = () => {
    if (focus) {
      return `2px solid ${error ? errorPalette.main : primary.main}`;
    }
  };

  const backgroundColor = (hover?: boolean) => {
    if (error && !focus) {
      return errorPalette.light;
    } else if (focus || hover) {
      return '#f1f1f3';
    } else {
      return '#F7F7F8';
    }
  };

  const colorRules = {
    color: greyscale.offBlack,
    '&:hover': {
      color: greyscale.offBlack,
    },
  };

  return {
    container(base) {
      return {
        ...base,
        width: '100%',
        fontFamily,
      };
    },
    control(base) {
      return {
        ...base,
        boxShadow: 'none',
        border: 'none',
        borderBottom: borderBottom(),
        '&:hover': {
          borderBottom: borderBottom(),
          backgroundColor: backgroundColor(true),
        },
        backgroundColor: backgroundColor(),
        borderColor: 'transparent',
      };
    },
    valueContainer(base) {
      return {
        ...base,
        padding: '8px 12px',
        minHeight: '42px',
      };
    },
    option(base, state) {
      return {
        ...base,
        backgroundColor: state.isSelected
          ? primary.lighter
          : state.isFocused
          ? primary.lightest
          : common.white,
        color: text.primary,
      };
    },
    placeholder(base) {
      return {
        ...base,
        color: 'rgba(0, 0, 0, 0.87)',
        opacity: 0.42,
        fontWeight: 'normal',
        fontSize: '15px',
      };
    },
    input(base) {
      return {
        ...base,
        input: {
          fontFamily,
        },
      };
    },
    multiValue(base) {
      return {
        ...base,
        borderRadius: radius.chip,
        padding: '2px',
      };
    },
    multiValueRemove(base) {
      return {
        ...base,
        color: greyscale.darkest,
        backgroundColor: greyscale.darker,
        borderRadius: radius.chipRemove,
        transform: 'scale(0.85)',
      };
    },
    menu(base) {
      return {
        ...base,
        ...body2,
        zIndex: zIndex.options,
      };
    },
    // Indicators
    indicatorsContainer(base) {
      return {
        ...base,
        padding: '6px',
      };
    },
    dropdownIndicator(base) {
      return {
        ...base,
        ...colorRules,
        padding: 0,
      };
    },
    clearIndicator(base) {
      return {
        ...base,
        ...colorRules,
        backgroundColor: backgroundColor(),
        padding: 0,
      };
    },
    indicatorSeparator(base) {
      return {
        ...base,
        display: 'none',
      };
    },
  };
};

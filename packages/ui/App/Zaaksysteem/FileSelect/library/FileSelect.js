// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Dropzone from 'react-dropzone';
import classnames from 'classnames';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { withStyles } from '@mui/styles';
import Typography from '@mui/material/Typography';
import Render from '../../../Abstract/Render/Render';
import Button from '../../../Material/Button/Button';
import { addScopeProp } from '../../../library/addScope';
import { fileSelectStylesheet } from './FileSelect.style';

/* eslint complexity: [2, 5] */
export const FileSelect = ({
  selectInstructions,
  dragInstructions,
  dropInstructions,
  orLabel,
  onDrop,
  classes,
  fileList,
  hasFiles,
  error,
  scope,
  accept,
  multiValue = true,
  applyBackground = true,
}) => (
  <Dropzone onDrop={onDrop} accept={accept}>
    {({ getRootProps, getInputProps, isDragActive, open }) => {
      const inputProps = getInputProps({
        multiple: multiValue,
      });
      const rootProps = getRootProps();

      return (
        <div
          className={classnames({
            [classes.wrapper]: applyBackground,
            [classes.error]: Boolean(error),
          })}
          {...cloneWithout(rootProps, 'onClick', 'tabIndex')}
        >
          <input {...inputProps} />

          <div className={classes.fileList}>{fileList}</div>

          <div>
            <div className={classes.form}>
              <Render condition={!hasFiles}>
                <Typography variant="subtitle1">
                  {isDragActive ? dropInstructions : dragInstructions}
                </Typography>
                <div className={classes.orLabel}>
                  <Typography variant="caption">- {orLabel} -</Typography>
                </div>
              </Render>
              <Render
                condition={(!hasFiles || multiValue) && selectInstructions}
              >
                <Button
                  presets={['primary', 'semiContained']}
                  action={open}
                  {...addScopeProp(scope, 'file-select', 'select')}
                >
                  {selectInstructions}
                </Button>
              </Render>
            </div>
          </div>
        </div>
      );
    }}
  </Dropzone>
);

export default withStyles(fileSelectStylesheet)(FileSelect);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import {
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
} from 'react-beautiful-dnd';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import {
  SortDirection,
  TableHeaderProps,
  SortDirectionType,
} from 'react-virtualized';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import classNames from 'classnames';
import defaultRowRenderer from './RowRenderer';
import {
  SortType,
  ColumnType,
  GetVisibleColumnsType,
  SortableTablePropsType,
  RowComponentType,
} from './types/SortableTableTypes';

export const getRowRenderer =
  (
    sorting: SortableTablePropsType['sorting'],
    classes: SortableTablePropsType['classes']
  ) =>
  (props: any) => {
    const { index, rowData } = props;
    const normalizedId = rowData.uuid || rowData.id;

    if (sorting === 'none' || sorting === 'column') {
      return defaultRowRenderer(props);
    } else if (sorting === 'dragdrop') {
      return (
        <Draggable draggableId={normalizedId} index={index} key={normalizedId}>
          {(provided: DraggableProvided, snapshot: DraggableStateSnapshot) => {
            return (
              <Row
                provided={provided}
                isDragging={snapshot.isDragging}
                classes={classes}
                {...props}
              />
            );
          }}
        </Draggable>
      );
    }
  };

export const getHeaderRenderer =
  (
    classes: any,
    sorting: SortType,
    sortDirection: SortDirectionType,
    disableSort?: boolean
  ) =>
  ({ dataKey, label, sortBy }: TableHeaderProps) => {
    const sortHeader = sortBy === dataKey;
    return (
      <div
        className={classNames(classes.tableCell, classes.tableCellHeader, {
          [classes.sortHeader]: sortHeader,
          [classes.tableCellHeaderDisabled]:
            disableSort === true || sorting !== 'column',
        })}
      >
        {label}
        {sortHeader ? (
          <Icon
            size="extraSmall"
            classes={{
              root:
                sortDirection === SortDirection.ASC
                  ? classes.sortAsc
                  : classes.sortDesc,
            }}
          >
            {iconNames.arrow_back}
          </Icon>
        ) : null}
      </div>
    );
  };

export const Row = ({
  provided,
  isDragging,
  isScrolling,
  style = {},
  classes,
  draggingText,
  ...rest
}: RowComponentType) => {
  const newProps = {
    ...cloneWithout(rest, ''),
    ...cloneWithout(provided.draggableProps, 'style'),
    ...provided.dragHandleProps,
    ref: provided.innerRef,
    style: {
      ...style,
      ...provided.draggableProps.style,
    },
  };

  if (!isDragging) {
    return defaultRowRenderer(newProps);
  } else {
    return (
      <div {...newProps} className={classes.draggingRow}>
        {draggingText}
      </div>
    );
  }
};

export const getVisibleColumns = ({
  columns,
  width,
  sorting,
  selectable,
  classes,
}: GetVisibleColumnsType): ColumnType[] => {
  const baseColumns = [];

  if (sorting === 'dragdrop') {
    baseColumns.push({
      label: '',
      name: 'draghandle',
      width: 26,
      disableSort: true,
      flexShrink: 0,
      /* eslint-disable-next-line */
      cellRenderer: () => (
        <Icon size="small" classes={{ root: classes.dragHandle }}>
          {iconNames.drag_indicator}
        </Icon>
      ),
    });
  }

  if (selectable) {
    baseColumns.push({
      label: '',
      name: 'checked',
      width: 40,
      disableSort: true,
      flexShrink: 0,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: any) => (
        <div className={classes.selectCell}>
          {typeof rowData.selected !== 'undefined' ? (
            <Checkbox
              checked={rowData.selected}
              name={`checkbox-${rowData.uuid}`}
              color={'primary'}
            />
          ) : null}
        </div>
      ),
    });
  }

  return [
    ...baseColumns,
    ...columns.filter(
      (thisColumn: ColumnType) =>
        !thisColumn.showFromWidth || thisColumn.showFromWidth <= width
    ),
  ];
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const TYPE_CONTACTCHANNEL_FRONTDESK = 'frontdesk';
export const TYPE_CONTACTCHANNEL_ASSIGNEE = 'assignee';
export const TYPE_CONTACTCHANNEL_MAIL = 'mail';
export const TYPE_CONTACTCHANNEL_PHONE = 'phone';
export const TYPE_CONTACTCHANNEL_EMAIL = 'email';
export const TYPE_CONTACTCHANNEL_WEBFORM = 'webform';
export const TYPE_CONTACTCHANNEL_SOCIALMEDIA = 'social_media';
export const TYPE_CONTACTCHANNEL_EXTERNAL = 'external';

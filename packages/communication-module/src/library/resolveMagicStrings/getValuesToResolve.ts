// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  FormValue,
  NestedFormValue,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { FlatFormValues } from './resolveMagicStrings.types';
import { mapFieldNameToApi } from './mapFieldNames';

const containsMagicString = (value: FormValue): boolean => {
  const MAGIC_STRING_REGEXP = /\[\[(\s+)?([a-z0-9_]+)(\s+)?\]\]/g;

  return (value as NestedFormValue).value
    ? MAGIC_STRING_REGEXP.test((value as NestedFormValue).value.toString())
    : MAGIC_STRING_REGEXP.test(value.toString());
};

const valueIsResolvable = (value: FormValue | FormValue[]): boolean => {
  return Array.isArray(value)
    ? value.some(item => containsMagicString(item))
    : containsMagicString(value);
};

export function getValuesToResolve<Values>(
  values: FlatFormValues<Values>
): FlatFormValues<Values> {
  return Object.entries(values).reduce<FlatFormValues<Values>>((acc, entry) => {
    const [name, value] = entry as [string, FormValue | FormValue[] | null];
    const fieldName = mapFieldNameToApi(name);

    return value && valueIsResolvable(value) && fieldName
      ? {
          ...acc,
          [fieldName]: value,
        }
      : acc;
  }, {} as any);
}

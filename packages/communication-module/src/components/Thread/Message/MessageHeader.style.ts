// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';
import { MessageHeaderPropsType } from './MessageHeader';

export const useMessageHeaderStyle = makeStyles(
  ({ mintlab: { greyscale }, breakpoints, palette: { primary } }: Theme) => ({
    header: {
      [breakpoints.up('sm')]: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
      },
    },
    icon: {
      marginTop: 5,
      minWidth: 50,
      display: 'flex',
      justifyContent: 'left',
    },
    meta: {
      flex: '1 1 auto',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      minWidth: 0,
    },
    title: {
      display: 'flex',
      justifyContent: 'flex-start',
    },
    titleMain: {
      whiteSpace: 'nowrap',
      fontWeight: ({ isUnread }: MessageHeaderPropsType) =>
        isUnread ? 700 : 500,
    },
    titleSub: {
      color: greyscale.darker,
      marginLeft: 10,
    },
    info: {
      minWidth: 0,
      color: ({ isUnread }: MessageHeaderPropsType) =>
        isUnread ? primary.main : greyscale.darkest,
    },
    dateAndMenu: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      '&>span>svg': {
        paddingBottom: 4,
      },
    },
    date: {
      marginLeft: 5,
      color: ({ isUnread }: MessageHeaderPropsType) =>
        isUnread ? primary.main : greyscale.darkest,
    },
    link: {
      color: primary.main,

      '&:hover, &:focus': {
        color: primary.darker,
      },
    },
  })
);

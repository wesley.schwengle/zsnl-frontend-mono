// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { ReactElement } from 'react';
import fecha from 'fecha';
import { useTranslation } from 'react-i18next';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import Typography from '@mui/material/Typography';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useMessageHeaderStyle } from './MessageHeader.style';
import { MessageActions } from './MessageActions/MessageActions';

const formatDate = (format: string, zulu: string) => {
  const newDate = new Date(zulu);
  return fecha.format(newDate, format);
};

export interface MessageHeaderPropsType {
  id: string;
  date: string;
  title: string;
  isUnread?: boolean;
  subTitle?: string;
  info?: string;
  hasSourceFile?: boolean;
  icon: ReactElement;
  hasAttachment?: boolean;
  failureReason?: string;
}

const MessageHeader: React.FunctionComponent<
  MessageHeaderPropsType
> = props => {
  const {
    date,
    title,
    subTitle,
    info,
    icon,
    id,
    hasSourceFile = false,
    hasAttachment = false,
    failureReason = '',
  } = props;
  const width = useWidth();
  //@ts-ignore
  const classes = useMessageHeaderStyle(props);
  const [t] = useTranslation('communication');
  const dateFormat =
    width === 'xs' ? t('thread.dateFormat') : t('thread.dateFormatLong');

  return (
    <div className={classes.header}>
      <div style={{ display: 'flex' }}>
        {width !== 'xs' && <div className={classes.icon}>{icon}</div>}
        <div className={classes.meta}>
          <div className={classes.title}>
            <Typography variant="body1" classes={{ root: classes.titleMain }}>
              {title}
            </Typography>
            <Typography variant="body1" classes={{ root: classes.titleSub }}>
              {subTitle}
            </Typography>
          </div>
          {Boolean(info) && (
            <Typography variant="body2" classes={{ root: classes.info }}>
              {info}
            </Typography>
          )}
        </div>
      </div>
      <div className={classes.dateAndMenu}>
        {hasAttachment && (
          <Icon style={{ color: '#1860C3' }} size="small">
            {iconNames.attachment}
          </Icon>
        )}
        {failureReason && (
          <Tooltip style={{ width: 'auto' }} title={failureReason}>
            <Icon style={{ color: '#ff0000' }} size="small">
              {iconNames.warning}
            </Icon>
          </Tooltip>
        )}
        <Typography variant="caption" classes={{ root: classes.date }}>
          {formatDate(dateFormat, date)}
        </Typography>
        <MessageActions id={id} hasSourceFile={hasSourceFile} />
      </div>
    </div>
  );
};

export default MessageHeader;

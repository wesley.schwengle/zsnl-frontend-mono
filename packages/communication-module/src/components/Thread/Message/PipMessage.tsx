// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import { ExternalMessageType } from '../../../types/Message.types';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';
import MessageSenderIcon from './MessageSenderIcon/MessageSenderIcon';
import CreateExternalMessageTitle from './library/CreateExternalMessageTitle';

type PipMessagePropsType = {
  message: ExternalMessageType;
  caseNumber?: string;
  expanded?: boolean;
  isUnread: boolean;
};

export const PipMessage: React.FunctionComponent<PipMessagePropsType> = ({
  message,
  isUnread,
  expanded = false,
}) => {
  const [t] = useTranslation('communication');
  const {
    content,
    createdDate,
    sender,
    type,
    summary,
    subject,
    attachments,
    id,
    failureReason,
  } = message;
  const [isExpanded, setExpanded] = useState(isUnread || expanded);
  //@ts-ignore
  const icon = <MessageSenderIcon type={sender ? sender.type : 'person'} />;
  const title = CreateExternalMessageTitle(message);

  return (
    <Accordion
      sx={{
        padding: 0,
        boxShadow: 'none',
        '&:before': {
          height: 0,
        },
      }}
      expanded={isExpanded}
    >
      <AccordionSummary
        onClick={() => setExpanded(!isExpanded)}
        sx={{
          height: '100px',
          padding: 0,
          boxShadow: 'none',
          '& .MuiExpansionPanelSummary-content': {
            margin: 0,
            display: 'inline',
          },
        }}
      >
        <MessageHeader
          date={createdDate}
          title={title}
          icon={icon}
          id={id}
          isUnread={isUnread}
          info={isExpanded ? subject : summary}
          hasAttachment={Boolean(attachments.length)}
          failureReason={failureReason}
          {...(sender &&
            sender.type === 'employee' && {
              subTitle: t('thread.pipMessage.subTitle'),
            })}
        />
      </AccordionSummary>
      <AccordionDetails sx={{ padding: 0, display: 'inline' }}>
        <MessageContent
          content={content}
          type={type}
          attachments={attachments}
        />
      </AccordionDetails>
    </Accordion>
  );
};

export default PipMessage;

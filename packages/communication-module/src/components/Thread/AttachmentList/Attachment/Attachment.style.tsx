// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAttachmentStyle = makeStyles(
  ({ mintlab: { shadows, greyscale }, palette: { primary } }: any) => ({
    wrapper: {
      width: '100%',
      backgroundColor: greyscale.dark,
      boxShadow: shadows.flat,
      display: 'flex',
      borderRadius: 8,
      alignItems: 'center',
    },
    metaWrapper: {
      padding: '0 0 0 16px',
      flexGrow: 1,
      display: 'flex',
      alignItems: 'center',
    },
    name: {
      flexGrow: 1,
      marginLeft: 16,
      textAlign: 'left',
      textDecoration: 'none',
      color: primary.dark,
    },
    size: {},
    link: {
      color: primary.main,
      textDecoration: 'underline',
      '&:hover': {
        textDecoration: 'underline',
      },
    },
  })
);

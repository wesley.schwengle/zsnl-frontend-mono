// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import {
  CONTACT_FINDER,
  CASE_FINDER,
} from '../../../../library/communicationFieldTypes.constants';
import * as CHANNELS from '../../../../library/communicationChannels.constants';
import { SaveContactMomentFormValuesType } from '../../../../types/Message.types';
import { CommunicationContextType } from '../../../../types/Context.types';

export const getContactMomentFormDefinition = ({
  contactName,
  contactUuid,
  caseUuid,
  capabilities: { canSelectContact, canSelectCase },
}: CommunicationContextType): FormDefinition<SaveContactMomentFormValuesType> => {
  const defaultValue = {
    label: contactName,
    value: contactUuid,
  };

  return [
    {
      name: 'contact_uuid',
      type: CONTACT_FINDER,
      hidden: canSelectContact === false,
      required: true,
      placeholder: 'communication:addFields.contact',
      label: 'communication:addFields.contact',
      ...(canSelectContact && contactName && contactUuid
        ? {
            choices: [defaultValue],
            value: defaultValue,
          }
        : {
            value: contactUuid ? { value: contactUuid } : null,
          }),
    },
    {
      name: 'case_uuid',
      type: CASE_FINDER,
      hidden: canSelectCase === false,
      value: caseUuid ? caseUuid : null,
      required: false,
      placeholder: 'communication:addFields.case',
      label: 'communication:addFields.case',
    },
    {
      name: 'channel',
      type: fieldTypes.CHOICE_CHIP,
      value: 'assignee',
      required: true,
      label: 'communication:addFields.channel',
      placeholder: '',
      choices: [
        {
          label: `communication:channels.${CHANNELS.TYPE_CHANNEL_ASSIGNEE}`,
          value: 'assignee',
          renderIcon() {
            return (
              <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_ASSIGNEE}`}</ZsIcon>
            );
          },
        },
        {
          label: `communication:channels.${CHANNELS.TYPE_CHANNEL_MAIL}`,
          value: 'mail',
          renderIcon() {
            return (
              <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_MAIL}`}</ZsIcon>
            );
          },
        },
        {
          label: `communication:channels.${CHANNELS.TYPE_CHANNEL_PHONE}`,
          value: 'phone',
          renderIcon() {
            return (
              <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_PHONE}`}</ZsIcon>
            );
          },
        },
        {
          label: `communication:channels.${CHANNELS.TYPE_CHANNEL_EMAIL}`,
          value: 'email',
          renderIcon() {
            return (
              <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_EMAIL}`}</ZsIcon>
            );
          },
        },
        {
          label: `communication:channels.${CHANNELS.TYPE_CHANNEL_POSTEX}`,
          value: 'postex',
          renderIcon() {
            return (
              <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_POSTEX}`}</ZsIcon>
            );
          },
        },
        {
          label: `communication:channels.${CHANNELS.TYPE_CHANNEL_WEBFORM}`,
          value: 'webform',
          renderIcon() {
            return (
              <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_WEBFORM}`}</ZsIcon>
            );
          },
        },
        {
          label: `communication:channels.${CHANNELS.TYPE_CHANNEL_SOCIALMEDIA}`,
          value: 'social_media',
          renderIcon() {
            return (
              <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_SOCIALMEDIA}`}</ZsIcon>
            );
          },
        },
        {
          label: `communication:channels.${CHANNELS.TYPE_CHANNEL_FRONTDESK}`,
          value: 'frontdesk',
          renderIcon() {
            return (
              <ZsIcon size="tiny">{`channel.inverted.${CHANNELS.TYPE_CHANNEL_FRONTDESK}`}</ZsIcon>
            );
          },
        },
      ],
    },
    {
      name: 'direction',
      type: fieldTypes.CHOICE_CHIP,
      value: 'incoming',
      required: true,
      label: 'communication:addFields.direction',
      placeholder: 'communication:addFields.direction',
      choices: [
        {
          label: 'communication:direction.incoming',
          value: 'incoming',
        },
        {
          label: 'communication:direction.outgoing',
          value: 'outgoing',
        },
      ],
    },
    {
      name: 'content',
      type: fieldTypes.TEXT,
      multi: true,
      value: '',
      required: true,
      placeholder: 'communication:addFields.content',
      isMultiline: true,
      rows: 7,
    },
  ];
};

export default getContactMomentFormDefinition;

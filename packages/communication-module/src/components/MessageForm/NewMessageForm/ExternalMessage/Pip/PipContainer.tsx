// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import {
  SimpleExternalMessageForm,
  SimpleExternalMessageFormProps,
} from '../../../SimpleExternalMessageForm/SimpleExternalMessageForm';
import {
  saveMessage,
  setAddCommunicationPending,
} from '../../../../../store/add/communication.add.actions';
import { CommunicationRootStateType } from '../../../../../store/communication.reducer';
import { SaveMessageFormValuesType } from '../../../../../types/Message.types';
import { resolveMagicStrings } from '../../../../../library/resolveMagicStrings';

type PropsFromStateType = Pick<
  SimpleExternalMessageFormProps<SaveMessageFormValuesType>,
  'busy' | 'caseUuid' | 'contactUuid' | 'context' | 'enablePreview'
>;

type PropsFromDispatchType = Pick<
  SimpleExternalMessageFormProps<SaveMessageFormValuesType>,
  'save'
>;

const mapStateToProps = (
  stateProps: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      add: { state },
      context: { context, caseUuid, contactUuid },
    },
  } = stateProps;

  return {
    caseUuid,
    contactUuid,
    context,
    enablePreview: context !== 'pip',
    busy: state === AJAX_STATE_PENDING,
  };
};

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => {
  return {
    async save(values) {
      /*
       * --- START TEMPORARY CODE ---
       * This is temporary until Python can resolve magic string
       */
      dispatch(setAddCommunicationPending());

      const resolvedValues =
        await resolveMagicStrings<SaveMessageFormValuesType>(
          values,
          values.case_uuid
        );

      /* --- END TEMPORARY CODE --- */

      // Todo: Fix Nullable type in FormValues shape
      const { case_uuid, subject, content, thread_uuid, attachments } =
        resolvedValues as any;
      const action = saveMessage({
        case_uuid,
        subject,
        thread_uuid,
        content,
        message_type: 'pip',
        attachments,
      });
      dispatch(action as any);
    },
  };
};

export default connect<
  PropsFromStateType,
  PropsFromDispatchType,
  Pick<SimpleExternalMessageFormProps, 'cancel'>,
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(SimpleExternalMessageForm);

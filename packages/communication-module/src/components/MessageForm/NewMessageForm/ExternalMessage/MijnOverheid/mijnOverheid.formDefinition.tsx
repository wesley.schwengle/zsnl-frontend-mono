// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { CASE_SELECTOR } from '../../../../../library/communicationFieldTypes.constants';
import { SaveMessageFormValuesType } from '../../../../../types/Message.types';

const mijnOverheidFormDefinition: FormDefinition<SaveMessageFormValuesType> = [
  {
    name: 'case_uuid',
    type: CASE_SELECTOR,
    value: '',
    required: true,
    placeholder: 'communication:addFields.selectCase',
    label: 'communication:addFields.selectCase',
    isSearchable: false,
  },
  {
    name: 'subject',
    type: fieldTypes.TEXT,
    multi: true,
    value: '',
    required: true,
    placeholder: 'communication:addFields.subject',
  },
  {
    name: 'content',
    type: fieldTypes.TEXT,
    multi: true,
    value: '',
    required: true,
    placeholder: 'communication:addFields.message',
    isMultiline: true,
    rows: 7,
  },
];

export default mijnOverheidFormDefinition;

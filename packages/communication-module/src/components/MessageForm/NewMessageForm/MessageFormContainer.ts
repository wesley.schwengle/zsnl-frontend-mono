// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { CommunicationRootStateType } from '../../../store/communication.reducer';
import {
  TYPE_PIP_MESSAGE,
  TYPE_EMAIL,
} from '../../../library/communicationTypes.constants';
import MessageForm, { MessageFormPropsType } from './MessageForm';

type PropsFromStateType = Pick<
  MessageFormPropsType,
  'canCreate' | 'defaultSubType'
>;

const mapStateToProps = ({
  communication: {
    context: {
      capabilities: { canCreate, canCreatePipMessage, canCreateEmail },
    },
  },
}: CommunicationRootStateType): PropsFromStateType => {
  const [defaultSubType] = [
    [canCreatePipMessage, TYPE_PIP_MESSAGE],
    [canCreateEmail, TYPE_EMAIL],
  ]
    .filter(([condition]) => condition)
    .map(([, type]) => type);

  return {
    canCreate,
    defaultSubType: defaultSubType
      ? (defaultSubType as PropsFromStateType['defaultSubType'])
      : undefined,
  };
};

export default connect<PropsFromStateType, {}, {}, CommunicationRootStateType>(
  mapStateToProps
)(MessageForm);

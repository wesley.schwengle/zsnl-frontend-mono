// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import MUITabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import { useTheme } from '@mui/material';
//@ts-ignore
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { addScopeAttribute } from '@mintlab/ui/App/library/addScope';
import { Theme } from '@mintlab/ui/types/Theme';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
  TYPE_EXTERNAL_MESSAGE,
} from '../../../../library/communicationTypes.constants';

export type TabsPropsType = {
  type: string;
  canCreateContactMoment: boolean;
  canCreateNote: boolean;
  canCreateMessage: boolean;
};

const Tabs: React.FunctionComponent<TabsPropsType> = ({
  type,
  canCreateContactMoment,
  canCreateNote,
  canCreateMessage,
}) => {
  const {
    typography,
    palette: { primary },
    mintlab: { greyscale },
  } = useTheme<Theme>();
  const [t] = useTranslation('communication');

  type LinkComponentPropsType = {
    linktype: string;
  };

  const LinkComponent = React.forwardRef<
    HTMLAnchorElement,
    LinkComponentPropsType
  >((props, ref) => <Link to={`../new/${props.linktype}`} {...props} />);
  LinkComponent.displayName = 'TabLink';

  if (!canCreateContactMoment && !canCreateNote) return null;

  const tabStyle = {
    textTransform: 'none',
    flexBasis: 'auto',
    flexDirection: 'row',
    display: 'inline-flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '18px',
    padding: '3px 20px',
    backgroundColor: 'transparent',
    color: greyscale.darkest,
    '& .MuiSelected': {
      color: primary.main,
      backgroundColor: primary.lightest,
    },
    ...typography.subtitle1,
    '&&& > span': {
      margin: 0,
      marginRight: '12px',
    },
  };

  return (
    <React.Fragment>
      <MUITabs
        sx={{
          marginBottom: '36px',
          minHeight: '60px',
          '& .MuiTabs-flexContainer': {
            '&>:nth-child(2)': {
              margin: '0px 20px',
            },
          },
        }}
        TabIndicatorProps={{
          sx: { display: 'none' },
        }}
        value={type}
        variant="fullWidth"
        indicatorColor="primary"
      >
        {canCreateContactMoment && (
          <Tab
            label={t('threadTypes.contact_moment')}
            value={TYPE_CONTACT_MOMENT}
            linktype={TYPE_CONTACT_MOMENT}
            icon={<Icon size="small">{iconNames.chat_bubble}</Icon>}
            disableTouchRipple={true}
            component={LinkComponent}
            sx={tabStyle}
            {...addScopeAttribute('add-form', 'tab', TYPE_CONTACT_MOMENT)}
          />
        )}
        {canCreateNote && (
          <Tab
            label={t('threadTypes.note')}
            value={TYPE_NOTE}
            linktype={TYPE_NOTE}
            icon={<Icon size="small">{iconNames.sort}</Icon>}
            sx={tabStyle}
            disableTouchRipple={true}
            component={LinkComponent}
            {...addScopeAttribute('add-form', 'tab', TYPE_NOTE)}
          />
        )}

        {canCreateMessage && (
          <Tab
            label={t('communication:threadTypes.message')}
            value={TYPE_EXTERNAL_MESSAGE}
            linktype={TYPE_EXTERNAL_MESSAGE}
            icon={<Icon size="small">{iconNames.email}</Icon>}
            sx={tabStyle}
            disableTouchRipple={true}
            component={LinkComponent}
            {...addScopeAttribute('add-form', 'tab', TYPE_EXTERNAL_MESSAGE)}
          />
        )}
      </MUITabs>
    </React.Fragment>
  );
};

export default Tabs;

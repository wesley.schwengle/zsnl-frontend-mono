// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { EmailTemplateDataType } from '../../../../types/EmailIntegration.types';

export const parseHtmlEmailTemplate = (
  values: any,
  emailTemplateData: EmailTemplateDataType
) => {
  const template = encodeURIComponent(emailTemplateData.template);
  const content = encodeURIComponent(values.content);
  // replace line breaks
  const parsedContent = content.replace(/%0A/g, '<br />');

  const parsedHtml = template
    // {{message}}
    .replace('%7B%7Bmessage%7D%7D', parsedContent)
    // replace linebreaks
    .replace('%0A', '<br />')
    // {{image_url}}
    .replace(
      '%7B%7Bimage_url%7D%7D',
      `/api/v1/file/${emailTemplateData.imageUuid}/download?inline=1`
    )
    // <script>
    .replace('%3Cscript%3E', '');

  return {
    ...values,
    content: decodeURIComponent(parsedHtml),
  };
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classnames from 'classnames';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source';
import { Render } from '@mintlab/ui/App/Abstract/Render';
import { GenericMessageFormPropsType } from '../GenericMessageForm.types';

type RenderFieldPropsType = Pick<GenericMessageFormPropsType, 'formName'> & {
  classes: any;
  labels?: boolean;
};

export const renderField = ({
  classes,
  formName,
  labels,
}: RenderFieldPropsType) => {
  return function RenderFieldComponent({
    FieldComponent,
    name,
    error,
    touched,
    value,
    enablePreview = false,
    ...rest
  }: FormRendererFormField) {
    const id = unique();
    const props = {
      ...cloneWithout(rest, 'type', 'mode', 'classes'),
      compact: true,
      name,
      value,
      key: `${formName}-component-${name}`,
      scope: `${formName}-component-${name}`,
      id,
      inputId: id,
    };

    return (
      <div className={classnames(classes.formRow)} key={props.key}>
        {labels ? (
          <label className={classes.label} htmlFor={props.id}>
            {rest.label}
          </label>
        ) : null}
        <FieldComponent {...props} error={error} />
        <Render condition={Boolean(touched) && Boolean(error)}>
          <div role="alert" className={classes.error}>
            {error}
          </div>
        </Render>
      </div>
    );
  };
};

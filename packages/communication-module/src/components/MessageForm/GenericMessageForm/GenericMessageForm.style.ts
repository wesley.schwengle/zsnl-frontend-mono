// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useGenericMessageFormStyle = makeStyles(
  ({
    typography,
    palette: { primary, error },
    mintlab: { greyscale, shadows, radius },
  }: Theme) => ({
    tabsRoot: {
      marginBottom: '40px',
    },
    tabWrapper: {
      flexDirection: 'row',
      backgroundColor: primary.light,
      color: primary.main,
      borderRadius: '18px',
      padding: '6px',
    },
    tabLabelContainer: {
      padding: '0px',
    },
    tabIndicator: {
      display: 'none',
    },
    tabLabel: {
      color: primary.main,
    },
    colLabels: {
      width: '100px',
    },
    formWrapper: {
      width: '100%',
      border: `1px solid ${greyscale.dark}`,
      borderRadius: '10px',
      boxShadow: shadows.flat,
    },
    buttons: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      padding: '16px',
      '&>:first-child': {
        marginRight: '16px',
      },
    },
    formRow: {
      padding: '24px',
      borderBottom: `1px solid ${greyscale.dark}`,
    },
    label: {
      marginBottom: 10,
      display: 'block',
      ...typography.subtitle1,
    },
    formValue: {
      whiteSpace: 'pre-wrap',
    },
    previewHtml: {
      border: 'none',
      width: '100%',
    },
    error: {
      ...typography.caption,
      color: error.dark,
      marginTop: '8px',
      borderRadius: '0px',
    },
  })
);

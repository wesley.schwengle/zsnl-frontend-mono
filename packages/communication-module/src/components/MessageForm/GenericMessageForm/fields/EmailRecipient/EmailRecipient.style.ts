// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(
  ({ mintlab: { greyscale, radius } }: any) => ({
    row: {
      display: 'flex',
      flexDirection: 'row',
      paddingTop: 12,
      alignItems: 'center',
      '&:first-child': {
        paddingTop: 0,
      },
    },
    label: {
      flex: '0 1',
      whiteSpace: 'nowrap',
      paddingLeft: 14,
      minWidth: 150,
    },
    field: {
      flex: 1,
    },
    withBackground: {
      borderRadius: radius.defaultFormElement,
      backgroundColor: greyscale.light,
    },
  })
);

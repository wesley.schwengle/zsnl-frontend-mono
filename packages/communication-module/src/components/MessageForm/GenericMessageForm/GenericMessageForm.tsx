// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { ReactElement, useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import { addScopeProp } from '@mintlab/ui/App/library/addScope';
import CaseFinder from '@zaaksysteem/common/src/components/form/fields/CaseFinder/CaseFinder';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import {
  CASE_FINDER,
  CASE_SELECTOR,
  EMAIL_RECIPIENT,
} from '../../../library/communicationFieldTypes.constants';
import { resolveMagicStrings } from '../../../library/resolveMagicStrings';
import CaseSelector from './fields/CaseSelector/CaseSelector';
import { useGenericMessageFormStyle } from './GenericMessageForm.style';
import EmailRecipient from './fields/EmailRecipient/EmailRecipient';
import { GenericMessageFormPropsType } from './GenericMessageForm.types';
import { renderField } from './library/renderField';
import GenericMessageFormPreview from './GenericMessageFormPreview';

const typeMap = {
  [CASE_FINDER]: CaseFinder,
  [CASE_SELECTOR]: CaseSelector,
  [EMAIL_RECIPIENT]: EmailRecipient,
};

/* eslint complexity: [2, 7] */
function GenericAddForm({
  save,
  cancel,
  busy,
  formDefinition,
  emailTemplateData,
  formName,
  top,
  caseUuid,
  mapPreviewValues,
  enablePreview = false,
  primaryButtonLabelKey = 'forms.add',
  rules = [],
  labels = false,
}: GenericMessageFormPropsType): ReactElement {
  const [t] = useTranslation('communication');
  const [previewActive, setPreviewActive] = useState(false);
  const classes = useGenericMessageFormStyle();

  const {
    fields,
    formik: { values, isValid },
  } = useForm({
    t,
    formDefinition,
    fieldComponents: typeMap,
    isInitialValid: true,
    rules,
  });

  if (previewActive) {
    resolveMagicStrings(values, '');
  }

  return (
    <div className={classes.formWrapper}>
      {top}
      {previewActive ? (
        <GenericMessageFormPreview
          formName={formName}
          values={values}
          fields={fields}
          mapPreviewValues={mapPreviewValues}
          emailTemplateData={emailTemplateData}
          caseUuid={caseUuid || ''}
        />
      ) : (
        fields.map(renderField({ classes, formName, labels }))
      )}

      <div className={classes.buttons}>
        <Button
          disabled={!isValid || busy}
          action={() => {
            save(values);
          }}
          presets={['primary', 'contained']}
          {...addScopeProp(formName, 'save')}
        >
          {t(primaryButtonLabelKey)}
        </Button>
        {enablePreview && (
          <Button
            action={() => {
              setPreviewActive(!previewActive);
            }}
            presets={['text', 'contained']}
            {...addScopeProp(formName, 'preview')}
          >
            {previewActive ? t('forms.closePreview') : t('forms.openPreview')}
          </Button>
        )}
        <Button
          action={cancel}
          presets={['text', 'contained']}
          {...addScopeProp(formName, 'cancel')}
        >
          {t('forms.cancel')}
        </Button>
      </div>
    </div>
  );
}

export default GenericAddForm;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { Button } from '@mintlab/ui/App/Material/Button';
import { useSwitchViewButtonStyle } from './SwitchViewButton.style';

type SwitchViewButtonPropsType = {};

const SwitchViewButton: React.FunctionComponent<
  SwitchViewButtonPropsType
> = () => {
  const classes = useSwitchViewButtonStyle();
  const [t] = useTranslation('communication');
  const navigate = useNavigate();

  return (
    <div className={classes.wrapper}>
      <Button
        action={() => navigate('..')}
        presets={['default', 'medium']}
        icon="arrow_back"
      >
        {t('navigation.back')}
      </Button>
    </div>
  );
};

export default SwitchViewButton;

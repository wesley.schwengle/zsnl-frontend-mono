// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const test = require('tape');
const {
  asArray,
  removeFromArray,
  isPopulatedArray,
  toggleItem,
} = require('./array');

const MAGIC_ANSWER = 42;

test('asArray()', assert => {
  {
    const actual = asArray(MAGIC_ANSWER);
    const expected = [MAGIC_ANSWER];
    const message = 'wraps a value that is not an array in an array';

    assert.deepEqual(actual, expected, message);
  }
  {
    const expected = [MAGIC_ANSWER];
    const actual = asArray(expected);
    const message = 'passes through a value that is an array itself';

    assert.equal(actual, expected, message);
  }

  assert.end();
});

test('removeFromArray()', assert => {
  const actual = ['foo', 'bar', 'quux'];
  const expected = ['foo', 'quux'];
  const message = 'exports a `removeFromArray` function that';

  removeFromArray(actual, 'bar');

  assert.deepEqual(actual, expected, message);
  assert.end();
});

test('isPopulatedArray()', assert => {
  {
    const actual = isPopulatedArray([]);
    const expected = false;
    const message =
      'evaluates whether the value is an array and has at least one entry';

    assert.equal(actual, expected, message);
  }
  {
    const actual = isPopulatedArray(['a', 'b', 'c']);
    const expected = true;
    const message =
      'evaluates whether the value is an array and has at least one entry';

    assert.equal(actual, expected, message);
  }
  {
    const actual = isPopulatedArray(MAGIC_ANSWER);
    const expected = false;
    const message =
      'evaluates whether the value is an array and has at least one entry';

    assert.equal(actual, expected, message);
  }

  assert.end();
});

test('toggleItem()', assert => {
  {
    const actual = toggleItem(['foo', 'bar', 'quux'], 'bar');
    const expected = ['foo', 'quux'];
    const message =
      'returns array with the value removed when the value exist in the array';

    assert.deepEqual(actual, expected, message);
  }
  {
    const actual = toggleItem(['foo', 'bar', 'quux'], 'baz');
    const expected = ['foo', 'bar', 'quux', 'baz'];
    const message =
      'returns array with the value added when the value does not exist in the array';

    assert.deepEqual(actual, expected, message);
  }

  assert.end();
});

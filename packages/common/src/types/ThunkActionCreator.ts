// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ThunkAction } from 'redux-thunk';
import { ActionWithPayload } from './ActionWithPayload';

export type ThunkActionCreator<State, Payload, ExtraArgument = {}> = (
  payload: Payload
) => ThunkAction<void, State, ExtraArgument, ActionWithPayload<Payload>>;

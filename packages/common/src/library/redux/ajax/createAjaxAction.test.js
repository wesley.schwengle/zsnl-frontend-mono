// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '../../request/request';
import { createAjaxAction } from './createAjaxAction';
import { createAjaxConstants } from './createAjaxConstants';

jest.mock('../../request/request');

describe('The `createAjaxAction` redux-thunk action', () => {
  const constants = createAjaxConstants('TEST');
  const action = createAjaxAction(constants)({
    method: 'GET',
    url: '/',
  });

  afterEach(() => {
    request.mockReset();
  });

  describe('when successful', () => {
    test('should dispatch pending & success action', async () => {
      const dispatchMock = jest.fn();
      expect.assertions(3);
      request.mockResolvedValue({
        clone: jest.fn(),
        json: () => Promise.resolve(),
      });

      const bodyParser = await action(dispatchMock);
      await bodyParser;

      expect(dispatchMock.mock.calls).toHaveLength(2);
      expect(dispatchMock.mock.calls[0][0].type).toEqual(constants.PENDING);
      expect(dispatchMock.mock.calls[1][0].type).toEqual(constants.SUCCESS);
    });
  });

  describe('when failed', () => {
    test('should dispatch pending & error action', async () => {
      const dispatchMock = jest.fn();
      expect.assertions(3);

      request.mockRejectedValue({
        clone: jest.fn(),
        json: () => Promise.resolve(),
      });

      const bodyParser = await action(dispatchMock);
      await bodyParser;

      expect(dispatchMock.mock.calls).toHaveLength(2);
      expect(dispatchMock.mock.calls[0][0].type).toEqual(constants.PENDING);
      expect(dispatchMock.mock.calls[1][0].type).toEqual(constants.ERROR);
    });
  });
});

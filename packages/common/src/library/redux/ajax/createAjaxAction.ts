// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint-disable valid-jsdoc */
import { Dispatch } from 'redux';
import { request } from '../../../library/request/request';
import {
  ActionWithPayload,
  ActionPayloadWithResponse,
} from '../../../types/ActionWithPayload';
import { AjaxActionConstants } from './createAjaxConstants';

type CreateAjaxAction = (
  constants: AjaxActionConstants
) => <P, D = any>(data: {
  method: 'GET' | 'POST' | 'PUT' | 'DELETE';
  url: string;
  data?: D;
  payload?: P;
  type?: 'json' | 'formdata';
}) => (dispath: Dispatch<AjaxAction>) => Promise<any>;

export interface AjaxAction<R = {}, P = {}>
  extends ActionWithPayload<ActionPayloadWithResponse<P, R>> {
  ajax: boolean;
  error?: boolean;
  statusCode?: number;
}

export const createAjaxAction: CreateAjaxAction =
  constants =>
  ({ method, url, data, payload, type = 'json' }) =>
  dispatch => {
    dispatch({
      type: constants.PENDING,
      ajax: true,
      payload: {
        ...(payload as any),
      },
    });

    const merge = (body: any) => ({
      ajax: true,
      payload: {
        ...payload,
        response: body,
      },
    });

    return request(method, url, data, { type, cached: false })
      .then(response => {
        try {
          dispatch({
            ...merge(response),
            type: constants.SUCCESS,
          });
        } catch (error: any) {
          console.error(error);
        }
      })
      .catch(response => {
        dispatch({
          ...merge(response),
          type: constants.ERROR,
          error: true,
          statusCode: response.status,
        });
      });
  };

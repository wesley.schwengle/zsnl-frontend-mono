// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import {
  hideDialog,
  DialogActionPayload,
} from '../../store/ui/dialog/dialog.actions';
import useDialogs from '../../library/useDialogs';
import { UIRootStateType } from '../../store/ui/ui.reducer';

interface PropsFromState {
  dialogs: DialogActionPayload[];
}

interface PropsFromDispatch {
  closeAction: (dialogType: string) => void;
}

const DialogRenderer: React.FunctionComponent<
  PropsFromState & PropsFromDispatch
> = ({ dialogs, closeAction }) => {
  const [availableDialogs] = useDialogs();

  return (
    <React.Fragment>
      {dialogs
        .map<[DialogActionPayload, Function | undefined]>(dialog => [
          dialog,
          availableDialogs[dialog.dialogType],
        ])
        .map(([dialog, Component]) => {
          if (dialog && Component) {
            const { dialogType, ...restProps } = dialog;
            const closeDialog = () => closeAction(dialogType);

            return (
              <Component
                key={dialogType}
                onClose={closeDialog}
                {...restProps}
              />
            );
          }
        })}
    </React.Fragment>
  );
};

const mapStateToProps = ({
  ui: { dialogs },
}: UIRootStateType): PropsFromState => ({
  dialogs,
});

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatch => ({
  closeAction: (dialogType: string) => dispatch(hideDialog(dialogType)),
});

export default connect<PropsFromState, PropsFromDispatch, {}, UIRootStateType>(
  mapStateToProps,
  mapDispatchToProps
)(DialogRenderer);

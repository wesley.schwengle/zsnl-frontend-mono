// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classnames from 'classnames';
import { useTranslation } from 'react-i18next';
import { DocumentPreviewPropsType } from '../DocumentPreview.types';
import { useNativeStyles } from './Native.style';

export interface NativePropsType extends DocumentPreviewPropsType {
  className?: string;
}

export const Native: React.ComponentType<NativePropsType> = ({
  url,
  className,
}) => {
  const classes = useNativeStyles();
  const [t] = useTranslation('common');
  return (
    <iframe
      title={t('filePreview.iframeDescription')}
      className={classnames(classes.iframe, className && className)}
      src={url}
    />
  );
};

export default Native;

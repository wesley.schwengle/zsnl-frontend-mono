// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormDefinition } from '../../types/formDefinition.types';
import Rule from './Rule';

export function executeRules<Values = any>(
  rules: Rule[],
  formDefinition: FormDefinition<Values>
): FormDefinition<Values> {
  return rules.reduce(
    (accumulator, rule) => rule.execute(accumulator),
    formDefinition
  );
}

export default executeRules;

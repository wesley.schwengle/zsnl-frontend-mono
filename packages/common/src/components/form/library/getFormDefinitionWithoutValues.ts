// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  FormDefinition,
  AnyFormDefinitionField,
} from '../types/formDefinition.types';

function removeValuesFromFields<FormShape>(
  fields: AnyFormDefinitionField<FormShape>[]
): AnyFormDefinitionField<FormShape>[] {
  return fields.map(field => ({
    ...field,
    value: null,
  }));
}

export function getFormDefinitionWithoutValues<FormShape>(
  formDefinition: FormDefinition<FormShape>
): FormDefinition<FormShape> {
  return removeValuesFromFields(formDefinition);
}

export default getFormDefinitionWithoutValues;

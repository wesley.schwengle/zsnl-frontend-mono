// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormDefinition, FormValuesType } from '../types/formDefinition.types';

export default function getValuesFromDefinition<FormShape = any>(
  fields: FormDefinition<FormShape>
): FormShape {
  return fields.reduce<FormShape>(
    (values, field) => ({
      ...values,
      [field.name]: field.value,
    }),
    {} as FormValuesType<FormShape>
  );
}

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';

type CustomFieldsType = {
  [key: string]: any;
};

type FormatGetCaseCustomFields = (customFields: CustomFieldsType) => any;

// Relationship fields are output from the backend as an array of one, in that case
// we spread the first element as the value

/* eslint complexity: [2, 12] */
const formatGetCaseCustomFields: FormatGetCaseCustomFields = customFields =>
  Object.keys(customFields).reduce((acc, key) => {
    const value = customFields[key].value;

    const parsedValue =
      isPopulatedArray(value) && value[0]?.type === 'relationship'
        ? value[0]
        : value;

    if (parsedValue?.type === 'relationship') {
      return {
        ...acc,
        [key]: {
          ...parsedValue,
        },
      };
    } else {
      return {
        ...acc,
        [key]: {
          value: parsedValue,
        },
      };
    }
  }, {});

export default formatGetCaseCustomFields;

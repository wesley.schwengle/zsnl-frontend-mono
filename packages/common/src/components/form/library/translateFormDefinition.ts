// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormDefinition } from '../types/formDefinition.types';

/* eslint complexity: [2, 8] */
export const translateFormDefinition = (
  formDefinition: FormDefinition,
  t: i18next.TFunction
): FormDefinition => {
  return formDefinition.map(field => {
    const translateProperty = (propName: string) =>
      Object.hasOwnProperty.call(field, propName)
        ? {
            [propName]: t(field[propName] as any) || field[propName],
          }
        : {};

    return {
      ...field,
      ...translateProperty('label'),
      ...translateProperty('placeholder'),
      ...translateProperty('hint'),
      ...(field.config?.errorMessage
        ? {
            config: {
              ...field.config,
              errorMessage: t(field.config.errorMessage),
            },
          }
        : {}),
      ...(field.translations
        ? {
            translations: Object.fromEntries(
              Object.keys(field.translations).map(key => [
                key,
                t(field.translations[key]),
              ])
            ),
          }
        : {}),
      value:
        field.value && (field.value as any).label
          ? {
              value: (field.value as any).value,
              label: t((field.value as any).label),
            }
          : field.value,
      ...(field.choices
        ? {
            choices: field.choices.map(opt => ({
              ...opt,
              label: t(opt.label),
            })),
          }
        : {}),
      ...(field.config
        ? {
            ...field.config,
            ...(field.config.errorMessage
              ? { errorMessage: t(field.config.errorMessage) }
              : {}),
          }
        : {}),
    };
  });
};

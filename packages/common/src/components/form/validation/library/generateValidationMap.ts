// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { AnyFormDefinitionField } from '../../types';
import {
  EMAIL,
  WEB_ADDRESS,
  IBAN,
  NUMERIC,
  PHONE_NUMBER,
} from '../../constants/fieldTypes';
import {
  createEmailSchema,
  createIbanSchema,
  createHttpsLinkSchema,
  createNumberSchema,
  createPhoneNumberSchema,
} from '../createSchema';
import { ValidationMap } from '../createValidation';

const adaptCreateSchema =
  (createSchema: (field: any, t: i18next.TFunction) => any) =>
  ({ field, t }: { field: AnyFormDefinitionField; t: i18next.TFunction }) =>
    createSchema(field, t);

const validations = {
  [EMAIL]: adaptCreateSchema(createEmailSchema),
  [IBAN]: adaptCreateSchema(createIbanSchema),
  [WEB_ADDRESS]: adaptCreateSchema(createHttpsLinkSchema),
  [NUMERIC]: adaptCreateSchema(createNumberSchema),
  [PHONE_NUMBER]: adaptCreateSchema(createPhoneNumberSchema),
} as any;

export const generateValidationMap = (fields: AnyFormDefinitionField[]) => {
  const validationMap = {} as ValidationMap;
  fields.forEach(field => {
    if (validations[field.type]) {
      validationMap[field.name] = validations[field.type];
    }
  });

  return validationMap;
};

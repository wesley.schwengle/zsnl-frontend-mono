// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as yup from 'yup';
import * as i18next from 'i18next';
import iban from 'iban';
import { get } from '@mintlab/kitchen-sink/source';
import { AnyObject } from 'yup/lib/types';
import {
  FormDefinitionNumberField,
  AnyFormDefinitionField,
  FormDefinitionEmailField,
} from '../types/formDefinition.types';
import { STATUS_FAILED, STATUS_PENDING } from '../constants/fileStatus';
import { getCustomErrorMessages } from './library/getCustomErrorMessages';

/*
 * Matches emailaddresses and magic strings, for example:
 * - test@zaaksysteem.nl
 * - [[ behandelaar_email ]]
 */
const EMAIL_OR_MAGIC_STRING_REGEXP =
  /^\[\[(\s+)?([a-z0-9_]+)(\s+)?\]\]$|^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
const HTTPS_LINK_REGEXP =
  /(https:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/;

const PHONE_REG_EXP =
  /^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/;

export const createPhoneNumberSchema = (
  field: FormDefinitionNumberField,
  t: i18next.TFunction
) => {
  return yup
    .string()
    .nullable()
    .matches(PHONE_REG_EXP, {
      message: t('common:validations.custom.string.phoneNumber'),
      excludeEmptyString: true,
    });
};

export const createNumberSchema = (
  field: FormDefinitionNumberField,
  t: i18next.TFunction
) => {
  const setMin = (schema: yup.NumberSchema) =>
    field.min ? schema.min(field.min) : schema;
  const setMax = (schema: yup.NumberSchema) =>
    field.max ? schema.max(field.max) : schema;
  const setLessThan = (schema: yup.NumberSchema) =>
    field.lessThan ? schema.lessThan(field.lessThan) : schema;
  const setMoreThan = (schema: yup.NumberSchema) =>
    field.moreThan ? schema.moreThan(field.moreThan) : schema;

  const schema = yup.lazy(value => {
    if (value === '') {
      return yup.string();
    }

    return [
      yup.number().typeError(t('common:validations.custom.string.numeric')),
    ]
      .map(setMin)
      .map(setMax)
      .map(setLessThan)
      .map(setMoreThan)[0];
  });

  return schema;
};

export const createFileSchema = (t: i18next.TFunction) => {
  return yup
    .object()
    .shape({ key: yup.string() })
    .test(
      'file-valid',
      t('common:validations.custom.file.invalidFile'),
      value =>
        !(
          get(value, 'status') === STATUS_PENDING ||
          get(value, 'status') === STATUS_FAILED
        )
    )
    .nullable();
};

export const createTextSchema = (field: AnyFormDefinitionField) => {
  const [schema] = [yup.string().trim().nullable()]
    .map(schema => (field.min ? schema.min(field.min) : schema))
    .map(schema => (field.max ? schema.max(field.max) : schema));

  return schema;
};

export const createMixedSchema = () => {
  return yup.mixed();
};

export const createEmailSchema = (
  field: FormDefinitionEmailField,
  t: i18next.TFunction
) => {
  return field.allowMagicString
    ? yup
        .string()
        .nullable()
        .matches(EMAIL_OR_MAGIC_STRING_REGEXP, {
          message: t(
            'formRenderer:validations.custom.string.emailOrMagicString'
          ),
          excludeEmptyString: true,
        })
    : yup.string().nullable().email();
};

export const createIbanSchema = (
  field: FormDefinitionEmailField,
  t: i18next.TFunction
) => {
  return yup
    .string()
    .nullable()
    .test('iban', t('formRenderer:validations.custom.string.iban'), value =>
      value ? iban.isValid(value) : true
    );
};

export const createHttpsLinkSchema = (
  field: FormDefinitionEmailField,
  t: i18next.TFunction
) => {
  return yup
    .string()
    .nullable()
    .matches(HTTPS_LINK_REGEXP, {
      message: t('formRenderer:validations.custom.string.httpsLink'),
      excludeEmptyString: true,
    });
};

export function createArraySchema<Schema extends AnyObject, Values = any>(
  field: AnyFormDefinitionField<Values>,
  t: i18next.TFunction
) {
  const errorMessages = getCustomErrorMessages(field, ['min', 'max'], t);

  const [schema] = [yup.array<Schema>().nullable()]
    .map(schema =>
      field.minItems ? schema.min(field.minItems, errorMessages.min) : schema
    )
    .map(schema =>
      field.maxItems ? schema.max(field.maxItems, errorMessages.max) : schema
    );

  return schema;
}

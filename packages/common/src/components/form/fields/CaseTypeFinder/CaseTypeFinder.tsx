// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Switch from '@mintlab/ui/App/Material/Switch';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Select } from '@mintlab/ui/App/Zaaksysteem/Select';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { asArray } from '@mintlab/kitchen-sink/source';
import { FormFieldComponentType } from '../../types/Form2.types';
import {
  CaseTypeType,
  CaseTypeOptionType,
  fetchCaseType,
  fetchCaseTypeChoices,
  saveCaseTypeToRemember,
} from './CaseTypeFinder.library';
import { useStyles } from './CaseTypeFinder.styles';

export type CaseTypeValueType = {
  data: CaseTypeType;
  value: string;
  label: string;
};

export const CaseTypeFinder: FormFieldComponentType<
  CaseTypeValueType,
  {
    type: SubjectTypeType;
    prefillActive: boolean;
    supportPrefillOption?: boolean;
    multiValueLabelIcon: any;
  }
> = ({
  name,
  value,
  multiValue = false,
  config = {},
  includeOffline = false,
  ...restProps
}) => {
  const [input, setInput] = React.useState('');
  const [rememberCaseType, setRememberCaseType] = React.useState(
    Boolean(config.prefillActive)
  );
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useStyles();
  const [t] = useTranslation('');

  const onChange = async (event: React.ChangeEvent<any>) => {
    const value = event.target.value;

    if (value?.value && rememberCaseType) {
      saveCaseTypeToRemember(value?.value);
    }

    if (!value) {
      return restProps.onChange({
        ...event,
        target: {
          ...event.target,
          value,
        },
      });
    }

    const selectValue = asArray(value);

    const fetchFullCaseType = async (option: CaseTypeOptionType) => {
      if (option.fetched === false) {
        return await fetchCaseType(option.data.id, openServerErrorDialog).then(
          (fullCaseType: CaseTypeType) => ({
            data: fullCaseType,
            value: option.data.id,
            label: fullCaseType.name,
          })
        );
      }
      return option;
    };

    const results = await Promise.all(
      selectValue.map(async option => fetchFullCaseType(option))
    );

    restProps.onChange({
      ...event,
      target: {
        ...event.target,
        value: multiValue ? results : results[0],
      },
    });
  };

  const onRememberCaseTypeChange = (event: any) => {
    const checked = event.target.checked;

    if (checked) {
      saveCaseTypeToRemember(value?.value);
    } else {
      saveCaseTypeToRemember();
    }

    setRememberCaseType(checked);
  };

  return (
    <div className={classes.wrapper}>
      {config.supportPrefillOption && (
        <div className={classes.switchWrapper}>
          <span>{t('caseCreate:caseType.remember')}:</span>
          <Switch
            checked={rememberCaseType}
            onChange={onRememberCaseTypeChange}
          />
        </div>
      )}
      <DataProvider
        providerArguments={[input]}
        autoProvide={input !== ''}
        provider={fetchCaseTypeChoices(
          openServerErrorDialog,
          includeOffline,
          config?.type
        )}
      >
        {({ data, busy }) => {
          return (
            <Select
              {...restProps}
              placeholder={t('common:forms.selectEntity', {
                entity: t('common:entityType.case_type'),
              })}
              value={value}
              name={name}
              choices={data || []}
              isClearable={true}
              getChoices={setInput}
              onChange={onChange}
              isSearchable={true}
              loading={busy}
              filterOption={option => Boolean(option)}
              isMulti={multiValue}
              multiValueLabelIcon={config?.multiValueLabelIcon}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </div>
  );
};

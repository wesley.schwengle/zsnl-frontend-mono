// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { fetchRoleChoices } from './RoleFinder.library';

type RoleFinderPropsType = {
  value: ValueType<string>;
  name: string;
  config?: any;
  [key: string]: any;
};

const RoleFinder: FunctionComponent<RoleFinderPropsType> = ({
  value,
  name,
  config,
  ...restProps
}) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const parentRoleUuid = config?.parentRoleUuid;

  return (
    <React.Fragment>
      <DataProvider
        autoProvide={Boolean(parentRoleUuid)}
        provider={fetchRoleChoices(openServerErrorDialog)}
        providerArguments={[parentRoleUuid]}
      >
        {({ data, busy, provide }) => {
          const normalizedChoices = data || [];
          const disabled =
            restProps?.disabled === true ||
            !isPopulatedArray(normalizedChoices);

          return (
            <FlatValueSelect
              {...restProps}
              name={name}
              choices={normalizedChoices}
              value={value}
              isClearable={true}
              loading={busy}
              getChoices={provide}
              isDisabled={disabled}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default RoleFinder;

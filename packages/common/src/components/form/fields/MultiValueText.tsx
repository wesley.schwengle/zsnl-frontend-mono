// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

/*
 * Wraps Select and configures it as multi value textfield
 */
const MultiValueText: FormFieldComponentType<
  ValueType<string>[],
  any
> = props => {
  const {
    multiValue,
    createOnBlur = true,
    value,
    readOnly,
    config,
    ...rest
  } = props;

  return readOnly ? (
    <ReadonlyValuesContainer value={(value || []).map(val => val.label)} />
  ) : (
    <Select
      {...rest}
      value={value}
      isMulti={multiValue}
      isClearable={true}
      createOnBlur={createOnBlur}
      creatable={true}
      formatCreateLabel={(input: string) => input}
      multiValueLabelIcon={config?.multiValueLabelIcon}
    />
  );
};

export default MultiValueText;

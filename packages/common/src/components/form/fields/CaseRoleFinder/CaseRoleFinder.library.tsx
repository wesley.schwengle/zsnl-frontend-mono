// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  CaseRoleFinderConfigType,
  RoleAndSubjectType,
} from './CaseRoleFinder.types';

export const fetchCaseRoleChoices =
  (onError: OpenServerErrorDialogType) =>
  async (config: CaseRoleFinderConfigType) => {
    const body =
      await request<APICaseManagement.GetSubjectRelationsResponseBody>(
        'GET',
        buildUrl<APICaseManagement.GetSubjectRelationsRequestParams>(
          '/api/v2/cm/case/get_subject_relations',
          {
            case_uuid: config.caseUuid,
            include: ['subject'],
          }
        )
      ).catch(onError);

    return body
      ? body?.data
          .map(role => ({
            role,
            subject:
              body.included &&
              body.included.filter(subject => {
                return (
                  role.relationships.subject &&
                  subject.id === role.relationships.subject.data.id
                );
              })[0],
          }))
          .sort((roleA, roleB) =>
            roleA.role.attributes.role < roleB.role.attributes.role ? -1 : 1
          )
          .filter(config.itemFilter || (() => true))
          .map(caseRole => {
            return {
              value: config.valueResolver
                ? config.valueResolver(caseRole)
                : caseRole.role.id,
              subLabel: config.subLabelResolver
                ? config.subLabelResolver(caseRole)
                : caseRole.subject?.attributes?.contact_information.email,
              label: createDisplayName(caseRole),
              id: caseRole.subject?.id || '',
              icon: (
                <ZsIcon size="tiny">
                  {caseRole.subject && caseRole.subject.type === 'organization'
                    ? 'entityType.organization'
                    : 'entityType.person'}
                </ZsIcon>
              ),
            };
          })
          .filter(item => Boolean(item.value) && Boolean(item.label))
      : [];
  };

export const isNotPresetRequestor = (item: RoleAndSubjectType) =>
  item.role.attributes.is_preset_client === false;

export const createDisplayName = ({
  role,
  subject,
}: RoleAndSubjectType): string => {
  if (subject && subject.attributes) {
    if (subject.attributes.surname) {
      const { surname, surname_prefix, first_names, first_name, noble_title } =
        subject.attributes;
      const name = [
        noble_title,
        first_names,
        first_name,
        surname_prefix,
        surname,
      ]
        .filter(item => Boolean(item))
        .join(' ');

      return `${name} (${role.attributes.role})`;
    } else if (subject.attributes.name) {
      return `${subject.attributes.name} (${role.attributes.role})`;
    }
  }

  return role.attributes.role;
};

export const createDisplayNameWithEmail = ({
  role,
  subject,
}: RoleAndSubjectType): string => {
  const email = subject?.attributes?.contact_information.email;
  let subjectName: string | null = null;

  if (subject?.attributes?.surname) {
    const { surname, surname_prefix, first_names, first_name, noble_title } =
      subject.attributes;
    subjectName = [
      noble_title,
      first_names,
      first_name,
      surname_prefix,
      surname,
    ]
      .filter(item => Boolean(item))
      .join(' ');
  } else if (subject?.attributes) {
    subjectName = subject.attributes.name;
  }

  const mainName = role.attributes.role;
  const formattedSubjectName = subjectName && `(${subjectName})`;
  const formattedEmail = email && `[${email}]`;

  return [mainName, formattedSubjectName, formattedEmail]
    .filter(Boolean)
    .join(' ');
};

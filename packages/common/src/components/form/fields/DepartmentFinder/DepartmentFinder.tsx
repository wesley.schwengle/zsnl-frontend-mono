// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { useTheme } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';
import { createSharedStyleSheet } from '@mintlab/ui/App/Zaaksysteem/Select/Form/Shared.style';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { fetchDepartmentChoices } from './DepartmentFinder.library';
import { DepartmentFinderOptionType } from './DepartmentFinder.types';

type DepartmentFinderPropsType = {
  value: DepartmentFinderOptionType;
  multiValue?: any;
  styles?: any;
  config?: any;
  [key: string]: any;
};

export const DepartmentFinder: FunctionComponent<DepartmentFinderPropsType> = ({
  value,
  multiValue = false,
  styles,
  config = {},
  ...restProps
}: any) => {
  const theme = useTheme<Theme>();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const { error } = restProps;
  const hideSpan = {
    '& span': {
      display: 'none',
    },
  };
  const stylesProp = {
    ...createSharedStyleSheet({
      theme,
      error,
    }),
    singleValue(base: any) {
      return {
        ...base,
        ...hideSpan,
      };
    },
    multiValueLabel(base: any) {
      return {
        ...base,
        ...hideSpan,
      };
    },
    ...((styles && styles) || {}),
  };
  return (
    <React.Fragment>
      <DataProvider
        autoProvide={true}
        provider={fetchDepartmentChoices(openServerErrorDialog)}
        providerArguments={[]}
      >
        {({ data, busy, provide }) => {
          const normalizedChoices = data || [];
          const disabled =
            restProps?.disabled === true ||
            !isPopulatedArray(normalizedChoices);

          return (
            <Select
              {...restProps}
              choices={normalizedChoices}
              value={value}
              isClearable={true}
              loading={busy}
              getChoices={provide}
              disabled={disabled}
              styles={stylesProp}
              isMulti={multiValue}
              multiValueLabelIcon={config?.multiValueLabelIcon}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default DepartmentFinder;

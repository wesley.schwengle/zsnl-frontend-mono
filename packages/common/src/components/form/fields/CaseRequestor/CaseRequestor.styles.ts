// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    padding: 14,
  },
  error: {
    color: 'red',
  },
}));

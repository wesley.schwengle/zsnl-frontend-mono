// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import { FormFieldComponentType } from '../../types/Form2.types';
import { ReadonlyValuesContainer } from '../../library/ReadonlyValuesContainer';
import { useCheckboxGroupStyles } from './CheckboxGroup.style';

type CheckboxValue = string | number;

export const CheckboxGroup: FormFieldComponentType<CheckboxValue[]> = ({
  value,
  name,
  onChange,
  choices,
  readOnly,
}) => {
  const classes = useCheckboxGroupStyles();
  const updateValue = (value: CheckboxValue[] | null) =>
    onChange({ target: { name, value } });

  const selectCheckbox = (checkboxValue: CheckboxValue) =>
    updateValue([...(value || []), checkboxValue]);

  const deselectCheckbox = (checkboxValue: CheckboxValue) =>
    updateValue((value || []).filter(item => item != checkboxValue));

  const isCheckboxSelected = (checkboxValue: CheckboxValue) =>
    (value || []).includes(checkboxValue);

  const getValueByName = (name: string): CheckboxValue =>
    choices
      ? choices.find(({ value }) => name === value.toString()).value
      : null;

  const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name } = event.target;
    const value = getValueByName(name);

    return isCheckboxSelected(value)
      ? deselectCheckbox(value)
      : selectCheckbox(value);
  };

  const getAllSelectedLabels = () => {
    if (!value) {
      return;
    }

    return choices
      ?.filter(choice => value.includes(choice.value))
      .map(choice => choice.label);
  };

  return readOnly ? (
    <ReadonlyValuesContainer value={getAllSelectedLabels()} />
  ) : (
    <ul className={classes.list}>
      {choices &&
        choices.map(({ label, value }) => (
          <li key={value}>
            <Checkbox
              label={label}
              name={value.toString()}
              onChange={handleCheckboxChange}
              checked={isCheckboxSelected(value)}
            />
          </li>
        ))}
    </ul>
  );
};

export default CheckboxGroup;

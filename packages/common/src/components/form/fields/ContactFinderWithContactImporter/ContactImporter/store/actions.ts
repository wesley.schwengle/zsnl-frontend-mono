// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import {
  InterfaceType,
  FormType,
  StoreShapeType,
  AnyRecordType,
} from '../types';

export const setLoadingAction = (loading: boolean) => {
  return {
    type: 'setLoading',
    payload: loading,
  };
};

export const setSessionData = (data: any) => {
  return {
    type: 'setSessionData',
    payload: data.result.instance,
  };
};

export const setModulesConfig = (data: any) => {
  return {
    type: 'setModulesConfig',
    payload: data.result.instance.rows,
  };
};

export const setFormDefinition = (searchType: FormType) => {
  return {
    type: 'setFormDefinition',
    payload: searchType,
  };
};

export const setSearchResultsAction = (
  rows: AnyRecordType[],
  type: FormType
) => {
  return {
    type: 'setSearchResults',
    payload: {
      rows,
      type,
    },
  };
};

export const clearSearchResultsAction = () => {
  return {
    type: 'clearSearchResults',
    payload: null,
  };
};

export const setFormTypeAction = (type: FormType) => {
  return {
    type: 'setFormType',
    payload: type,
  };
};

export const initDefaults = () => {
  return {
    type: 'initDefaults',
    payload: null,
  };
};

export const setSelectedInterfaceAction = (interf: InterfaceType['id']) => {
  return {
    type: 'setSelectedInterface',
    payload: interf,
  };
};

export const setImportReferenceAction = (
  uuid: StoreShapeType['importReference']
) => {
  return {
    type: 'setImportReference',
    payload: uuid,
  };
};

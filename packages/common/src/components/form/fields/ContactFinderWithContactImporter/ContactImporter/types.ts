// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';

export type InterfaceType = {
  uuid: string;
  id: number;
  name: string;
  config: {
    [key: string]: string;
  };
};

export type StoreShapeType = {
  interfaces: InterfaceType[];
  capabilities: CapabilitiesType;
  allowedFormTypes: String[];
  initializing: boolean;
  loading: boolean;
  subjectTypes: SubjectTypeType[];
  selectedFormType: FormType;
  selectedInterface: number | 'local';
  rows: AnyRecordType[] | null;
  importReference: string | null;
  importing: boolean;
  externalSearchAllowed: boolean;
  activeInterfaces: string[];
};

export type CapabilitiesType = String[];

export type FormType = 'person' | 'company';

export type PersonTableRowType = {
  uuid: string;
  name: string;
  dateOfBirth: Date | String;
  address: string;
  gender: string;
  hasCorrespondenceAddress: boolean;
  isDeceased: boolean;
  isSecret: boolean;
  isInResearch: boolean;
  type: 'person';
};

export type CompanyTableRowType = {
  uuid: string;
  name: string;
  cocNumber: string;
  cocLocationNumber: string;
  address: string;
  type: 'organization';
};

export type AnyTableRowType = PersonTableRowType | CompanyTableRowType;
export type RowDataType = { rowData: AnyTableRowType; [key: string]: any };

export type PersonRecordType = {
  uuid: string;

  [key: string]: any;
  instance: {
    instance_type: 'person';
    [key: string]: any;
  };
};
export type CompanyRecordType = {
  uuid: string;

  [key: string]: any;
  instance: {
    instance_type: 'company';
    [key: string]: any;
  };
};

export type AnyRecordType = PersonRecordType | CompanyRecordType;

export type SelectRowType = ({ rowData }: any) => void;

export type PersonFields = {
  bsn: string;
  dateOfBirth: Date;
  familyName: string;
  prefix: string;
  zipCode: string;
  streetNumber: string;
  streetNumberLetter: string;
  suffix: string;
};

export type CompanyFields = {
  rsin: string;
  cocNumber: string;
  cocLocationNumber: string;
  company: string;
  street: string;
  zipCode: string;
  number: string;
  letter: string;
  suffix: string;
};

export type AnyTypeFields = PersonFields | CompanyFields;

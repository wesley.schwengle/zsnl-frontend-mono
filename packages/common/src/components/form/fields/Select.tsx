// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import SelectCmp from '@mintlab/ui/App/Zaaksysteem/Select';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

/* eslint complexity: [2, 7] */
export const Select: FormFieldComponentType<any> = props => {
  const { value, choices, readOnly } = props;

  if (readOnly) {
    let readOnlyLabel;

    if (value?.label) {
      readOnlyLabel = value.label;
    } else if (!value) {
      readOnlyLabel = value;
    } else if (Array.isArray(value)) {
      readOnlyLabel = value.join(',') || '-';
    } else {
      const selectedChoice = choices?.find(choice => choice.value === value);

      readOnlyLabel = selectedChoice.label;
    }

    return <ReadonlyValuesContainer value={readOnlyLabel} />;
  } else {
    return <SelectCmp {...props} value={value || null} />;
  }
};

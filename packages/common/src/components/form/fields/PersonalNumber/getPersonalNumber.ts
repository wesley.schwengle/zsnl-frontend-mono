// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';

export const getPersonalNumber = async (
  setValue: (personalNumber: string) => void,
  uuid: string
) => {
  const sensitiveData = await request(
    'GET',
    buildUrl<APICaseManagement.GetPersonSensitiveDataRequestParams>(
      '/api/v2/cm/contact/get_person_sensitive_data',
      {
        uuid,
      }
    )
  );

  const personalNumber = sensitiveData.data.attributes.personal_number;

  // FE lets BE know that BE sent the BSN to FE
  // this blip in logic will be fixed in the future
  const response = await request(
    'POST',
    buildUrl<APICaseManagement.CreateLogForBsnRetrievedRequestParams>(
      '/api/v2/cm/subject/create_bsn_retrieved_log_entry',
      {
        subject_uuid: uuid,
      }
    )
  );

  if (response.data) {
    setValue(personalNumber);
  }
};

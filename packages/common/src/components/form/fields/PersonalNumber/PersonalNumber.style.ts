// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const usePersonNumberStylesheet = makeStyles(() => ({
  wrapper: {
    display: 'flex',
  },
  button: {
    marginLeft: 20,
  },
}));

export type OptionClassesType = ReturnType<typeof usePersonNumberStylesheet>;

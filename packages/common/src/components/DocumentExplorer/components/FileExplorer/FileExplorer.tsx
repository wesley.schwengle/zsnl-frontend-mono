// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { isDocument } from '../DocumentExplorer/utils/isDocument';
import {
  FileExplorerPropsType,
  RowClickEvent,
} from './types/FileExplorerTypes';

const FileExplorer: React.ComponentType<FileExplorerPropsType> = ({
  items,
  columns,
  onFileOpen,
  onFolderOpen,
  onRowClick,
  loading = false,
  noRowsMessage,
  ...rest
}) => {
  const handleRowDoubleClick = ({ rowData }: RowClickEvent) => {
    if (isDocument(rowData)) {
      onFileOpen(rowData);
    } else {
      onFolderOpen(rowData);
    }
  };

  return (
    <SortableTable
      rows={items}
      columns={columns}
      onRowDoubleClick={handleRowDoubleClick}
      onRowClick={onRowClick}
      loading={loading}
      noRowsMessage={noRowsMessage}
      {...rest}
    />
  );
};

export default FileExplorer;

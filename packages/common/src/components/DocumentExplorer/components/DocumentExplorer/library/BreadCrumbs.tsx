// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
//@ts-ignore
import Breadcrumbs from '@mintlab/ui/App/Material/Breadcrumbs';
import {
  PresetLocations,
  StoreShapeType,
  HandleItemClickType,
  ClassesType,
} from '../types/types';

type BreadCrumbsPropsType = Pick<StoreShapeType, 'path' | 'location'> & {
  handleItemClick: HandleItemClickType;
} & {
  classes: ClassesType;
};

const BreadCrumbs: FunctionComponent<BreadCrumbsPropsType> = ({
  path,
  location,
  handleItemClick,
  classes,
}) => {
  const currentIndex = path.findIndex(element => element.id === location);
  const prevIndex = currentIndex > 0 ? currentIndex - 1 : currentIndex;

  return (
    <div className={classes.breadCrumbs}>
      {location !== PresetLocations.Home ? (
        <IconButton
          onClick={event => handleItemClick(event, path[prevIndex])}
          color="inherit"
          style={{ marginRight: 10, marginLeft: -16 }}
        >
          <Icon size="medium">{iconNames.arrow_back}</Icon>
        </IconButton>
      ) : null}

      <Breadcrumbs maxItems={4} items={path} onItemClick={handleItemClick} />
    </div>
  );
};

export default BreadCrumbs;

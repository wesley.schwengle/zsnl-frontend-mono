// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    on: 'op',
    today: 'Vandaag',
    search: 'Zoeken',
    till: 'tot',
    start: 'Begin',
    now: 'Nu',
    filtersLabel: 'Timeline filters',
    exportLabel: 'Exporteren',
    exported:
      'De tijdlijn wordt op de achtergrond geëxporteerd en is later beschikbaar onder Exportbestanden.',
    eventType: {
      'auth/alternative': 'Alternatieve authenticatie',
      'case/checklist/item/create': 'Checklistitem aangemaakt',
      'case/checklist/item/update': 'Checklistitem gewijzigd',
      'case/checklist/item/remove': 'Checklistitem verwijderd',
      'case/close': 'Zaak afgehandeld',
      'case/create': 'Zaak geregisteerd',
      'case/reopen': 'Zaak heropend',
      'case/duplicated': 'Zaak gekopieerd',
      'case/document/assign': 'Document toegewezen',
      'case/document/create': 'Document aangemaakt',
      'case/document/revert': 'Document gewijzigd',
      'case/document/label': 'Documentgegevens gewijzigd',
      'case/document/metadata/update': 'Documentgegevens gewijzigd',
      'case/document/properties/update': 'Documentgegevens gewijzigd',
      'case/document/publish': 'Documentpublicatie',
      'case/document/reject': 'Document geweigerd',
      'case/document/rename': 'Document hernoemd',
      'case/document/trash': 'Document verwijderd',
      'case/document/unpublish': 'Documentpublicatie',
      'case/document/accept': 'Document geaccepteerd',
      'case/document/remove': 'Document verwijderd',
      'case/document/delete_document': 'Document permanent verwijderd',
      'case/document/replace': 'Document vervangen',
      'case/document/restore': 'Document hersteld',
      'case/document/copy': 'Document gekopieerd',
      'case/document/sign': 'Document ondertekend',
      'case/document/copy_case_into': 'Document gekopieerd',
      'case/document/copy_case_from': 'Document gekopieerd',
      'case/note/create': 'Notitie aangemaakt',
      'case/note/created': 'Notitie aangemaakt',
      'case/contact_moment/created': 'Contactmoment aangemaakt',
      'case/email/created': 'E-mail toegevoegd',
      'case/pip/feedback': 'PIP-bericht',
      'case/pip/reject_update': 'Wijzigingsvoorstel geweigerd',
      'case/pip/updatefield': 'Wijzigingsvoorstel ingediend',
      'case/update/piprequest': 'Wijzigingsvoorstel verwerkt',
      'case/relation/remove': 'Relatie verwijderd',
      'case/suspend': 'Zaak opgeschort',
      'case/resume': 'Zaak hervat',
      'case/task/new_assignee': 'Taak toegewezen',
      'case/task/set_completion': 'Taak streefdatum gewijzigd',
      'case/task/created': 'Taak aangemaakt',
      'case/task/updated': 'Taak gewijzigd',
      'case/task/deleted': 'Taak verwijderd',
      'case/update/purge_date': 'Zaaktermijn gewijzigd',
      'case/update/registration_date': 'Zaaktermijn gewijzigd',
      'case/update/status': 'Zaakstatus gewijzigd',
      'case/update/subject': 'Zaakrelatie gewijzigd',
      'case/update/completion_date': 'Afhandeldatum gewijzigd',
      'case/update/case_type': 'Zaaktype gewijzigd',
      'case/update/confidentiality': 'Vertrouwelijkheid gewijzigd',
      'case/update/department': 'Afdeling gewijzigd',
      'case/update/field': 'Kenmerk gewijzigd',
      'case/update/milestone': 'Mijlpaal behaald',
      'case/update/relation': 'Relatie gewijzigd',
      'case/update/requestor': 'Aanvrager gewijzigd',
      'case/object/create': 'Object aangemaakt',
      'case/object/relate': 'Object gerelateerd',
      'case/object/delete': 'Object verwijderd',
      'case/subject/add': 'Betrokkene toegevoegd',
      'case/update/target_date': 'Zaaktermijn gewijzigd',
      'case/update/allocation': 'Zaak toegewezen',
      'case/update/result': 'Zaakresultaat gewijzigd',
      'case/update/settle_date': 'Streefafhandeldatum gewijzigd',
      'case/attribute/create': 'Kenmerk aangemaakt',
      'case/attribute/update': 'Kenmerk gewijzigd',
      'case/attribute/remove': 'Kenmerk verwijderd',
      'case/relation/update': 'Zaakrelatie gewijzigd',
      'case/subcase': 'Deelzaak aangemaakt',
      'case/subject/update': 'Betrokkene gewijzigd',
      'case/subject/remove': 'Betrokkene verwijderd',
      'case/view': 'Zaak opgevraagd',
      'case/payment/status': 'Betaalstatus gewijzigd',
      'case/send_external_system_message': 'Extern bericht verstuurd',
      'case/early_settle': 'Zaak vroegtijdig afgehandeld',
      'case/publish': 'Zaak gepubliceerd',
      'document/assign': 'Document toegewezen',
      'document/create': 'Document aangemaakt',
      'case/accept': 'Zaak in behandeling genomen',
      'document/delete_document': 'Document verwijderd',
      'document/assignment/reject': 'Document geweigerd',
      'document/metadata/update': 'Documentgegevens gewijzigd',
      'email/send': 'E-mail verzonden',
      'object/view': 'Object bekeken',
      'object/queue/item_failed': 'Zaakactie mislukt',
      'subject/contactmoment/create': 'Contactmoment aangemaakt',
      'subject/create': 'Contact aangemaakt',
      'subject/inspect': 'BSN ingezien',
      'subject/note/create': 'Notitie aangemaakt',
      'subject/update': 'Contact gewijzigd',
      'subject/update_contact_data': 'Contactgegevens gewijzigd',
      'custom_object/updated': 'Object aangepast',
      'custom_object/created': 'Object aangemaakt',
      'custom_object/deleted': 'Object verwijderd',
      'custom_object/relatedTo': 'Object gerelateerd',
      'custom_object/unrelatedFrom': 'Objectrelatie verwijderd',
      'auth/alternative/username': 'Gebruikersnaam gewijzigd',
      'auth/alternative/activate': 'Account geactiveerd/gedeactiveerd',
      'auth/alternative/mail': 'Activatie-e-mail verstuurd',
      'auth/alternative/account': 'Account aangemaakt',
    },
  },
};

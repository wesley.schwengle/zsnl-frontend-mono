// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = (minHeight: number) =>
  makeStyles(() => ({
    dialogRoot: {
      width: 500,
      minHeight,
      padding: 0,
    },
  }));

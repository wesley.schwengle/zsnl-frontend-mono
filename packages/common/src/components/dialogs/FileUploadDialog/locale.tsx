// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    title: {
      single: 'Voeg bestand toe',
      multiple: 'Voeg bestanden toe',
    },
    save: 'Toevoegen',
    label: {
      single: 'Upload een bestand',
      multiple: 'Upload een of meerdere bestanden',
    },
  },
};

export default locale;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { addScopeProp } from '@mintlab/ui/App/library/addScope';
import Button from '@mintlab/ui/App/Material/Button';

/**
 * @param {Object} options
 * @param {Array} options.primaryPresets
 * @param {Array} options.secondaryPresets
 * @param {Array} [options.tertiaryPresets]
 * @returns {Function}
 */
export const createDialogActions =
  ({ primaryPresets, secondaryPresets, tertiaryPresets }) =>
  (
    primaryButton = {},
    secondaryButton = {},
    tertiaryButton = {},
    rootScope = ''
  ) => {
    const buttons = [
      { ...tertiaryButton, presets: tertiaryPresets },
      { ...secondaryButton, presets: secondaryPresets },
      { ...primaryButton, presets: primaryPresets },
    ];

    return buttons
      .filter(button => button.text)
      .map(button => ({
        ...addScopeProp(rootScope, button.text),
        ...button,
      }))
      .map(({ onClick, presets, text, scope, ...rest }, index) => (
        <Button
          key={index}
          action={onClick}
          presets={presets}
          {...addScopeProp(scope, text)}
          {...rest}
        >
          {text}
        </Button>
      ));
  };

export default createDialogActions;

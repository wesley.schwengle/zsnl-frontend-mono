// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useErrorDialog } from '@zaaksysteem/common/src/hooks/useErrorDialog';
import { TranslatedErrorMessage } from '@zaaksysteem/common/src/library/request/TranslatedErrorMessage';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';

export type OpenServerErrorDialogType = (
  errorObj: V2ServerErrorsType
) => undefined;
export type ReturnType = [React.ReactNode, OpenServerErrorDialogType];

const useServerErrorDialog = (): ReturnType => {
  const [errorDialog, openErrorDialog] = useErrorDialog();

  const openServerErrorDialog = (errorObj: V2ServerErrorsType) => {
    openErrorDialog({
      message: <TranslatedErrorMessage errorObj={errorObj} />,
    });

    return undefined;
  };

  return [errorDialog, openServerErrorDialog];
};

export default useServerErrorDialog;

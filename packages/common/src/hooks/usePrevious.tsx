// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useEffect, useRef } from 'react';

export const usePrevious = <T extends unknown>(value: T): T | undefined => {
  const ref = useRef<T>();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

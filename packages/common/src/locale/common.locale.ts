// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    systemName: 'Zaaksysteem',
    currency: 'euro',
    yes: 'Ja',
    no: 'Nee',
    of: 'van',
    none: 'Geen',
    or: 'of',
    and: 'en',
    verbs: {
      view: 'Bekijken',
      create: 'Aanmaken',
      save: 'Opslaan',
      edit: 'Bewerken',
      update: 'Bewerken',
      delete: 'Verwijderen',
      activate: 'Activeren',
      deactivate: 'Deactiveren',
      activated: 'Geactiveerd',
      deactivated: 'Gedeactiveerd',
    },
    terms: {
      day: 'Dag',
      days: 'Dagen',
      workDay: 'Werkdag',
      workDays: 'Werkdagen',
      calendarDay: 'Kalenderdag',
      calendarDays: 'Kalenderdagen',
      week: 'Week',
      weeks: 'Weken',
      month: 'Maand',
      months: 'Maanden',
      year: 'Jaar',
      years: 'Jaren',
    },
    dates: {
      dayNamesShort: ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
      dayNames: [
        'Zondag',
        'Maandag',
        'Dinsdag',
        'Woensdag',
        'Donderdag',
        'Vrijdag',
        'Zaterdag',
      ],
      monthNamesShort: [
        'jan',
        'feb',
        'mrt',
        'apr',
        'mei',
        'jun',
        'jul',
        'aug',
        'sep',
        'okt',
        'nov',
        'dec',
      ],
      monthNames: [
        'januari',
        'februari',
        'maart',
        'april',
        'mei',
        'juni',
        'juli',
        'augustus',
        'september',
        'oktober',
        'november',
        'december',
      ],
      DoFn: (month: string): string => month,
      dateFormatText: 'DD MMMM YYYY',
      dateFormatTextShort: 'D MMM YYYY',
      dateFormat: 'DD-MM-YYYY',
      timeFormat: 'HH:mm',
      timeFormatFull: 'HH:mm:ss',
    },
    general: {
      loading: 'Bezig met laden…',
      noResults: 'Er zijn geen resultaten om weer te geven.',
      openInNewWindow: 'Open in nieuw venster',
    },
    case: {
      status: {
        new: 'Nieuw',
        open: 'In behandeling',
        stalled: 'Opgeschort',
        resolved: 'Afgehandeld',
      },
      role: {
        Aanvrager: 'Aanvrager',
        Behandelaar: 'Behandelaar',
        Coordinator: 'Coördinator',
        Ontvanger: 'Ontvanger',
      },
    },
    filePreview: {
      previousPage: 'Vorige pagina',
      nextPage: 'Volgende pagina',
      zoomIn: 'Inzoomen',
      zoomOut: 'Uitzoomen',
      rotateRight: 'Kloksgewijs 90 graden draaien',
      iframeDescription: 'Frame voor PDF weergave',
      downloadDocument: 'Document downloaden',
    },
    forms: {
      selectEntity: '{{entity}} selecteren…',
      add: 'Toevoegen',
      ok: 'OK',
      cancel: 'Annuleren',
      delete: 'Verwijderen',
      choose: 'Maak een keuze…',
      checkboxReadonlyChecked: 'Ja',
      view: 'Bekijken',
      proceed: 'Verder',
      save: 'Opslaan',
    },
    snack: {
      close: 'Sluiten',
    },
    dialog: {
      error: {
        title: 'Fout',
        details: 'Details',
        body: 'Er is een fout op de server opgetreden. Probeer het later opnieuw.',
      },
      ok: 'OK',
      cancel: 'Annuleren',
      save: 'Opslaan',
      close: 'Sluiten',
      search: 'Zoeken',
    },
    noAccess: 'Geen toegang',
    serverErrors: {
      status: {
        '401': 'U bent niet geauthoriseerd om deze actie uit te voeren.',
        '403': 'U heeft onvoldoende rechten om deze actie uit te voeren.',
        '404':
          'De opgevraagde pagina of gegevensbron kon niet worden gevonden. Probeer het later opnieuw.',
      },
      'invalid/syntax':
        'Uit het antwoord van de server kon geen geldige JSON worden opgebouwd.',
      'invalid/file': 'Dit bestand wordt niet geaccepteerd.',
      keys: {
        'file/type_not_allowed':
          'Bestanden van dit bestandstype zijn niet toegestaan.',
        'file/starts_with_dot': 'Bestandsnaam mag niet beginnen met "."',
      },
    },
    clientErrors: {
      render:
        'Er is helaas iets fout gegaan bij het weergeven van dit onderdeel.',
    },
    validations: {
      yup: {
        mixed: {
          required: 'Het veld ${label} is niet ingevuld',
          notType:
            'De opgegeven waarde is van een ongeldig type. Geldig type: ${type}.',
        },
        string: {
          email:
            "'${value}' is geen geldig e-mailadres. Suggestie: naam@example.com",
        },
        number: {
          lessThan: 'Waarde moet kleiner zijn dan ${less}',
          moreThan: 'Waarde moet groter zijn dan ${more}',
        },
        array: {
          min: 'Minimaal ${min} items',
          max: 'Maximaal ${max} items',
        },
      },
      custom: {
        array: {
          min: 'Voer minimaal ${min} item(s) toe',
          max: 'Voer maximaal ${max} item(s) toe',
          email: {
            min: 'Voer minimaal ${min} e-mailadres(sen) in.',
            max: 'Voer maximaal ${max} e-mailadres(sen) in.',
          },
        },
        string: {
          emailOrMagicString:
            "'${value}' is geen geldig e-mailadres of magicstring. Suggestie: naam@example.com, [[ magic_string ]]",
          iban: "'${value}' is geen geldig IBAN. Suggestie: NL91ABNA0417164300",
          httpsLink:
            "'${value}' is geen geldige https link. Suggestie: https://www.zaaksysteem.nl",
          phoneNumber:
            "'${value}' is geen telefoonnummer. Suggestie: 0631542424",
          numeric: 'De opgegeven waarde is geen nummer. Suggestie: 123',
        },
        file: {
          noFile: 'Geen bestand(en) geüpload. Voeg een bestand toe.',
          invalidFile: 'Ongeldig(e) bestand(en) geüpload. Probeer het opnieuw.',
        },
      },
    },
    fileSelect: {
      addFile: 'Bestanden toevoegen',
      selectInstructionsSingular: 'Selecteer een bestand',
      dragInstructionsSingular: 'Sleep uw bestand hierheen',
      dropInstructionsSingular: 'Sleep uw bestand hierheen',
      selectInstructionsPlural: 'Selecteer bestand(en)',
      dragInstructionsPlural: 'Sleep uw bestand(en) hierheen',
      dropInstructionsPlural: 'Sleep uw bestand(en) hierheen',
      orLabel: 'of',
    },
    caseRequestor: {
      error: 'De aanvrager van de zaak is niet beschikbaar.',
      loading: 'Bezig met laden…',
    },
    externalComponents: {
      mapVersionMismatchMessage:
        'Zaaksysteem heeft een verouderde versie: {{oldVersion}} van de kaartimplementatie ontvangen. De huidige versie: {{newVersion}}. Dit betekent dat de kaart mogelijk niet goed werkt. Neem contact op met de systeembeheerder om de implementatie te updaten of te veranderen',
      mapVersionMismatchTitle: 'Versie komt niet overeen',
    },
    entityType: {
      case: 'Zaak',
      file: 'Bestand',
      person: 'Persoon',
      organization: 'Organisatie',
      employee: 'Medewerker',
      case_type: 'Zaaktype',
      folder: 'Map',
      object_type: 'Objecttype',
      custom_object_type: 'Objecttype (beta)',
      attribute: 'Kenmerk',
      email_template: 'E-mailsjabloon',
      document_template: 'Documentsjabloon',
      other: 'Overig',
      contact: 'Contact',
      contact_moment: 'Contactmoment',
    },
    timeline: {
      filters: {
        documents: 'Documenten',
        changes: 'Wijzigingen',
      },
    },
    accessibility: {
      close: 'sluiten',
    },
  },
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fetch from 'node-fetch';
import { increment, cspReqestTiming } from './logging.js';

const hostsMap = new Map();
const bucketHost = process.env.BUCKET_HOST;

export default (nonce, unsafe, reqHost) => {
  const whitelistMain = [
    'https://*.bus.koppel.app',
    'https://editor.zaaksysteem.demo.datamask.nl/',
    'https://geodata.nationaalgeoregister.nl/',
    bucketHost,
  ]
    .filter(Boolean)
    .join(' ');

  const whitelistStyles = ['https://fonts.googleapis.com/css'].join(' ');

  const whitelistImages = [
    'https://c1-powerpoint-15.cdn.office.net',
    'https://c1-excel-15.cdn.office.net',
    'https://c1-word-view-15.cdn.office.net',
    'https://geodata.nationaalgeoregister.nl/',
    bucketHost,
  ]
    .filter(Boolean)
    .join(' ');

  const whitelistFrames = [
    'https://*.bus.koppel.app',
    'https://login.live.com/',
    'https://ffc-word-edit.officeapps.live.com/',
    'https://ffc-word-view.officeapps.live.com/',
    bucketHost,
  ]
    .filter(Boolean)
    .join(' ');

  const whitelistConnect = [
    'https://geodata.nationaalgeoregister.nl/',
    bucketHost,
  ]
    .filter(Boolean)
    .join(' ');

  const buildCspHeader = whitelistDomains =>
    [
      `default-src 'self' ${whitelistMain} ${whitelistDomains}`,
      `style-src 'self' ${
        unsafe ? "'unsafe-inline'" : `'nonce-${nonce}'`
      } ${whitelistStyles} ${whitelistDomains}`,
      `frame-src 'self' ${whitelistFrames} ${whitelistDomains}`,
      `img-src 'self' blob: data: ${whitelistImages} ${whitelistDomains}`,
      `object-src 'none'`,
      `frame-ancestors 'self'`,
      `require-trusted-types-for 'script'`,
      `connect-src 'self' data: ${whitelistConnect} ${whitelistDomains}`,
      `base-uri 'self'`,
    ].join('; ');

  let host = hostsMap.get(reqHost);
  let cspReq;

  const executeCspRequest = cspReqestTiming(reqHost, () =>
    fetch('https://' + reqHost + '/csp')
      .then(rs => rs.json())
      .then(res =>
        res.csp_hosts
          .filter(hostname => hostname !== '*')
          .map(hostname => 'https://' + hostname)
          .join(' ')
      )
      .catch(() => '')
  );

  if (!host || host.cspReqTimestamp + 10 * 60 * 1000 < Date.now()) {
    increment(reqHost);
    cspReq = executeCspRequest();
    host = { name: reqHost, cspReqTimestamp: Date.now(), cspReq };
    hostsMap.set(reqHost, host);
  } else {
    cspReq = host.cspReq;
  }

  return cspReq.then(buildCspHeader);
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { join, dirname } from 'path';
import { promises } from 'fs';
import { randomBytes } from 'crypto';
import { fileURLToPath } from 'url';
import helmet from 'helmet';
import express from 'express';
import generateCspHeader from './generateCspHeader.js';
import { increment } from './logging.js';

const thisFileDirectory = dirname(fileURLToPath(import.meta.url));

console.log(process.env);

const app = express();
const generateNonce = () => randomBytes(16).toString('base64');

app.use(helmet({ contentSecurityPolicy: false }));
app.use(function (req, res, next) {
  res.removeHeader('X-Powered-By');
  next();
});

const frontends = [
  ['main'],
  ['admin'],
  ['my-pip'],
  ['external-components'],
  ['objection-app'],
];

frontends.forEach(([fe]) => {
  const serveIndexFile = (req, res) => {
    increment(req.headers.host);
    const nonce = generateNonce();
    const unsafe = Boolean(req.query.unsafeinlinestyles);
    const indexPath = join(
      thisFileDirectory,
      '..',
      'build',
      'apps',
      fe,
      'index.html'
    );

    const indexPromise = promises.readFile(indexPath).then(data => {
      const index = data.toString();
      return index.replace('%%NONCE%%', nonce);
    });

    const cspHeaderPromise = generateCspHeader(nonce, unsafe, req.headers.host);

    Promise.all([indexPromise, cspHeaderPromise])
      .then(([renderedIndex, cspHeader]) => {
        res.setHeader('Content-Security-Policy', cspHeader);
        res.setHeader('Cross-Origin-Embedder-Policy', 'unsafe-none');
        res.send(renderedIndex);
      })
      .catch(() => {
        res.status(500);
      });
  };

  const serveAssetFile = (req, res) => {
    increment(req.headers.host);
    const parts = req.path.split('/');
    const file = parts[parts.length - 1];

    res.sendFile(join(thisFileDirectory, '..', 'build', 'apps', fe, file));
  };

  app.get('/' + fe, serveIndexFile);

  app.get('/' + fe + '/*', (req, res) => {
    if (req.path.includes('.') && !req.path.includes('.html')) {
      serveAssetFile(req, res);
    } else {
      serveIndexFile(req, res);
    }
  });
});

app.listen(process.env.PORT);

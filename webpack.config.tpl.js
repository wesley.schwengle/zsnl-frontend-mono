// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable complexity */

import { resolve as _resolve, relative, dirname } from 'path';
import { fileURLToPath } from 'url';
import { createRequire } from 'module';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { WebpackManifestPlugin } from 'webpack-manifest-plugin';
import copyPublicDirectoryPlugin from './node/copyPublicDirectoryPlugin.js';

const req = createRequire(import.meta.url);
const webpack = req('webpack');
const MEP = req('mini-css-extract-plugin');

const thisFileDirectory = dirname(fileURLToPath(import.meta.url));

if (!process.env.NODE_ENV) throw new Error('Empty NODE_ENV env variable');
const isEnvDevelopment = process.env.NODE_ENV === 'development';
const isEnvProduction = process.env.NODE_ENV === 'production';

const makeEnvVarDefinitions = xs =>
  Object.fromEntries(
    xs.map(name => [
      `process.env.${name}`,
      process.env[name] ? JSON.stringify(process.env[name]) : undefined,
    ])
  );

export default app => {
  return {
    mode: isEnvProduction ? 'production' : isEnvDevelopment && 'development',
    bail: isEnvProduction,
    devtool: 'source-map',
    entry: { [app.name]: app.indexPath },
    output: {
      path: _resolve(thisFileDirectory, 'build', 'apps', app.name),
      pathinfo: isEnvDevelopment,
      filename: isEnvProduction
        ? 'index.[contenthash:8].js'
        : isEnvDevelopment && '[name].js',
      chunkFilename: isEnvProduction
        ? 'index.[contenthash:8].chunk.js'
        : isEnvDevelopment && '[name].chunk.js',
      publicPath: app.publicUrlFrag,
      devtoolModuleFilenameTemplate: isEnvProduction
        ? info =>
            relative(app.srcPath, info.absoluteResourcePath).replace(/\\/g, '/')
        : isEnvDevelopment &&
          (info => _resolve(info.absoluteResourcePath).replace(/\\/g, '/')),
      globalObject: 'this',
    },
    optimization: {
      minimize: isEnvProduction,
      splitChunks: {
        chunks: 'all',
        name: isEnvDevelopment ? app.name : false,
      },
      runtimeChunk: {
        name: entrypoint => `runtime-${entrypoint.name}`,
      },
    },
    resolve: {
      modules: ['node_modules'],
      extensions: ['.js', '.ts', '.tsx', '.svg', '.css', '.png'],
      alias: {
        '@zaaksysteem': _resolve(thisFileDirectory, 'packages'),
      },
    },
    module: {
      rules: [
        {
          test: /\.(js|mjs|jsx|ts|tsx)$/,
          include: [
            app.srcPath,
            _resolve(thisFileDirectory, 'packages', 'common', 'src'),
            _resolve(thisFileDirectory, 'packages', 'ui', 'App'),
            _resolve(
              thisFileDirectory,
              'packages',
              'communication-module',
              'src'
            ),
            _resolve(thisFileDirectory, 'packages', 'kitchen-sink', 'source'),
          ],
          loader: req.resolve('babel-loader'),
          resolve: { fullySpecified: false },
          options: {
            babelrc: false,
            configFile: false,
            cacheDirectory: true,
            cacheCompression: false,
            compact: true,
            presets: [
              '@babel/preset-typescript',
              [
                '@babel/preset-react',
                {
                  development: isEnvDevelopment,
                  useBuiltIns: true,
                },
              ],
            ],
          },
        },
        {
          test: /.css$/,
          use: [MEP.loader, 'css-loader'],
          sideEffects: true,
        },
        {
          test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                mimetype: 'application/font-woff',
              },
            },
          ],
        },
        {
          test: /.svg$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                mimetype: 'image/svg+xml',
              },
            },
          ],
        },
        {
          test: /.png$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                mimetype: 'image/png',
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new webpack.DefinePlugin(
        makeEnvVarDefinitions(['APP_CONTEXT_ROOT', 'NODE_ENV', 'PUBLIC_URL'])
      ),
      new HtmlWebpackPlugin(
        Object.assign(
          {},
          {
            inject: true,
            template: app.htmlPath,
          },
          isEnvProduction
            ? {
                minify: {
                  removeComments: true,
                  collapseWhitespace: true,
                  removeRedundantAttributes: true,
                  useShortDoctype: true,
                  removeEmptyAttributes: true,
                  removeStyleLinkTypeAttributes: true,
                  keepClosingSlash: true,
                  minifyJS: true,
                  minifyCSS: true,
                  minifyURLs: true,
                },
              }
            : undefined
        )
      ),
      new MEP(),
      new WebpackManifestPlugin({
        fileName: 'asset-manifest.json',
        publicPath: app.publicUrlFrag,
        generate: (seed, files, entrypoints) => {
          const manifestFiles = files.reduce((manifest, file) => {
            manifest[file.name] = file.path;
            return manifest;
          }, seed);
          const entrypointFiles = entrypoints[app.name].filter(
            fileName => !fileName.endsWith('.map')
          );

          return {
            files: manifestFiles,
            entrypoints: entrypointFiles,
          };
        },
      }),
      new webpack.IgnorePlugin({ resourceRegExp: /moment$/ }),
      copyPublicDirectoryPlugin(app.name),
    ].filter(Boolean),
  };
};
